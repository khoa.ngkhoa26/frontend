# Frontend

Use React + TypeScript, TailwindCSS for design

## Khối lượng dòng mã (Lines of Code)
```sh
      51 text files.
      51 unique files.
       0 files ignored.

github.com/AlDanial/cloc v 1.98  T=0.30 s (170.8 files/s, 11013.1 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
TypeScript                      50            351             27           2908
CSS                              1              0              0              3
-------------------------------------------------------------------------------
SUM:                            51            351             27           2911
-------------------------------------------------------------------------------
```
