export const enum MenuEnum {
  HOME = "/",
  LOGIN = "/login",
  EXAM = "/exam",
  ADMIN = "/admin"
}

export const enum AdminEnum {
  CAUHOI = "cauhoi",
  CAUHOIDETHI = "cauhoidethi",
  CAUTRALOI = "cautraloi",
  CHUONG = "chuong",
  DANGKYLOPHOC = "dangkylophoc",
  DETHI = "dethi",
  DETHIMONHOC = "dethimonhoc",
  LOPHOC = "lophoc",
  MONHOC = "monhoc",
  NGUOIDUNG = "nguoidung",
  QUYEN = "quyen",
  SINHVIEN = "sinhvien",
  THOIGIANDETHICHINH = "thoigiandethichinh"
}

export const enum InfoEnum {
  ACCOUNT_STUDENT = "account-student",
  CLASS_SUBJECT = "class-subject",
  EXAM_SCHEDULE = "exam-schedule",
  RESULT_REAL_EXAM = "real-exam",
  RESULT_TEST_EXAM = "test-exam"
}