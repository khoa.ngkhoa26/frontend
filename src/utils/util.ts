const formatDate = (isoDate: string): string => {
  const date = new Date(isoDate)
  const options: Intl.DateTimeFormatOptions = {
      year: "numeric",
      month: "numeric",
      day: "numeric",
  }
  return date.toLocaleDateString("vi-VN", options)
}

const formatDateTime = (isoDate: string): string => {
  const date = new Date(isoDate)
  const options: Intl.DateTimeFormatOptions = {
      year: "numeric",
      month: "numeric",
      day: "numeric",
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
  }
  return date.toLocaleDateString("vi-VN", options)
}

export default {
  formatDate, formatDateTime
}