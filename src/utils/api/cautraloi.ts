import axios from "axios";
import { ArrayCauTraLoi, LastOrCount } from "../types";

const API_URL: string = "http://localhost:3000/api";

async function insert(cauhoi_id: number, name: string): Promise<any> {
  try {
    const response = await axios.post(
      `${API_URL}/cautraloi/insert`,
      {
        cauhoi_id: cauhoi_id,
        name: name,
      },
      {
        withCredentials: true,
      }
    );
    return response.data["message"];
  } catch (error) {
    throw new Error("Can't insert data");
  }
}

async function update(
  id: number,
  cauhoi_id: number,
  name: string
): Promise<string> {
  try {
    const response = await axios.put(
      `${API_URL}/cautraloi/update`,
      {
        id: id,
        cauhoi_id: cauhoi_id,
        name: name,
      },
      {
        withCredentials: true,
      }
    );
    return response.data["message"];
  } catch (error) {
    throw new Error("Can't update data");
  }
}

async function remove(id: number): Promise<string> {
  try {
    const response = await axios.delete(`${API_URL}/cautraloi/remove/${id}`, {
      withCredentials: true,
    });
    return response.data["message"];
  } catch (error) {
    throw new Error("Can't update data");
  }
}

async function getAll(limit: number, offset: number): Promise<ArrayCauTraLoi> {
  try {
    const response = await axios.get(
      `${API_URL}/cautraloi/all/?limit=${limit}&offset=${offset}`,
      {
        withCredentials: true,
      }
    );
    return response.data;
  } catch (error) {
    throw new Error("Can't get data");
  }
}

async function getCount(): Promise<LastOrCount> {
  try {
    const response = await axios.get(`${API_URL}/cautraloi/count`, {
      withCredentials: true,
    });
    return response.data["getcount_cautraloi"];
  } catch (error) {
    throw new Error("Can't get data");
  }
}

async function get(cauhoi_id: number) {
  try {
    const response = await axios.get(
      `${API_URL}/cautraloi/cauhoi/${cauhoi_id}`,
      {
        withCredentials: true,
      }
    );
    return response.data;
  } catch (error) {
    throw new Error("Can't get data");
  }
}

export default {
  get,
  insert,
  update,
  remove,
  getAll,
  getCount,
};
