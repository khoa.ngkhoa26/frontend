import axios from "axios"

const API_URL: string = "http://localhost:3000/api"

async function checkadmin(): Promise<string | undefined> {
  try {
    const response = await axios.get(
      `${API_URL}/auth/checkadmin`, {
        withCredentials: true
      }
    )
    return response.data
  } catch (error) {
    return undefined
  }
}

async function checklogin(): Promise<string | undefined> {
  try {
    const response = await axios.get(
      `${API_URL}/auth/checklogin`, {
        withCredentials: true
      }
    )
    return response.data
  } catch (error) {
    return undefined
  }
}

async function login(username: string, password: string): Promise<void> {
  try {
    const response = await axios.post(
      `${API_URL}/auth/login`,
      {
        username: username,
        password: password
      }, {
        withCredentials: true
      }
    )
    return response.data
  } catch (error) {
    throw new Error("Can't get data")
  }
}

async function logout(): Promise<void> {
  try {
    const response = await axios.delete(
      `${API_URL}/auth/logout`, {
        withCredentials: true
      }
    )
    return response.data
  } catch (error) {
    throw new Error("Can't get data")
  }
}

export default {
  checkadmin, checklogin, login, logout
}