import axios from "axios";
import { ArrayCauHoi, CauHoi } from "../types";

const API_URL: string = "http://localhost:3000/api";

async function insert(chuong_id: number, name: string): Promise<void> {
  try {
    const response = await axios.post(
      `${API_URL}/cauhoi/insert`,
      {
        chuong_id: chuong_id,
        name: name,
      },
      {
        withCredentials: true,
      }
    );
    return response.data["message"];
  } catch (error) {
    throw new Error("Can't insert data");
  }
}

async function update(
  id: number,
  chuong_id: number,
  name: string
): Promise<void> {
  try {
    const response = await axios.put(
      `${API_URL}/cauhoi/update`,
      {
        id: id,
        chuong_id: chuong_id,
        name: name,
      },
      {
        withCredentials: true,
      }
    );
    return response.data["message"];
  } catch (error) {
    throw new Error("Can't update data");
  }
}

async function remove(id: number): Promise<void> {
  try {
    const response = await axios.delete(`${API_URL}/cauhoi/remove/${id}`, {
      withCredentials: true,
    });
    return response.data["message"];
  } catch (error) {
    throw new Error("Can't remove data");
  }
}

async function getAll(limit: number, offset: number): Promise<ArrayCauHoi> {
  try {
    const response = await axios.get(
      `${API_URL}/cauhoi/all/?limit=${limit}&offset=${offset}`,
      {
        withCredentials: true,
      }
    );
    return response.data;
  } catch (error) {
    throw new Error("Can't get data");
  }
}

async function get(cauhoi_id: number): Promise<CauHoi> {
  try {
    const response = await axios.get(`${API_URL}/cauhoi/id/${cauhoi_id}`, {
      withCredentials: true,
    });
    return response.data;
  } catch (error) {
    throw new Error("Can't get question data");
  }
}

export default {
  get,
  insert,
  update,
  remove,
  getAll,
};
