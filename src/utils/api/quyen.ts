import axios from "axios"
import { ArrayQuyen } from "../types"

const API_URL: string = "http://localhost:3000/api"

async function insert(
  name: string
): Promise<string> {
  try {
    const response = await axios.post(
      `${API_URL}/quyen/insert`,
      {
        name: name
      }, {
        withCredentials: true
      }
    )
    return response.data["message"]
  } catch (error) {
    throw new Error("Can't insert data")
  }
}

async function update(
  id: number, name: string
): Promise<string> {
  try {
    const response = await axios.put(
      `${API_URL}/quyen/update`,
      {
        id: id,
        name: name
      }, {
        withCredentials: true
      }
    )
    return response.data["message"]
  } catch (error) {
    throw new Error("Can't update data")
  }
}

async function remove(id: number): Promise<string> {
  try {
    const response = await axios.delete(
      `${API_URL}/quyen/remove/${id}`, {
        withCredentials: true
      }
    )
    return response.data["message"]
  } catch (error) {
    throw new Error("Can't remove data")
  }
}

async function getAll(
  limit: number, offset: number
): Promise<ArrayQuyen> {
  try {
    const response = await axios.get(
      `${API_URL}/quyen/all/?limit=${limit}&offset=${offset}`, {
        withCredentials: true
      }
    )
    return response.data
  } catch (error) {
    throw new Error("Can't get data")
  }
}

export default {
  insert, update, remove,
  getAll
}