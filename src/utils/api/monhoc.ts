import axios from "axios"
import { ArrayMonHoc } from "../types"

const API_URL: string = "http://localhost:3000/api"

async function insert(
  lophoc_id: number, name: string
): Promise<void> {
  try {
    const response = await axios.post(
      `${API_URL}/monhoc/insert`,
      {
        lophoc_id: lophoc_id,
        name: name
      }, {
        withCredentials: true
      }
    )
    return response.data["message"]
  } catch (error) {
    throw new Error("Can't insert data")
  }
}

async function update(
  id: number, lophoc_id: number, name: string
): Promise<void> {
  try {
    const response = await axios.put(
      `${API_URL}/monhoc/update`,
      {
        id: id,
        lophoc_id: lophoc_id,
        name: name
      }, {
        withCredentials: true
      }
    )
    return response.data["message"]
  } catch (error) {
    throw new Error("Can't update data")
  }
}

async function remove(id: number): Promise<void> {
  try {
    const response = await axios.delete(
      `${API_URL}/monhoc/remove/${id}`, {
        withCredentials: true
      }
    )
    return response.data["message"]
  } catch (error) {
    throw new Error("Can't remove data")
  }
}

async function getAll(
  limit: number, offset: number
): Promise<ArrayMonHoc> {
  try {
    const response = await axios.get(
      `${API_URL}/monhoc/all/?limit=${limit}&offset=${offset}`, {
        withCredentials: true
      }
    )
    return response.data
  } catch (error) {
    throw new Error("Can't get data")
  }
}

async function getAllOnSinhvien(
  sinhvien_id: number
): Promise<ArrayMonHoc> {
  try {
    const response = await axios.get(
      `${API_URL}/monhoc/sinhvien/${sinhvien_id}`, {
        withCredentials: true
      }
    )
    return response.data
  } catch (error) {
    throw new Error("Can't get data")
  }
}

export default {
  insert, update, remove,
  getAll, getAllOnSinhvien
}