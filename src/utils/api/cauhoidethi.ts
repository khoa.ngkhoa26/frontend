import axios from "axios";
import { ArrayCauHoiDeThi, CauHoiDeThi, LastOrCount } from "../types";

const API_URL: string = "http://localhost:3000/api";

async function update(
  id: number,
  cauhoi_id: number,
  cautraloi_id: number
): Promise<void> {
  try {
    const response = await axios.put(
      `${API_URL}/cauhoidethi/update`,
      {
        id: id,
        cauhoi_id: cauhoi_id,
        cautraloi_id: cautraloi_id,
      },
      {
        withCredentials: true,
      }
    );
    return response.data["message"];
  } catch (error) {
    throw new Error("Can't update data");
  }
}

async function getAll(
  limit: number,
  offset: number
): Promise<ArrayCauHoiDeThi> {
  try {
    const response = await axios.get(
      `${API_URL}/cauhoidethi/all/?limit=${limit}&offset=${offset}`,
      {
        withCredentials: true,
      }
    );
    return response.data;
  } catch (error) {
    throw new Error("Can't get data");
  }
}

async function get(dethi_id: LastOrCount): Promise<CauHoiDeThi[]> {
  try {
    const response = await axios.get(
      `${API_URL}/cauhoidethi/dethi/${dethi_id}`,
      {
        withCredentials: true,
      }
    );
    return response.data;
  } catch (error) {
    throw new Error("Can't get data");
  }
}

export default {
  get,
  update,
  getAll,
};
