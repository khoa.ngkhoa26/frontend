import axios from "axios"
import { ArrayThoiGianDeThiChinh } from "../types"

const API_URL: string = "http://localhost:3000/api"

async function insert(
  dethimonhoc_id: number, time: number
): Promise<string> {
  try {
    const response = await axios.post(
      `${API_URL}/thoigiandethichinh/insert`,
      {
        dethimonhoc_id: dethimonhoc_id,
        time: time
      }, {
        withCredentials: true
      }
    )
    return response.data["message"]
  } catch (error) {
    throw new Error("Can't insert data")
  }
}

async function update(
  id: number, dethimonhoc_id: number, time: number
): Promise<string> {
  try {
    const response = await axios.put(
      `${API_URL}/thoigiandethichinh/update`,
      {
        id: id,
        dethimonhoc_id: dethimonhoc_id,
        time: time
      }, {
        withCredentials: true
      }
    )
    return response.data
  } catch (error) {
    throw new Error("Can't update data")
  }
}

async function remove(id: number): Promise<string> {
  try {
    const response = await axios.delete(
      `${API_URL}/thoigiandethichinh/remove/${id}`, {
        withCredentials: true
      }
    )
    return response.data["message"]
  } catch (error) {
    throw new Error("Can't remove data")
  }
}

async function getAll(
  limit: number, offset: number
): Promise<ArrayThoiGianDeThiChinh> {
  try {
    const response = await axios.get(
      `${API_URL}/thoigiandethichinh/all/?limit=${limit}&offset=${offset}`, {
        withCredentials: true
      }
    )
    return response.data
  } catch (error) {
    throw new Error("Can't get data")
  }
}

async function getAllOnSinhvien(
  sinhvien_id: number
): Promise<ArrayThoiGianDeThiChinh> {
  try {
    const response = await axios.get(
      `${API_URL}/thoigiandethichinh/sinhvien/${sinhvien_id}`, {
        withCredentials: true
      }
    )
    return response.data
  } catch (error) {
    throw new Error("Can't get data")
  }
}

export default {
  insert, update, remove,
  getAll, getAllOnSinhvien
}