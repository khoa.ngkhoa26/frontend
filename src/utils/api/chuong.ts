import axios from "axios"
import { ArrayChuong, LastOrCount } from "../types"

const API_URL: string = "http://localhost:3000/api"

async function insert(
  monhoc_id: number, number: number, name: string
): Promise<void> {
  try {
    const response = await axios.post(
      `${API_URL}/chuong/insert`,
      {
        monhoc_id: monhoc_id,
        number: number,
        name: name
      }, {
        withCredentials: true
      }
    )
    return response.data["message"]
  } catch (error) {
    throw new Error("Can't insert data")
  }
}

async function update(
  id: number, monhoc_id: number, number: number, name: string
): Promise<void> {
  try {
    const response = await axios.put(
      `${API_URL}/chuong/update`,
      {
        id: id,
        monhoc_id: monhoc_id,
        number: number,
        name: name
      }, {
        withCredentials: true
      }
    )
    return response.data["message"]
  } catch (error) {
    throw new Error("Can't update data")
  }
}

async function remove(id: number): Promise<void> {
  try {
    const response = await axios.delete(
      `${API_URL}/chuong/remove/${id}`, {
        withCredentials: true
      }
    )
    return response.data["message"]
  } catch (error) {
    throw new Error("Can't remove data")
  }
}

async function getAll(
  limit: number, offset: number
): Promise<ArrayChuong> {
  try {
    const response = await axios.get(
      `${API_URL}/chuong/all/?limit=${limit}&offset=${offset}`, {
        withCredentials: true
      }
    )
    return response.data
  } catch (error) {
    throw new Error("Can't get data")
  }
}

async function getCount(): Promise<LastOrCount> {
  try {
    const response = await axios.get(`${API_URL}/chuong/count`, {
      withCredentials: true,
    });
    return response.data["getcount_chuong"];
  } catch (error) {
    throw new Error("Can't get data");
  }
}

export default {
  insert, update, remove,
  getAll, getCount
}