import axios from "axios"
import { ArrayNguoiDung, OnlyNguoiDung } from "../types"

const API_URL: string = "http://localhost:3000/api"

async function insert(
  username: string, quyen_id: number, password: string, displayname: string
): Promise<string> {
  try {
    const response = await axios.post(
      `${API_URL}/nguoidung/insert`,
      {
        username: username,
        quyen_id: quyen_id,
        password: password,
        displayname: displayname
      }, {
        withCredentials: true
      }
    )
    return response.data["message"]
  } catch (error) {
    throw new Error("Can't insert data")
  }
}

async function update(
  username: string, quyen_id: number, password: string, displayname: string
): Promise<string> {
  try {
    const response = await axios.put(
      `${API_URL}/nguoidung/update`,
      {
        username: username,
        quyen_id: quyen_id,
        password: password,
        displayname: displayname
      }, {
        withCredentials: true
      }
    )
    return response.data["message"]
  } catch (error) {
    throw new Error("Can't update data")
  }
}

async function updateWithoutQuyen(
  username: string, password: string, displayname: string
): Promise<string> {
  try {
    const response = await axios.put(
      `${API_URL}/nguoidung/update/info`,
      {
        username: username,
        password: password,
        displayname: displayname
      }, {
        withCredentials: true
      }
    )
    return response.data["message"]
  } catch (error) {
    throw new Error("Can't update data")
  }
}

async function remove(username: string): Promise<string> {
  try {
    const response = await axios.delete(
      `${API_URL}/nguodung/remove/${username}`, {
        withCredentials: true
      }
    )
    return response.data["message"]
  } catch (error) {
    throw new Error("Can't remove data")
  }
}

async function get(
  username: string
): Promise<OnlyNguoiDung> {
  try {
    const response = await axios.get(
      `${API_URL}/nguoidung/username/${username}`, {
        withCredentials: true
      }
    )
    return response.data
  } catch (error) {
    throw new Error("Can't get data")
  }
}

async function getAll(
  limit: number, offset: number
): Promise<ArrayNguoiDung> {
  try {
    const response = await axios.get(
      `${API_URL}/nguoidung/all/?limit=${limit}&offset=${offset}`, {
        withCredentials: true
      }
    )
    return response.data
  } catch (error) {
    throw new Error("Can't get data")
  }
}

export default {
  insert, update, updateWithoutQuyen, remove,
  get, getAll
}