import axios from "axios"
import { ArrayLopHoc, OnlyLopHoc } from "../types"

const API_URL: string = "http://localhost:3000/api"

async function insert(
  name: string
): Promise<void> {
  try {
    const response = await axios.post(
      `${API_URL}/lophoc/insert`,
      {
        name: name
      }, {
        withCredentials: true
      }
    )
    return response.data["message"]
  } catch (error) {
    throw new Error("Can't insert data")
  }
}

async function update(
  id: number, name: string
): Promise<void> {
  try {
    const response = await axios.put(
      `${API_URL}/lophoc/update`,
      {
        id: id,
        name: name
      }, {
        withCredentials: true
      }
    )
    return response.data["message"]
  } catch (error) {
    throw new Error("Can't update data")
  }
}

async function remove(id: number): Promise<void> {
  try {
    const response = await axios.delete(
      `${API_URL}/lophoc/remove/${id}`, {
        withCredentials: true
      }
    )
    return response.data["message"]
  } catch (error) {
    throw new Error("Can't remove data")
  }
}

async function getAll(
  limit: number, offset: number
): Promise<ArrayLopHoc> {
  try {
    const response = await axios.get(
      `${API_URL}/lophoc/all/?limit=${limit}&offset=${offset}`, {
        withCredentials: true
      }
    )
    return response.data
  } catch (error) {
    throw new Error("Can't get data")
  }
}

async function getOnSinhvien(
  sinhvien_id: number
): Promise<OnlyLopHoc> {
  try {
    const response = await axios.get(
      `${API_URL}/lophoc/sinhvien/${sinhvien_id}`, {
        withCredentials: true
      }
    )
    return response.data
  } catch (error) {
    throw new Error("Can't get data")
  }
}

export default {
  insert, update, remove,
  getAll, getOnSinhvien
}