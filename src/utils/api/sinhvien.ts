import axios from "axios"
import { ArraySinhVien, OnlySinhVien } from "../types"

const API_URL: string = "http://localhost:3000/api"

async function insert(
  username: string, name: string, gender: string, birthday: string
): Promise<string> {
  try {
    const response = await axios.post(
      `${API_URL}/sinhvien/insert`,
      {
        account: username,
        name: name,
        gender: gender,
        birthday: birthday
      }, {
        withCredentials: true
      }
    )
    return response.data["message"]
  } catch (error) {
    throw new Error("Can't insert data")
  }
}

async function update(
  id: number, username: string, name: string, gender: string, birthday: string
): Promise<string> {
  try {
    const response = await axios.put(
      `${API_URL}/sinhvien/update`,
      {
        id: id,
        account: username,
        name: name,
        gender: gender,
        birthday: birthday
      }, {
        withCredentials: true
      }
    )
    return response.data["message"]
  } catch (error) {
    throw new Error("Can't update data")
  }
}

async function remove(id: number): Promise<string> {
  try {
    const response = await axios.delete(
      `${API_URL}/sinhvien/remove/${id}`, {
        withCredentials: true
      }
    )
    return response.data["message"]
  } catch (error) {
    throw new Error("Can't remove data")
  }
}

async function get(
  username: string
): Promise<OnlySinhVien> {
  try {
    const response = await axios.get(
      `${API_URL}/sinhvien/username/${username}`, {
        withCredentials: true
      }
    )
    return response.data
  } catch (error) {
    throw new Error("Can't get data")
  }
}

async function getAll(
  limit: number, offset: number
): Promise<ArraySinhVien> {
  try {
    const response = await axios.get(
      `${API_URL}/sinhvien/all/?limit=${limit}&offset=${offset}`, {
        withCredentials: true
      }
    )
    return response.data
  } catch (error) {
    throw new Error("Can't get data")
  }
}

export default {
  insert, update, remove,
  get, getAll
}