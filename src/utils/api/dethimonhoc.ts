import axios from "axios"
import { ArrayDeThiMonHoc } from "../types"

const API_URL: string = "http://localhost:3000/api"

async function getAll(
  limit: number, offset: number
): Promise<ArrayDeThiMonHoc> {
  try {
    const response = await axios.get(
      `${API_URL}/dethimonhoc/all/?limit=${limit}&offset=${offset}`, {
        withCredentials: true
      }
    )
    return response.data
  } catch (error) {
    throw new Error("Can't get data")
  }
}

async function getAllRealOnSinhvien(
  sinhvien_id: number
): Promise<ArrayDeThiMonHoc> {
  try {
    const response = await axios.get(
      `${API_URL}/dethimonhoc/real/sinhvien/${sinhvien_id}`, {
        withCredentials: true
      }
    )
    return response.data
  } catch (error) {
    throw new Error("Can't get data")
  }
}

export default {
  getAll, getAllRealOnSinhvien
}