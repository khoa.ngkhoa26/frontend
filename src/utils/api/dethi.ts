import axios from "axios";
import { ArrayDeThi, DeThi, LastOrCount } from "../types";

const API_URL: string = "http://localhost:3000/api";

async function update(
  id: number,
  cauhoi_id: number,
  name: string
): Promise<void> {
  try {
    const response = await axios.post(
      `${API_URL}/cauhoidethi/update`,
      {
        id: id,
        cauhoi_id: cauhoi_id,
        name: name,
      },
      {
        withCredentials: true,
      }
    );
    return response.data;
  } catch (error) {
    throw new Error("Can't update data");
  }
}

async function getAll(limit: number, offset: number): Promise<ArrayDeThi> {
  try {
    const response = await axios.get(
      `${API_URL}/dethi/all/?limit=${limit}&offset=${offset}`,
      {
        withCredentials: true,
      }
    );
    return response.data;
  } catch (error) {
    throw new Error("Can't get data");
  }
}

async function insertThu(
  dethimonhoc_id: number,
  sinhvien_id: number
): Promise<void> {
  try {
    await axios.post(
      `${API_URL}/dethi/dethithu`,
      {
        dethimonhoc_id,
        sinhvien_id,
      },
      { withCredentials: true }
    );
  } catch (error) {
    throw new Error("Can't insert thi thu");
  }
}

async function getCount(): Promise<LastOrCount> {
  try {
    const response = await axios.get(`${API_URL}/dethi/count`, {
      withCredentials: true,
    });
    return response.data["getcount_dethi"];
  } catch (error) {
    throw new Error("Can't get data");
  }
}

async function get(dethi_id: LastOrCount): Promise<DeThi> {
  try {
    const response = await axios.get(`${API_URL}/dethi/id/${dethi_id}`, {
      withCredentials: true,
    });
    return response.data;
  } catch (error) {
    throw new Error("Can't get question data");
  }
}

export default {
  get,
  getCount,
  insertThu,
  update,
  getAll,
};
