import axios from "axios"
import { ArrayDangKyLopHoc } from "../types"

const API_URL: string = "http://localhost:3000/api"

async function insert(
  lophoc_id: number, sinhvien_id: string
): Promise<void> {
  try {
    const response = await axios.post(
      `${API_URL}/dangkylophoc/insert`,
      {
        lophoc_id: lophoc_id,
        sinhvien_id: sinhvien_id
      }, {
        withCredentials: true
      }
    )
    return response.data["message"]
  } catch (error) {
    throw new Error("Can't insert data")
  }
}

async function update(
  id: number, lophoc_id: number, sinhvien_id: string
): Promise<void> {
  try {
    const response = await axios.put(
      `${API_URL}/dangkylophoc/update`,
      {
        id: id,
        lophoc_id: lophoc_id,
        sinhvien_id: sinhvien_id
      }, {
        withCredentials: true
      }
    )
    return response.data["message"]
  } catch (error) {
    throw new Error("Can't update data")
  }
}

async function remove(id: number): Promise<void> {
  try {
    const response = await axios.delete(
      `${API_URL}/dangkylophoc/remove/${id}`, {
        withCredentials: true
      }
    )
    return response.data["message"]
  } catch (error) {
    throw new Error("Can't remove data")
  }
}

async function getAll(
  limit: number, offset: number
): Promise<ArrayDangKyLopHoc> {
  try {
    const response = await axios.get(
      `${API_URL}/dangkylophoc/all/?limit=${limit}&offset=${offset}`, {
        withCredentials: true
      }
    )
    return response.data
  } catch (error) {
    throw new Error("Can't get data")
  }
}

export default {
  insert, update, remove,
  getAll
}