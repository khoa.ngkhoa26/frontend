export type LastOrCount = number | undefined

export type CauHoi = {
  cauhoi_id: number
  chuong_id: number
  cautraloi_id: number
  cauhoi_name: string
}
export type ArrayCauHoi = CauHoi[] | undefined

export type CauHoiDeThi = {
  cauhoidethi_id: number
  dethi_id: number
  cauhoi_id: number
  cautraloi_id: number
}
export type ArrayCauHoiDeThi = CauHoiDeThi[] | undefined

export type CauTraLoi = {
  cautraloi_id: number
  cauhoi_id: number
  cautraloi_name: string
}
export type OnlyCauTraLoi = CauTraLoi | undefined
export type ArrayCauTraLoi = CauTraLoi[] | undefined

export type Chuong = {
  chuong_id: number
  monhoc_id: number
  chuong_number: number
  chuong_name: string
}
export type OnlyChuong = Chuong | undefined
export type ArrayChuong = Chuong[] | undefined

export type DangKyLopHoc = {
  dangkylophoc_id: number
  sinhvien_id: number
  lophoc_id: number
}
export type ArrayDangKyLopHoc = DangKyLopHoc[] | undefined

export type DeThi = {
  dethi_id: number
  dethimonhoc_id: string
  sinhvien_id: number
  dethi_startdate: string
  dethi_enddate: string
}
export type ArrayDeThi = DeThi[] | undefined

export type DeThiMonHoc = {
  dethimonhoc_id: number
  monhoc_id: number
  dethimonhoc_name: string
  dethimonhoc_real: boolean
  dethimonhoc_questions: number
  dethimonhoc_time: number
}
export type OnlyDeThiMonHoc = DeThiMonHoc | undefined
export type ArrayDeThiMonHoc = DeThiMonHoc[] | undefined

export type LopHoc = {
  lophoc_id: number
  lophoc_name: string
}
export type OnlyLopHoc = LopHoc | undefined
export type ArrayLopHoc = LopHoc[] | undefined

export type MonHoc = {
  monhoc_id: number
  lophoc_id: number
  monhoc_name: string
}
export type OnlyMonHoc = MonHoc | undefined
export type ArrayMonHoc = MonHoc[] | undefined

export type NguoiDung = {
  nguoidung_account: string
  quyen_id: number
  nguoidung_password: string
  nguoidung_displayname: string
}
export type OnlyNguoiDung = NguoiDung | undefined
export type ArrayNguoiDung = NguoiDung[] | undefined

export type Quyen = {
  quyen_id: number
  quyen_name: string
}
export type ArrayQuyen = Quyen[] | undefined

export type SinhVien = {
  sinhvien_id: number
  nguoidung_account: string
  sinhvien_name: string
  sinhvien_gender: string
  sinhvien_birthday: string
}
export type OnlySinhVien = SinhVien | undefined
export type ArraySinhVien = SinhVien[] | undefined

export type ThoiGianDeThiChinh = {
  thoigiandethichinh_id: number
  dethimonhoc_id: number
  thoigiandethichinh_time: string
}
export type ArrayThoiGianDeThiChinh = ThoiGianDeThiChinh[] | undefined