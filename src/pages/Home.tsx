import useSetPageChoose from "../hooks/useSetPageChoose"

interface HomeProps {
  rerender: boolean
  changeRerender: (newRerender: boolean) => void
  pageChoose: string
  changePageChoose: (newPageChoose: string) => void
}

function Home(props: HomeProps): JSX.Element {
  useSetPageChoose({
    rerender: props.rerender, changeRerender: props.changeRerender,
    pageChoose: props.pageChoose, changePageChoose: props.changePageChoose
  })

  return (
    <div
      className="relative bg-cover h-screen"
      style={{
        backgroundImage: `linear-gradient(90deg, #181818 10%, hsla(0, 0%, 9%, .98) 20%, hsla(0, 0%, 9%, .97) 25%, 
        hsla(0, 0%, 9%, .95) 35%, hsla(0, 0%, 9%, .94) 40%, hsla(0, 0%, 9%, .92) 45%, hsla(0, 0%, 9%, .9) 50%, 
        hsla(0, 0%, 9%, .87) 55%, hsla(0, 0%, 9%, .82) 60%, hsla(0, 0%, 9%, .75) 65%, hsla(0, 0%, 9%, .63) 70%, 
        hsla(0, 0%, 9%, .45) 75%, hsla(0, 0%, 9%, .27) 80%, hsla(0, 0%, 9%, .15) 85%, hsla(0, 0%, 9%, .08) 90%, 
        hsla(0, 0%, 9%, .03) 95%, hsla(0, 0%, 9%, 0)), 
        url('https://images.unsplash.com/photo-1602722053020-af31042989d5?q=80&w=2940&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D')`,
      }} >
      <div className="absolute inset-0 flex items-center justify-start
       text-white text-left px-6 py-12 right-0 ml-12">
        <div className="w-1/2">
          <h1 className="text-4xl font-bold mb-2">
            Luyện Thi Trắc Nghiệm
          </h1>
          <h3 className="text-lg mb-2 font-bold">
            Mọi Lúc Mọi Nơi Trên TEST
          </h3>
          <p className="text-sm mb-4">
            Trang web TEST luyện thi trắc nghiệm là một nền tảng giáo dục hoàn hảo 
            để chuẩn bị cho các kỳ thi quan trọng. Giao diện trực quan và dễ sử dụng 
            của trang web này là điểm nổi bật đầu tiên khiến người dùng dễ dàng tiếp 
            cận các bài kiểm tra và ôn tập mọi lúc mọi nơi.
          </p>
          <a href="#">
            <button className="bg-sky-400 text-white px-6 py-3 rounded-lg shadow-md hover:bg-sky-400 font-bold">
              Luyện Thi Ngay
            </button>
          </a>
        </div>
      </div>
    </div>
  )
}

export default Home
