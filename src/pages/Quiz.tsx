import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import apiSinhvien from "../utils/api/sinhvien";
import apiDeThi from "../utils/api/dethi";
import apiCauHoiDeThi from "../utils/api/cauhoidethi";
import apiCauHoi from "../utils/api/cauhoi";
import apiCauTraLoi from "../utils/api/cautraloi";
import { CauHoi, CauTraLoi, DeThi } from "../utils/types";
import useCountdownTimer from "../hooks/useTimer";

function Quiz(): JSX.Element {
  const { subjectId } = useParams();
  const [exam, setExam] = useState<DeThi>();
  const [examQuestions, setExamQuestions] = useState<
    {
      question: CauHoi;
      options: CauTraLoi[];
    }[]
  >([]);

  const [showTimeoutMessage, setShowTimeoutMessage] = useState(false);
  const [showSubmitConfirmation, setShowSubmitConfirmation] = useState(false);

  const { remainingSeconds, formattedTime } = useCountdownTimer(
    exam?.dethi_enddate!,
    () => {
      setShowTimeoutMessage(true);
    }
  );

  const [currentPage, setCurrentPage] = useState(1);
  const questionsPerPage = 5;
  const totalPages = Math.ceil(examQuestions.length / questionsPerPage);

  // state lưu trữ câu trả lời cho mỗi câu hỏi
  const [answers, setAnswers] = useState<Array<number | null>>(
    Array(examQuestions.length).fill(null)
  );

  useEffect(() => {
    const data: string = localStorage.getItem("login-username") as string;
    async function fetchData() {
      const sinhviendata = await apiSinhvien.get(data);

      if (!sinhviendata) {
        alert("Không tìm được thông tin thí sinh!");
        window.location.href = "/exam";
        return;
      }

      if (!subjectId) {
        alert("Không tìm được thông tin đề bài!");
        window.location.href = "/exam";
        return;
      }

      await apiDeThi.insertThu(parseInt(subjectId), sinhviendata.sinhvien_id);
      const examId = await apiDeThi.getCount();
      const examData = await apiDeThi.get(examId);
      const rawExamQuestions = await apiCauHoiDeThi.get(examId);
      const questions = await Promise.all(
        rawExamQuestions.map(async (question) => {
          return {
            question: await apiCauHoi.get(question.cauhoi_id),
            options: await apiCauTraLoi.get(question.cauhoi_id),
          };
        })
      );
      console.log(examData);

      setExam(examData);
      setExamQuestions(questions);
    }
    fetchData();
  }, [subjectId]);

  const handleAnswerChange = (index: number, answerIndex: number) => {
    const updatedAnswers = [...answers];

    updatedAnswers[index] = answerIndex;

    setAnswers(updatedAnswers);
  };

  const handleSubmitExam = () => {
    setShowSubmitConfirmation(true); // Thông báo xác nhận khi nộp bài
  };

  const startOffset = (currentPage - 1) * questionsPerPage;
  const endOffset = startOffset + questionsPerPage;

  return (
    <div className="flex max-w-4xl mx-auto mt-20">
      <div className="w-3/4 p-6 bg-white rounded-md shadow-xl">
        {/* Cho mỗi trang chứa đủ 5 câu hỏi */}
        {/* Render các radiobox và xử lý thay đổi trạng thái của câu trả lời */}
        {examQuestions.slice(startOffset, endOffset).map((question, index) => (
          <div key={startOffset + index} className="quiz-question">
            <p className="text-lg font-semibold mb-2">
              {`Câu ${startOffset + index + 1}`}:{" "}
              {question.question.cauhoi_name}
            </p>
            {/* hiện các câu hỏi */}
            <div className="mt-2">
              {examQuestions[startOffset + index].options.map((option, i) => {
                return (
                  <label key={i} className="block my-2">
                    <input
                      type="radio"
                      name={`question_${startOffset + index}`}
                      value={option.cautraloi_id}
                      className="mr-2"
                      onChange={() =>
                        handleAnswerChange(startOffset + index, i)
                      }
                      checked={answers[startOffset + index] === i}
                    />
                    {option.cautraloi_name}
                  </label>
                );
              })}
            </div>
          </div>
        ))}

        {/* Pagination */}
        <div className="flex justify-between mt-6">
          <button
            onClick={() => {
              if (currentPage > 1) setCurrentPage(currentPage - 1);
            }}
            className="bg-sky-400 px-4 py-2 rounded-md text-white disabled:opacity-50 disabled:cursor-not-allowed"
            disabled={currentPage === 1}
          >
            Previous
          </button>
          <button
            onClick={() => {
              if (currentPage < totalPages) setCurrentPage(currentPage + 1);
            }}
            className="bg-sky-400 px-4 py-2 rounded-md text-white disabled:opacity-50 disabled:cursor-not-allowed"
            disabled={currentPage === totalPages}
          >
            Next
          </button>
        </div>
      </div>

      {/* Bảng thông tin bên phải */}
      <div className="w-50 h-60 p-6 bg-gray-200 rounded-md shadow-xl ml-4">
        <p className="mb-4 text-center">
          <span className="font-bold">Môn thi: </span>
          {}
        </p>
        <div className="mb-4">
          Số lượt trang: {currentPage}/{totalPages}
        </div>
        <div>
          Thời gian:{" "}
          {`${formattedTime.minutes ?? "00"}:${formattedTime.seconds ?? "00"}`}
        </div>
        <button
          className="mt-6 bg-sky-400 px-4 py-2 rounded-md text-white block mx-auto"
          onClick={handleSubmitExam}
        >
          Nộp bài
        </button>
        {showSubmitConfirmation && (
          <div className="bg-gray-100 border border-gray-300 p-4 rounded-md fixed top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2">
            <p className="mb-2">Bạn có chắc muốn nộp bài?</p>
            <div className="flex justify-center">
              <button
                className="bg-sky-400 text-white px-4 py-2 rounded-md mr-2"
                onClick={() => {
                  setShowSubmitConfirmation(false);
                  setShowTimeoutMessage(true);
                }}
              >
                Xác nhận
              </button>
              <button
                className="bg-red-300 px-4 py-2 rounded-md"
                onClick={() => setShowSubmitConfirmation(false)}
              >
                Hủy
              </button>
            </div>
          </div>
        )}

        {showTimeoutMessage && (
          <div
            className="bg-green-400 p-4 mt-4 rounded text-center absolute 
                       top-24 left-1/2 transform -translate-x-1/2 -translate-y-1/2"
          >
            <p className="font-bold text-white">THÔNG BÁO</p>
            <p className="text-white">Bài của bạn đã được nộp thành công</p>
          </div>
        )}
      </div>
    </div>
  );
}

export default Quiz;
