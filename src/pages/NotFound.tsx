function NotFound(): JSX.Element {
  return (
    <div className="mx-32 p-16">
      <h1 className="text-center">Không tìm thấy trang</h1>
      <p className="text-center">Vui lòng thử lại</p>
    </div>
  )
}

export default NotFound