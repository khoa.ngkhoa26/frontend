import { useEffect, useState } from "react"

import usePreventNotLogin from "../hooks/usePreventNotLogin"
import useSetPageChoose from "../hooks/useSetPageChoose"
import { ArrayDeThiMonHoc, ArrayMonHoc, ArrayThoiGianDeThiChinh, OnlyLopHoc, OnlyNguoiDung, OnlySinhVien } from "../utils/types"
import apiDethimonhoc from "../utils/api/dethimonhoc"
import apiNguoidung from "../utils/api/nguoidung"
import apiSinhvien from "../utils/api/sinhvien"
import apiLophoc from "../utils/api/lophoc"
import apiMonhoc from "../utils/api/monhoc"
import apiThoigiandethichinh from "../utils/api/thoigiandethichinh"
import InfoMenu from "../components/InfoMenu"
import { InfoEnum } from "../utils/enum"
import InfoDisplay from "../components/InfoDisplay"

interface InfoProps {
  rerender: boolean
  changeRerender: (newRerender: boolean) => void
  pageChoose: string
  changePageChoose: (newPageChoose: string) => void
}

function Info(props: InfoProps): JSX.Element {
  usePreventNotLogin()
  useSetPageChoose({
    rerender: props.rerender, changeRerender: props.changeRerender,
    pageChoose: props.pageChoose, changePageChoose: props.changePageChoose
  })

  const [loginUsername, setLoginUsername] = useState<string>("")

  // Data chứa dữ liệu người dùng
  const [nguoidunginfo, setNguoidungInfo] = useState<OnlyNguoiDung>(undefined)
  const [sinhvieninfo, setSinhvienInfo] = useState<OnlySinhVien>(undefined)
  const [lophocinfo, setLophocInfo] = useState<OnlyLopHoc>(undefined)
  const [monhocinfo, setMonhocInfo] = useState<ArrayMonHoc>(undefined)
  const [dethimonhocinfo, setDethimonhocInfo] = useState<ArrayDeThiMonHoc>(undefined)
  const [thoigiandethiinfo, setThoigiandethiInfo] = useState<ArrayThoiGianDeThiChinh>(undefined)

  // Data quản lý UI/UX
  const [reloadPage, setReloadPage] = useState<boolean>(false)
  const [infomenuChoose, setInfomenuChoose] = useState<string>(InfoEnum.ACCOUNT_STUDENT)

  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      const data: string = localStorage.getItem("login-username") as string
      setLoginUsername(data)

      let nguoidungdata: OnlyNguoiDung
      let sinhviendata: OnlySinhVien
      let lophocdata: OnlyLopHoc
      let monhocdata: ArrayMonHoc
      let dethimonhocdata: ArrayDeThiMonHoc
      let thoigiandethidata: ArrayThoiGianDeThiChinh

      nguoidungdata = await apiNguoidung.get(loginUsername)
      if (nguoidungdata !== undefined)
        setNguoidungInfo(nguoidungdata)

      sinhviendata= await apiSinhvien.get(loginUsername)
      if (sinhviendata !== undefined)
        setSinhvienInfo(sinhviendata)

      // Lớp học
      if (sinhviendata !== undefined) {
        lophocdata = await apiLophoc.getOnSinhvien(sinhviendata.sinhvien_id)
        if (lophocdata !== undefined)
          setLophocInfo(lophocdata)
      }

      // Môn học
      if (sinhviendata !== undefined) {
        monhocdata = await apiMonhoc.getAllOnSinhvien(sinhviendata.sinhvien_id)
        if (monhocdata !== undefined)
          setMonhocInfo(monhocdata)
      }

      // Đề thi môn học và Thời gian đề thi chính thức
      if (sinhviendata !== undefined) {
        dethimonhocdata = await apiDethimonhoc.getAllRealOnSinhvien(sinhviendata.sinhvien_id)
        if (dethimonhocdata !== undefined)
          setDethimonhocInfo(dethimonhocdata)
      }
      if (sinhviendata !== undefined) {
        thoigiandethidata = await apiThoigiandethichinh.getAllOnSinhvien(sinhviendata.sinhvien_id)
        if (thoigiandethidata !== undefined)
          setThoigiandethiInfo(thoigiandethidata)
      }
    }

    fetchData()
    if (reloadPage === true)
      setReloadPage(false)
  }, [loginUsername, reloadPage === true])

  return (
    <div className="flex justify-center mt-24 mb-32">
      <InfoMenu infomenuChoose={infomenuChoose} changeInfomenuChoose={setInfomenuChoose} />

      <InfoDisplay
        changeReloadPage={setReloadPage}
        infomenuChoose={infomenuChoose}
        nguoidunginfo={nguoidunginfo}
        sinhvieninfo={sinhvieninfo}
        lophocinfo={lophocinfo}
        monhocinfo={monhocinfo}
        dethimonhocinfo={dethimonhocinfo}
        thoigiandethiinfo={thoigiandethiinfo}
      />
    </div>
  )
}

export default Info