import { useState } from "react"

import AdminDisplay from "../components/AdminDisplay"
import AdminMenu from "../components/AdminMenu"
import usePreventAdmin from "../hooks/usePreventAdmin"
import useSetPageChoose from "../hooks/useSetPageChoose"
import { AdminEnum } from "../utils/enum"

interface AdminProps {
  rerender: boolean
  changeRerender: (newRerender: boolean) => void
  pageChoose: string
  changePageChoose: (newPageChoose: string) => void
}

function Admin(props: AdminProps): JSX.Element {
  const [menuChoose, setMenuChoose] = useState<string>(AdminEnum.CAUHOI)
  const [limit, setLimit] = useState<number>(10)

  usePreventAdmin({changePageChoose: props.changePageChoose})
  useSetPageChoose({
    rerender: props.rerender, changeRerender: props.changeRerender,
    pageChoose: props.pageChoose, changePageChoose: props.changePageChoose
  })

  return (
    <div className="flex justify-between items-center w-full mb-6">
      <AdminMenu menuChoose={menuChoose} changeMenuChoose={setMenuChoose} />
      <AdminDisplay
        menuChoose={menuChoose}
        limit={limit} changeLimit={setLimit}
      />
    </div>
  )
}

export default Admin