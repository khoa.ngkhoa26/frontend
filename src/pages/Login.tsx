import { ChangeEvent, FormEvent, useState } from "react"
import { useNavigate } from "react-router-dom"

import apiAuth from '../utils/api/auth'
import usePreventLogin from "../hooks/usePreventLogin"
import useSetPageChoose from "../hooks/useSetPageChoose"
import { MenuEnum } from "../utils/enum"

interface LoginProps {
  rerender: boolean
  changeRerender: (newRerender: boolean) => void
  pageChoose: string
  changePageChoose: (newPageChoose: string) => void
}

function Login(props: LoginProps): JSX.Element {
  const navigate = useNavigate()
  const [username, setUsername] = useState<string>("")
  const [password, setPassword] = useState<string>("")
  const [showPassword, setShowPassword] = useState<boolean>(false)
  const [usernameError, setUsernameError] = useState<boolean>(false)
  const [passwordError, setPasswordError] = useState<boolean>(false)
  const [isUsernameFocused, setIsUsernameFocused] = useState<boolean>(false)
  const [isPasswordFocused, setIsPasswordFocused] = useState<boolean>(false)
  const [errorMessage, setErrorMessage] = useState<string>("")

  const onChangeInputUsername = (event: ChangeEvent<HTMLInputElement>): void => {
    setUsername(event.target.value)
    setUsernameError(false) // Reset lỗi khi người dùng thay đổi giá trị
  }

  const onChangeInputPassword = (event: ChangeEvent<HTMLInputElement>): void => {
    setPassword(event.target.value)
    setPasswordError(false) // Reset lỗi khi người dùng thay đổi giá trị
  }

  const onCheckShowPassword = () => {
    setShowPassword(!showPassword)
  }

  const onFocusInput = (field: string) => {
    if (field === 'username') {
      setIsUsernameFocused(true)
      setIsPasswordFocused(false)
    } else if (field === 'password') {
      setIsPasswordFocused(true)
      setIsUsernameFocused(false)
    }
  }

  const onBlurInput = () => {
    setIsUsernameFocused(false)
    setIsPasswordFocused(false)
  }

  const onSubmitLogin = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()

    // Reset thông báo trước mỗi lần submit
    setErrorMessage("")

    if (username === "") {
      setUsernameError(true)
    }

    if (password === "") {
      setPasswordError(true)
    }

    if (username !== "" && password !== "") {
      try {
        // Thực hiện đăng nhập 
        await apiAuth.login(username, password)

        props.changeRerender(true)
        props.changePageChoose(MenuEnum.HOME)
        localStorage.setItem('login-username', username)
        navigate("/")
      } catch (error) {
        setErrorMessage("Tài khoản không tồn tại!")
      }
    }
  }

  usePreventLogin()
  useSetPageChoose({
    rerender: props.rerender, changeRerender: props.changeRerender,
    pageChoose: props.pageChoose, changePageChoose: props.changePageChoose
  })

  return (
    <div className="flex items-center justify-center h-screen bg-gradient-to-br from-black via-sky-500 to-red-500">
      <form className="w-96 bg-white rounded p-8 shadow-2xl" onSubmit={onSubmitLogin}>
        <h1 className="text-center font-bold text-2xl text-black">Đăng Nhập</h1>

        <div className="py-6">
          <div className="mb-4">
            <label className="block text-black text-sm font-bold mb-2" htmlFor="username">
              Tài khoản
            </label>
            <input
              id="username"
              type="text"
              className={`appearance-none border-b-2 rounded-t-none w-full py-2 px-3 text-black  leading-tight 
                          focus:outline-none focus:shadow-outline transition-colors duration-300 
                          ease-in-out ${usernameError ? 'border-red-500' : ''} 
                          ${isUsernameFocused ? 'border-sky-400' : ''}`}
              onChange={onChangeInputUsername}
              onFocus={() => onFocusInput('username')}
              onBlur={onBlurInput}
            />

            {usernameError && <p className="text-red-500 text-xs mt-1">
              Vui lòng nhập tên đăng nhập!
            </p>}
          </div>
          <div className="mb-3">
            <label className="block text-black text-sm font-bold mb-2" htmlFor="password">
              Mật khẩu
            </label>
            <input
              id="password"
              type={showPassword ? "text" : "password"}
              className={`appearance-none border-b-2 w-full py-2 px-3 text-black leading-tight 
                          focus:outline-none focus:shadow-outline transition-colors duration-300 
                          ease-in-out ${passwordError ? 'border-red-500' : ''} 
                          ${isPasswordFocused ? 'border-sky-400' : ''}`}
              onChange={onChangeInputPassword}
              onFocus={() => onFocusInput('password')}
              onBlur={onBlurInput}
            />
            {passwordError && <p className="text-red-500 text-xs mt-1">
              Vui lòng nhập mật khẩu!
            </p>}
          </div>

          <div className="mb-3 flex items-center">
            <input
              type="checkbox"
              checked={showPassword}
              onChange={onCheckShowPassword}
              className="mr-2 leading-tight"
            />
            <label className="text-sm font-light" htmlFor="showPassword">
              Hiển thị mật khẩu
            </label>
          </div>
        </div>

        <button
          type="submit"
          className="block w-full bg-sky-400 transition duration-300 ease-in-out hover:bg-black 
            text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            >
            Đăng nhập
        </button>
        
      </form>
      {errorMessage && (
       <div className={`bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded absolute 
                          top-20 left-1/2 transform -translate-x-1/2 transition-transform 
                          duration-1000 ${errorMessage ? 'translate-y-0' : '-translate-y-full'} 
                          transition duration-1000 ease-in-out`}>
       <strong className="font-bold">Lỗi!</strong>
       <span className="block sm:inline"> {errorMessage}</span>
     </div>
        )}
    </div>
  )
}

export default Login
