import { useEffect } from "react"

import apiAuth from "../utils/api/auth"
import ApiMessageEnum from "../utils/apimessage"

interface UseCheckIfAdminProps {
  isLogin: boolean
  isAdmin: boolean
  changeIsAdmin: (newIsAdmin: boolean) => void
}

const useCheckIfAdmin = (props: UseCheckIfAdminProps): void => {
  useEffect(() => {
    const checkAdmin = async () => {
      try {
        const checkadminMessage: string | undefined = await apiAuth.checkadmin()
        if (checkadminMessage === ApiMessageEnum.USER_IS_ADMIN) {
          props.changeIsAdmin(true)
        } else {
          props.changeIsAdmin(false)
        }
      } catch (error) {
        console.error("Error checking admin:", error)
      }
    }

    checkAdmin()
  }, [props.isLogin])
}

export default useCheckIfAdmin
