import { useEffect } from "react"

import apiAuth from "../utils/api/auth"
import ApiMessageEnum from "../utils/apimessage"

interface UseCheckIfLoginProps {
  rerender: boolean
  changeRerender: (newRerender: boolean) => void
  isLogin: boolean
  changeIsLogin: (newIsLogin: boolean) => void
}

const useCheckIfLogin = (props: UseCheckIfLoginProps): void => {
  useEffect(() => {
    const checkLogin = async () => {
      try {
        const checkloginMessage: string | undefined = await apiAuth.checklogin()

        if (checkloginMessage === ApiMessageEnum.LOGINTOKEN_IS_FOUND) {
          props.changeIsLogin(true)
        } else {
          props.changeIsLogin(false)
        }
      } catch (error) {
        console.error("Error checking login:", error)
      }
    }

    checkLogin()
  }, [props.rerender])
}

export default useCheckIfLogin
