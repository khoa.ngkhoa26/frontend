import { useEffect } from "react"
import { useNavigate } from "react-router-dom"

import apiAuth from "../utils/api/auth"
import ApiMessageEnum from "../utils/apimessage"

const usePreventNotLogin = (): void => {
  const navigate = useNavigate()

  useEffect(() => {
    const checkLogin = async () => {
      try {
        const checkloginMessage: string | undefined = await apiAuth.checklogin()
        if (checkloginMessage === ApiMessageEnum.LOGINTOKEN_IS_NOT_FOUND) {
          navigate("/")
        }
      } catch (error) {
        console.error("Error checking login:", error)
      }
    }

    checkLogin()
  }, [])
}

export default usePreventNotLogin