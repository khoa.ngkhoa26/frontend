import { useEffect } from "react"
import { useNavigate } from "react-router-dom"

import apiAuth from "../utils/api/auth"
import ApiMessageEnum from "../utils/apimessage"
import { MenuEnum } from "../utils/enum"

interface usePreventAdminProps {
  changePageChoose: (newPageChoose: string) => void
}

const usePreventAdmin = (props: usePreventAdminProps): void => {
  const navigate = useNavigate()

  useEffect(() => {
    const checkLogin = async () => {
      try {
        const checkloginMessage: string | undefined = await apiAuth.checklogin()
        if (checkloginMessage === ApiMessageEnum.LOGINTOKEN_IS_NOT_FOUND) {
          navigate("/")
          props.changePageChoose(MenuEnum.HOME)
        }
      } catch (error) {
        console.error("Error checking login:", error)
      }
    }
    const checkAdmin = async () => {
      try {
        const checkadminMessage: string | undefined = await apiAuth.checkadmin()
        if (checkadminMessage === ApiMessageEnum.USER_IS_NOT_ADMIN) {
          navigate("/")
          props.changePageChoose(MenuEnum.HOME)
        }
      } catch (error) {
        console.error("Error checking admin login:", error)
      }
    }

    checkLogin()
    checkAdmin()
  }, [])
}

export default usePreventAdmin