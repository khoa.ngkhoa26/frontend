import { useEffect } from "react"
import { useLocation } from "react-router-dom"

interface UseCheckPathProps {
  changePageChoose: (newPageChoose: string) => void
}

const useCheckPath = (props: UseCheckPathProps): void => {
  const location = useLocation()

  useEffect(() => {
    props.changePageChoose(location.pathname)
  }, [])
}

export default useCheckPath