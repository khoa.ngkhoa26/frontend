import { useState, useEffect } from "react";

const useCountdownTimer = (endDate: string, onTimerEnd: () => void) => {
  const calculateRemainingSeconds = () => {
    const now = new Date().getTime();
    const endTime = new Date(endDate).getTime();
    const remainingSeconds = Math.max(Math.floor((endTime - now) / 1000), 0);
    return remainingSeconds;
  };

  const [remainingSeconds, setRemainingSeconds] = useState(
    calculateRemainingSeconds()
  );

  useEffect(() => {
    if (remainingSeconds <= 0) {
      // Timer has ended
      if (onTimerEnd) {
        onTimerEnd();
      }
    } else {
      const timer = setTimeout(() => {
        setRemainingSeconds(calculateRemainingSeconds());
      }, 1000);

      return () => {
        clearTimeout(timer);
      };
    }
  }, [remainingSeconds, endDate, onTimerEnd]);

  return {
    remainingSeconds,
    formattedTime: {
      minutes: Math.floor(remainingSeconds / 60),
      seconds: remainingSeconds % 60,
    },
  };
};

export default useCountdownTimer;
