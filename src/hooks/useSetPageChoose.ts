import { useEffect } from "react"
import { useLocation, Location } from "react-router-dom"
import { MenuEnum } from "../utils/enum"

interface useSetPageChooseProps {
  rerender: boolean
  changeRerender: (newRerender: boolean) => void
  pageChoose: string
  changePageChoose: (newPageChoose: string) => void
}

const useSetPageChoose = (props: useSetPageChooseProps) => {
  const location: Location<any> = useLocation()

  useEffect(() => {
    if (!props.rerender)
      switch (location.pathname) {
        case MenuEnum.HOME:
          props.changePageChoose(MenuEnum.HOME)
          props.changeRerender(true)
          break

        case MenuEnum.LOGIN:
          props.changePageChoose(MenuEnum.LOGIN)
          props.changeRerender(true)
          break

        case MenuEnum.ADMIN:
          props.changePageChoose(MenuEnum.ADMIN)
          props.changeRerender(true)
          break

        case MenuEnum.EXAM:
          props.changePageChoose(MenuEnum.EXAM)
          props.changeRerender(true)
          break

        default:
          break
      }

  }, [props.pageChoose])
}

export default useSetPageChoose