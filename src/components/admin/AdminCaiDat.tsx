interface AdminCaiDatProps {
  limit: number
  changeLimit: (newLimit: number) => void
}

function AdminCaiDat(props: AdminCaiDatProps): JSX.Element {
  return (
    <div className="mx-auto w-1/2">
      <h1 className="text-center">Cài đặt</h1>
      <div>
        <span>Chỉnh giới hạn giá trị của bảng (1-50):</span>
        <input type="number" value={props.limit} readOnly={true} />
      </div>
    </div>
  )
}

export default AdminCaiDat