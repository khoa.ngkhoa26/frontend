import { useEffect, useState } from "react"

import { ArrayChuong, Chuong } from "../../utils/types"
import apiChuong from "../../utils/api/chuong"

import { ArrayMonHoc, MonHoc } from "../../utils/types"
import apiMonHoc from "../../utils/api/monhoc"

import ApiMessageEnum from "../../utils/apimessage"

function AdminChuong(): JSX.Element {
  const [chuongAll, setChuongAll] = useState<ArrayChuong>(undefined)

  const [showEditPopup, setShowEditPopup] = useState<boolean>(false)
  const [showDeletePopup, setShowDeletePopup] = useState<boolean>(false)

  const [monHocList, setLopHocList] = useState<Array<string>>([])
  const [chuongList, setchuongList] = useState<Array<string>>([])

  // Sửa
  const [chuong, setChuong] = useState<Chuong>({
    chuong_id: 1,
    monhoc_id: 1,
    chuong_number: 1,
    chuong_name: "",
  })

  // Sửa
  const openEditPopup = (chuong: Chuong): void => {
    setChuong(chuong)
    setShowEditPopup(true)
  }
  const closeEditPopup = () => {
    setShowEditPopup(false)
  }
  const handleEditSubmit  = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault()

    const message: string = await apiChuong.update(chuong.chuong_id, chuong.monhoc_id, chuong.chuong_number, chuong.chuong_name)
    if (message === ApiMessageEnum.UPDATE_CHUONG_SUCCESS) {
      setChuong({
        chuong_id: 0,
        monhoc_id: 0,
        chuong_number: 0,
        chuong_name: "",
      })
      setShowEditPopup(false)
      alert("Cập nhật thành công")

      const newData: ArrayChuong = await apiChuong.getAll(100,0)
      if (newData !== undefined){
        setChuongAll(newData)
      }
    }
  }

  // Xóa
  const openDeletePopup = (chuong: Chuong): void => {
    setChuong(chuong)
    setShowDeletePopup(true)
  }
  const closeDeletePopup = () => {
    setShowDeletePopup(false)
  }
  const handleDelete = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault()
  
    const message: string = await apiChuong.remove(chuong.chuong_id)
    console.log(message)
    if (message === ApiMessageEnum.REMOVE_CHUONG_SUCCESS) {
      setChuong({
        chuong_id: 1,
        monhoc_id: 1,
        chuong_number: 1,
        chuong_name: "",
      })
      setShowDeletePopup(false)
      alert("Xóa thành công!")

      const newData: ArrayChuong = await apiChuong.getAll(100,0)
      if (newData !== undefined){
        setChuongAll(newData)
      }
    }
  }

  const onChangeMonHocId = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    const numericValue = event.target.value.replace(/\D/g, "")
    if (parseInt(numericValue, 10) < 1) {
      setChuong({...chuong, monhoc_id: 1})
      return
    }

    setChuong({...chuong, monhoc_id: parseInt(numericValue, 10)})
  }
  const onChangeChuongNumber = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    const numericValue = event.target.value.replace(/\D/g, "")
    if (parseInt(numericValue, 10) < 1) {
      setChuong({...chuong, chuong_number: 1})
      return
    }

    setChuong({...chuong, chuong_number: parseInt(numericValue, 10)})
  }
  const onChangeChuongName = (event: React.ChangeEvent<HTMLTextAreaElement>): void => {
    setChuong({ ...chuong, chuong_name: event.target.value })
  }

  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      const data: ArrayChuong = await apiChuong.getAll(100, 0)
      if (data !== undefined)
      setChuongAll(data)
    }

    fetchData()
  }, [])
  
    // Lấy thông tin môn học
    useEffect(() => {
    const fetchMonHoc = async (): Promise<void> => {
      try {
        const data: ArrayMonHoc = await apiMonHoc.getAll(10, 0)
  
        if (data !== undefined) {
          // Lấy danh sách tên tài khoản từ dữ liệu người dùng
          const idMonHoc = data.map((monhoc: MonHoc) => monhoc.monhoc_id)
          setLopHocList(idMonHoc)
        }
      } catch (error) {
        console.error('Error fetching account list:', error)
      }
    }
  
    fetchMonHoc()
  }, [])

  // Lấy thông tin chương number
  useEffect(() => {
    const fetchChuong = async (): Promise<void> => {
      try {
        const data: ArrayChuong = await apiChuong.getAll(10, 0)
  
        if (data !== undefined) {
          // Lấy danh sách tên tài khoản từ dữ liệu người dùng
          const numberChuong = data.map((chuong: Chuong) => chuong.chuong_number)
          setchuongList(numberChuong)
        }
      } catch (error) {
        console.error('Error fetching account list:', error)
      }
    }
  
    fetchChuong()
  }, [])

  return (
    <>
      {/* Nội dung trong trang */}
      <div className="flex justify-between items-center mb-2">
        <h1 className="text-black text-2xl font-bold">Quản lý Chương</h1>
      </div>

      <div className="table-wrapper" style={{ maxHeight: '550px', overflowY: 'auto' }}>
        <table className="w-full text-center mt-6 shadow-lg rounded-lg">
          <thead className="text-center bg-gray-700 shadow-lg rounded-lg">
            <tr className="text-white">
              <th scope="col" className="px-6 py-3">STT</th>
              <th scope="col" className="px-6 py-3">ID</th>
              <th scope="col" className="px-6 py-3">ID Môn Học</th>
              <th scope="col" className="px-6 py-3">Số Chương</th>
              <th scope="col" className="px-6 py-3">Tên</th>
              <th scope="col" className="px-6 py-3">Action</th>
            </tr>
          </thead>

          <tbody>
            { chuongAll !== undefined && chuongAll.length > 0
              ?
              (chuongAll.map((chuong: Chuong, thutu: number) => (
                <tr
                  key={chuong.chuong_id}
                  className={
                    `${thutu % 2 === 0
                    ? 'even:bg-gray-50 even:dark:bg-gray-800'
                    : 'odd:bg-white odd:dark:bg-gray-900'}
                    border-b hover:bg-gray-100 hover:cursor-pointers`
                  }
                >
                  <td className="px-6 py-4 font-bold">{thutu + 1}</td>
                  <td className="px-6 py-4">{chuong.chuong_id}</td>
                  <td className="px-6 py-4">{chuong.monhoc_id}</td>
                  <td className="px-6 py-4">{chuong.chuong_number}</td>
                  <td className="px-6 py-4">{chuong.chuong_name}</td>
                  <td className="px-6 py-4">
                    <button
                      onClick={openEditPopup.bind(null, chuong)}
                      className="bg-gray-700  transition duration-300 ease-in-out transform 
                      hover:scale-110 hover:bg-sky-400 text-white py-2 px-4 rounded-lg mr-2"
                    >
                      Sửa
                    </button>
                    <button
                      onClick={openDeletePopup.bind(null, chuong)}
                      className="bg-gray-700  transition duration-300 ease-in-out transform 
                      hover:scale-110 hover:bg-red-400 text-white py-2 px-4 rounded-lg"
                    >
                      Xóa
                    </button>
                  </td>
                </tr>
              )))
              :
              (
                <tr>
                  <td colSpan={6}>Không có dữ liệu. Vui lòng thử lại sau</td>
                </tr>
              )
            }
          </tbody>
        </table>
      </div>

       {/* Popup chỉnh sửa */}
       {showEditPopup && (
        <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
          <div className="bg-white p-8 w-1/2 rounded shadow-lg">
            <h2 className="text-2xl font-bold mb-4">Chỉnh Sửa Chương</h2>
            <form onSubmit={handleEditSubmit} className="space-y-4">
              <div>
                <label htmlFor="edit_chuong_id" className="block font-semibold">
                  ID:
                </label>
                <input
                  type="text"
                  id="edit_chuong_id"
                  value={chuong.chuong_id}
                  disabled
                  className="border border-gray-300 rounded px-3 py-2 w-full bg-gray-100"
                />
              </div>
              <div>
                <label htmlFor="edit_monhoc_id" className="block font-semibold">
                  ID Môn học:
                </label>
                <select
                  value={chuong.monhoc_id}
                  onChange={onChangeMonHocId}
                  className="border border-gray-300 rounded px-3 py-2 w-full "
                >
                  {monHocList.map((monhoc_id) => (
                    <option key={monhoc_id} value={monhoc_id}>
                      {monhoc_id}
                    </option>
                  ))}
                </select>
              </div>
              <div>
                <label htmlFor="edit_chuong_number" className="block font-semibold">
                  Số Chương:
                </label>
                <select
                  value={chuong.chuong_number}
                  onChange={onChangeChuongNumber}
                  className="border border-gray-300 rounded px-3 py-2 w-full "
                >
                  {chuongList.map((chuongNum_id) => (
                    <option key={chuongNum_id} value={chuongNum_id}>
                      {chuongNum_id}
                    </option>
                  ))}
                </select>
              </div>
              <div>
                <label htmlFor="edit_chuong_name" className="block font-semibold">
                  Tên Chương:
                </label>
                <textarea
                  id="edit_chuong_name"
                  value={chuong.chuong_name}
                  onChange={onChangeChuongName}
                  className="border border-gray-300 rounded px-3 py-2 w-full"
                  rows={4}
                />
              </div>
              <div className="flex justify-end">
                <button
                  type="submit"
                  className="bg-sky-400 text-white px-4 py-2 rounded hover:bg-sky-500"
                >
                  Lưu
                </button>
                <button
                  type="button"
                  onClick={closeEditPopup}
                  className="bg-gray-300 text-gray-700 px-4 py-2 rounded ml-4 hover:bg-gray-400"
                >
                  Hủy
                </button>
              </div>
            </form>
          </div>
        </div>
      )}

       {/* Popup xóa */}
       {showDeletePopup && (
        <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
          <div className="bg-white p-8 w-1/2 rounded shadow-lg">
          <h2 className="text-2xl font-bold mb-4">Xác nhận Xóa Chương</h2>
          <p className="mb-4">Bạn có chắc muốn xóa chương này?</p>
            <form onSubmit={handleDelete} className="space-y-4">
              <div>
                <p className="font-semibold">
                  ID: <strong>{chuong.chuong_id}</strong>
                </p>
              </div>
              <div>
                <p className="font-semibold">
                  ID Môn Học: <strong>{chuong.monhoc_id}</strong> 
                </p>
              </div>
              <div>
                <p className="font-semibold">
                  Số Chương: <strong>{chuong.chuong_number}</strong> 
                </p>
              </div>
              <div>
                <p className="font-semibold">
                  Tên Chương: <strong>{chuong.chuong_name}</strong>
                </p>
              </div>
              <div className="flex justify-end">
                <button
                  type="submit"
                  className="bg-red-400 text-white px-4 py-2 rounded hover:bg-red-500"
                >
                  Xác nhận xóa
                </button>
                <button
                  onClick={closeDeletePopup}
                  className="bg-gray-300 text-gray-700 px-4 py-2 rounded ml-4 hover:bg-gray-400"
                >
                  Hủy
                </button>
              </div>
            </form>
          </div>
        </div>
      )}
    </>
  )
}

export default AdminChuong