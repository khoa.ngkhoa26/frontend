import { useEffect, useState } from "react"

import { ArrayDeThiMonHoc, DeThiMonHoc } from "../../utils/types"
import apiDethimonhoc from "../../utils/api/dethimonhoc"
import ApiMessageEnum from "../../utils/apimessage"
import chuong from "../../utils/api/chuong"

function AdminDeThiMonHoc(): JSX.Element {
  const [dethimonhocAll, setDethimonhocAll] = useState<ArrayDeThiMonHoc>(undefined)
  const [showEditPopup, setShowEditPopup] = useState<boolean>(false)
  const [showDeletePopup, setShowDeletePopup] = useState<boolean>(false)

  // Thêm 
  const [deThiMonHoc, setDeThiMonHoc] = useState<DeThiMonHoc>({
    dethimonhoc_id: 1,
    dethimonhoc_name: '',
    dethimonhoc_questions: 1,
    dethimonhoc_real: '',
    dethimonhoc_time: 1
  })

  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      const data: ArrayDeThiMonHoc = await apiDethimonhoc.getAll(10, 0)
      if (data !== undefined)
      setDethimonhocAll(data)
    }

    fetchData()
  }, [])

  // Chỉnh sửa
  const openEditPopup = (dethimonhoc: DeThiMonHoc): void => {
    setDeThiMonHoc(dethimonhoc)
    setShowEditPopup(true)
  }
  const closeEditPopup = () => {
    setShowEditPopup(false)
  }
  const handleEdit= async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault()

    const message: string = await apiDethimonhoc.update(deThiMonHoc.dethimonhoc_id ,deThiMonHoc.dethimonhoc_name, deThiMonHoc.dethimonhoc_questions, 
      deThiMonHoc.dethimonhoc_real, deThiMonHoc.dethimonhoc_time)
    if (message === ApiMessageEnum.UPDATE_DETHIMONHOC_SUCCESS) {
      setDeThiMonHoc({
        dethimonhoc_id: 0,
        dethimonhoc_name: '',
        dethimonhoc_questions: 0,
        dethimonhoc_real: '',
        dethimonhoc_time: 0
      })
      setShowEditPopup(false)
      alert("Cập nhật thành công")

      const newData: ArrayDeThiMonHoc = await apiDethimonhoc.getAll(10,0)
      if (newData !== undefined){
        setDethimonhocAll(newData)
      }
    }
  }

  // Xóa
  const openDeletePopup = (dethimonhoc) => {
    setDeThiMonHoc(dethimonhoc)
    setShowDeletePopup(true)
  }
  const closeDeletePopup = () => {
    setShowDeletePopup(false)
  }
  const handleDelete = async () => {
    // xử lý xóa
  }

  const onChangeDeThiName = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setDeThiMonHoc({ ...deThiMonHoc, dethimonhoc_name: event.target.value })
  }
  const onChangeDeThiReal = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setDeThiMonHoc({ ...deThiMonHoc, dethimonhoc_real: event.target.value })
  }
  const onChangeDeThiCauHoi = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const numericValue = event.target.value.replace(/\D/g, "")
    if (parseInt(numericValue, 10) < 1) {
      setDeThiMonHoc({...deThiMonHoc, dethimonhoc_questions: 1})
      return
    }

    setDeThiMonHoc({...deThiMonHoc, dethimonhoc_questions: parseInt(numericValue, 10)})
  }
  const onChangeDeThiTime = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const numericValue = event.target.value.replace(/\D/g, "")
    if (parseInt(numericValue, 10) < 1) {
      setDeThiMonHoc({...deThiMonHoc, dethimonhoc_time: 1})
      return
    }

    setDeThiMonHoc({...deThiMonHoc, dethimonhoc_time: parseInt(numericValue, 10)})
  }

  // chọn chức năng cho đề thi chính dropdow true or flase
  const handleSelectChange = (e) => {
    const newValue = e.target.value === 'true' // Chuyển đổi giá trị từ chuỗi sang boolean
    setDeThiMonHoc({
      ...editingDeThi,
      dethimonhoc_real: newValue,
   })
  }

  return (
    <>
      {/*Nội dung trong trang */}
      <div className="flex justify-between items-center mb-4">
        <h1 className="text-black text-2xl font-bold">Quản lý Đề thi môn học</h1>
      </div>

      <div className="table-wrapper" style={{ maxHeight: '550px', overflowY: 'auto' }}>
        <table className="w-full text-center mt-6 shadow-lg rounded-lg">
          <thead className="text-center bg-gray-700 shadow-lg rounded-lg">
            <tr className="text-white">
              <th scope="col" className="px-6 py-3">STT</th>
              <th scope="col" className="px-6 py-3">ID</th>
              <th scope="col" className="px-6 py-3">Tên</th>
              <th scope="col" className="px-6 py-3">Câu hỏi</th>
              <th scope="col" className="px-6 py-3">Đề thi chính</th>
              <th scope="col" className="px-6 py-3">Thời gian</th>
              <th scope="col" className="px-6 py-3">Action</th>
            </tr>
          </thead>

          <tbody>
            { dethimonhocAll !== undefined && dethimonhocAll.length > 0
              ?
              (dethimonhocAll.map((dethimonhoc: DeThiMonHoc, thutu: number) => (
                <tr
                  key={dethimonhoc.dethimonhoc_id}
                  className={
                    `${thutu % 2 === 0
                    ? 'even:bg-gray-50 even:dark:bg-gray-800'
                    : 'odd:bg-white odd:dark:bg-gray-900'}
                    border-b hover:bg-gray-100 hover:cursor-pointers`
                  }
                >
                  <td className="px-6 py-4 font-bold">{thutu + 1}</td>
                  <td className="px-6 py-4">{dethimonhoc.dethimonhoc_id}</td>
                  <td className="px-6 py-4">{dethimonhoc.dethimonhoc_name}</td>
                  <td className="px-6 py-4">{dethimonhoc.dethimonhoc_questions}</td>
                  <td className="px-6 py-4">{dethimonhoc.dethimonhoc_real + ""}</td>
                  <td className="px-6 py-4">{dethimonhoc.dethimonhoc_time}</td>
                  <td className="px-6 py-4">
                    <button
                      onClick={openEditPopup.bind(null, dethimonhoc)}
                      className="bg-gray-700 transition duration-300 ease-in-out transform 
                      hover:scale-110 hover:bg-sky-400 
                      text-white py-2 px-4 rounded-lg mr-2"
                    >
                      Sửa
                    </button>
                    <button
                    onClick={openDeletePopup.bind(null, dethimonhoc)}
                      className="bg-gray-700 transition duration-300 ease-in-out transform 
                      hover:scale-110 hover:bg-red-400 
                      text-white py-2 px-4 rounded-lg"
                    >
                      Xóa
                    </button>
                  </td>
                </tr>
              )))
              :
              (
                <tr>
                  <td colSpan={6}>Không có dữ liệu. Vui lòng thử lại sau</td>
                </tr>
              )
            }
          </tbody>
        </table>
      </div>

       {/* Popup sửa */}
       {showEditPopup && (
        <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
          <div className="bg-white p-8 w-1/2 rounded shadow-lg">
            <form onSubmit={handleEdit}>
            <h2 className="text-2xl font-bold mb-4">Sửa Đề Thi Môn Học</h2>
            <div className="mt-2">
              <label className="text-black">
                ID:
              </label>
              <input
                type="text"
                value={deThiMonHoc.dethimonhoc_id}
                disabled
                className="border border-gray-300 rounded px-3 py-2 w-full bg-gray-100"
              />
            </div>
            <div className="mt-2">
              <label className="text-black">
                Tên:
              </label>
              <input
                type="text"
                value={deThiMonHoc.dethimonhoc_name}
                onChange={onChangeDeThiName }
                className="border border-gray-300 rounded px-3 py-2 w-full"
              />
            </div>
            <div className="mt-2">
              <label className="text-black">
                Số lượng câu hỏi:
              </label>
              <input
                type="text"
                value={deThiMonHoc.dethimonhoc_questions}
                onChange={onChangeDeThiCauHoi}
                className="border border-gray-300 rounded px-3 py-2 w-full"
              />
            </div>
            <div className="mt-2">
              <label className="text-black">Đề thi chính:</label>
              <select
                value={deThiMonHoc.dethimonhoc_real.toString()} // Chuyển đổi giá trị thành chuỗi để chọn trong select
                onChange={handleSelectChange}
                className="border border-gray-300 rounded px-3 py-2 w-full"
              >
                <option value="true">True</option>
                <option value="false">False</option>
              </select>
            </div>
            <div className="mt-2">
              <label className="text-black">
                Thời gian:
              </label>
              <input
                type="text"
                value={deThiMonHoc.dethimonhoc_time}
                onChange={onChangeDeThiTime}
                className="border border-gray-300 rounded px-3 py-2 w-full"
              />
            </div>
            <div className="flex justify-end mt-5">
              <button
                type="submit"
                className="bg-sky-400 text-white px-4 py-2 rounded hover:bg-sky-500"
              >
                Lưu
              </button>
              <button
                onClick={closeEditPopup}
                className="bg-gray-300 text-gray-700 px-4 py-2 rounded ml-4 hover:bg-gray-400"
              >
                Hủy
              </button>
            </div>
            </form>
          </div>
        </div>
      )}

      {/* Popup xóa */}
      {showDeletePopup && (
        <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
          <div className="bg-white p-8 w-1/2 rounded shadow-lg">
            <h2 className="text-2xl font-bold mb-4">Xóa Đề Thi Môn Học</h2>
            <form onSubmit={handleDelete}>
              <div className="mt-2">
                <p>
                  ID:
                  <strong> {deThiMonHoc.dethimonhoc_id}</strong>
                </p>
                <p>
                  Tên: 
                  <strong> {deThiMonHoc.dethimonhoc_name}</strong>
                </p>
                <p>
                  Số lượng câu hỏi:
                  <strong> {deThiMonHoc.dethimonhoc_questions}</strong>
                </p>
                <p>
                  Đề thi chính:
                  <strong> {deThiMonHoc.dethimonhoc_real}</strong>
                </p>
                <p>
                  Thời gian:  
                  <strong> {deThiMonHoc.dethimonhoc_time}</strong>
                </p>
              </div>
              <div className="flex justify-end mt-5">
                <button
                  type="submit"
                  className="bg-red-500 text-white px-4 py-2 rounded hover:bg-red-600"
                >
                  Xóa
                </button>
                <button
                  onClick={closeDeletePopup}
                  className="bg-gray-300 text-gray-700 px-4 py-2 rounded ml-4 hover:bg-gray-400"
                >
                  Hủy
                </button>
              </div>
            </form>
          </div>
        </div>
      )}
    </>
  )
}

export default AdminDeThiMonHoc