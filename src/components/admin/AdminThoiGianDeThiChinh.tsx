import { useEffect, useState } from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCirclePlus } from "@fortawesome/free-solid-svg-icons"

import { ArrayThoiGianDeThiChinh, ThoiGianDeThiChinh } from "../../utils/types"
import apiThoigiandethichinh from "../../utils/api/thoigiandethichinh"

function AdminThoiGianDeThiChinh(): JSX.Element {
  const [thoigiandethichinhAll, setThoigiandethichinhAll] = useState<ArrayThoiGianDeThiChinh>(undefined)

  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      const data: ArrayThoiGianDeThiChinh = await apiThoigiandethichinh.getAll(10, 0)
      if (data !== undefined)
        setThoigiandethichinhAll(data)
    }

    fetchData()
  }, [])

  return (
    <>
      <div className="flex justify-between items-center mb-4">
        <h1 className="text-black text-2xl font-bold">Quản lý thời gian đề thi chính</h1>
        <button className="bg-gray-700 transition-all ease-in-out hover:bg-green-400
                          text-white font-bold py-2 px-4 rounded flex items-center gap-2 mr-4">
          <FontAwesomeIcon icon={faCirclePlus} className="w-5 h-5" />
          <span>Thêm</span>
        </button>
      </div>

      <table className="text-center w-full mt-6 mr-10 shadow-lg rounded-lg">
        <thead className="text-center bg-gray-700 shadow-lg rounded-lg">
          <tr className="text-white">
            <th scope="col" className="px-6 py-3">STT</th>
            <th scope="col" className="px-6 py-3">ID</th>
            <th scope="col" className="px-6 py-3">ID Đề thi môn học</th>
            <th scope="col" className="px-6 py-3">Ngày thi</th>
            <th scope="col" className="px-6 py-3">Action</th>
          </tr>
        </thead>

        <tbody>
          { thoigiandethichinhAll !== undefined && thoigiandethichinhAll.length > 0
            ?
            (thoigiandethichinhAll.map((thoigiandethichinh: ThoiGianDeThiChinh, thutu: number) => (
              <tr
                key={thoigiandethichinh.thoigiandethichinh_id}
                className={
                  `${thutu % 2 === 0
                  ?
                  'even:bg-gray-50 even:dark:bg-gray-800'
                  :
                  'odd:bg-white odd:dark:bg-gray-900'} border-b hover:bg-gray-100 hover:cursor-pointers`}>
                <td className="px-6 py-4 font-bold">{thutu + 1}</td>
                <td className="px-6 py-4">{thoigiandethichinh.thoigiandethichinh_id}</td>
                <td className="px-6 py-4">{thoigiandethichinh.dethimonhoc_id}</td>
                <td className="px-6 py-4">{thoigiandethichinh.thoigiandethichinh_time}</td>
                <td className="px-6 py-4">
                <button
                  className="bg-gray-700 transition-all ease-in-out hover:bg-sky-400 text-white py-2 px-4 rounded-lg mr-2"
                >
                  Sửa
                </button>
                <button
                  className="bg-gray-700 transition-all ease-in-out hover:bg-red-400 text-white py-2 px-4 rounded-lg"
                >
                  Xóa
                </button>
                </td>
              </tr>
            )))
            :
            (
              <tr>
                <td colSpan={5}>Không có dữ liệu. Vui lòng thử lại sau</td>
              </tr>
            )
          }
        </tbody>
      </table>
    </>
  )
}

export default AdminThoiGianDeThiChinh