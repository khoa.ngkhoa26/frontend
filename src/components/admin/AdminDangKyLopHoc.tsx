import { useEffect, useState } from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCirclePlus } from "@fortawesome/free-solid-svg-icons"

import { ArrayDangKyLopHoc, DangKyLopHoc } from "../../utils/types"
import apiDangkylophoc from "../../utils/api/dangkylophoc"

import { ArrayLopHoc, LopHoc } from "../../utils/types"
import apiLophoc from "../../utils/api/lophoc"

import { ArraySinhVien, SinhVien } from "../../utils/types"
import apiSinhvien from "../../utils/api/sinhvien"

import ApiMessageEnum from "../../utils/apimessage"

function AdminDangKyLopHoc(): JSX.Element {
  const [dangkylophocAll, setDangkylophocAll] = useState<ArrayDangKyLopHoc>(undefined)

  const [showAddPopup, setShowAddPopup] = useState<boolean>(false)
  const [showEditPopup, setShowEditPopup] = useState<boolean>(false)
  const [showDeletePopup, setShowDeletePopup] = useState<boolean>(false)

  const [lopHocList, setLopHocList] = useState<Array<string>>([])
  const [sinhVienList, setSinhVienList] = useState<Array<string>>([])

  const [dangKyLopHoc, setDangKyLopHoc] = useState<DangKyLopHoc>({
    dangkylophoc_id: 1,
    sinhvien_id: 1,
    lophoc_id: 1
  })

  // Thêm
  const openAddPopup = () => {
    setShowAddPopup(true)
  }
  const closeAddPopup = () => {
    setShowAddPopup(false)
  }
  const handleAdd = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault()
  
    const message: string = await apiDangkylophoc.insert(dangKyLopHoc.sinhvien_id, dangKyLopHoc.lophoc_id,)
    if (message === ApiMessageEnum.INSERT_DANGKYLOPHOC_SUCCESS) {
      setShowAddPopup(false)
      alert('Thêm thành công')
      
      // Reload lại dữ liệu sau khi thêm thành công
      const newData: ArrayDangKyLopHoc = await apiDangkylophoc.getAll(10, 0)
      if (newData !== undefined) {
        setDangkylophocAll(newData)
      }
    }
  }

  // Sửa
  const openEditPopup = (dangkylophoc: DangKyLopHoc): void => {
    setDangKyLopHoc(dangkylophoc)
    setShowEditPopup(true)
  }
  const closeEditPopup = () => {
    setShowEditPopup(false)
  }
  const handleEdit  = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault()

    const message: string = await apiDangkylophoc.update(dangKyLopHoc.dangkylophoc_id, dangKyLopHoc.sinhvien_id, dangKyLopHoc.lophoc_id )
    if (message === ApiMessageEnum.UPDATE_DANGKYLOPHOC_SUCCESS) {
      setDangKyLopHoc({
        dangkylophoc_id: 0,
        lophoc_id: 0,
        sinhvien_id: 0
      })
      setShowEditPopup(false)
      alert("Cập nhật thành công")

      const newData: ArrayDangKyLopHoc = await apiDangkylophoc.getAll(10, 0)
      if (newData !== undefined){
        setDangkylophocAll(newData)
      }
    }
  }

  // Xóa
  const openDeletePopup = (dangkylophoc: DangKyLopHoc): void => {
    setDangKyLopHoc(dangkylophoc)
    setShowDeletePopup(true)
  }
  
  const closeDeletePopup = () => {
    setShowDeletePopup(false)
  }
  const handleDelete = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault()
  
    const message: string = await apiDangkylophoc.remove(dangKyLopHoc.dangkylophoc_id)
    console.log(message)
    if (message === ApiMessageEnum.REMOVE_DANGKYLOPHOC_SUCCESS) {
      setDangKyLopHoc({
        dangkylophoc_id: 1,
        lophoc_id: 1,
        sinhvien_id: 1
      })
      setShowDeletePopup(false)
      alert("Xóa thành công!")

      const newData: ArrayDangKyLopHoc = await apiDangkylophoc.getAll(10,0)
      if (newData !== undefined){
        setDangkylophocAll(newData)
      }
    }
  }

  const onChangeLopHocId = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    const numericValue = event.target.value.replace(/\D/g, "")
    if (parseInt(numericValue, 10) < 1) {
      setDangKyLopHoc({...dangKyLopHoc, lophoc_id: 1})
      return
    }
    
    setDangKyLopHoc({...dangKyLopHoc, lophoc_id: parseInt(numericValue, 10)})
  }
  const onChangeDangKySinhVienId = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    const numericValue = event.target.value.replace(/\D/g, "")
    if (parseInt(numericValue, 10) < 1) {
      setDangKyLopHoc({...dangKyLopHoc, sinhvien_id: 1})
      return
    }
    
    setDangKyLopHoc({...dangKyLopHoc, sinhvien_id: parseInt(numericValue, 10)})
  }  
  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      const data: ArrayDangKyLopHoc = await apiDangkylophoc.getAll(10, 0)
      if (data !== undefined)
      setDangkylophocAll(data)
    }

    fetchData()
  }, [])

   // Lấy thông tin lớp học
   useEffect(() => {
    const fetchLopHoc = async (): Promise<void> => {
      try {
        const data: ArrayLopHoc = await apiLophoc.getAll(10, 0)
  
        if (data !== undefined) {
          // Lấy danh sách tên tài khoản từ dữ liệu người dùng
          const idLopHoc = data.map((lophoc: LopHoc) => lophoc.lophoc_id)
          setLopHocList(idLopHoc)
        }
      } catch (error) {
        console.error('Error fetching account list:', error)
      }
    }
  
    fetchLopHoc()
  }, [])
  
  // Lấy thông tin sinh viên
  useEffect(() => {
    const fetchSinhVien = async (): Promise<void> => {
      try {
        const data: ArraySinhVien = await apiSinhvien.getAll(10, 0)
  
        if (data !== undefined) {
          // Lấy danh sách tên tài khoản từ dữ liệu người dùng
          const idSinhVien = data.map((sinhvien: SinhVien) => sinhvien.sinhvien_id)
          setSinhVienList(idSinhVien)
        }
      } catch (error) {
        console.error('Error fetching account list:', error)
      }
    }
  
    fetchSinhVien()
  }, [])

  return (
    <>
      <div className="flex justify-between items-center mb-4">
        <h1 className="text-black text-2xl font-bold">Quản lý đăng ký lớp học</h1>
        <button 
        onClick={openAddPopup}
        className="bg-gray-700 transition duration-300 ease-in-out transform 
        hover:scale-110 hover:bg-green-400 text-white font-bold py-2 px-4 rounded flex items-center gap-2 mr-4">
          <FontAwesomeIcon icon={faCirclePlus} className="w-5 h-5" />
          <span>
            Thêm
          </span>
        </button>
      </div>

      <div className="table-wrapper" style={{ maxHeight: '550px', overflowY: 'auto' }}>
        <table className="w-full text-center mt-6 shadow-lg rounded-lg">
        <thead className="text-center bg-gray-700 shadow-lg rounded-lg">
          <tr className="text-white">
            <th scope="col" className="px-6 py-3">STT</th>
            <th scope="col" className="px-6 py-3">ID</th>
            <th scope="col" className="px-6 py-3">ID Sinh viên</th>
            <th scope="col" className="px-6 py-3">ID Lớp học</th>
            <th scope="col" className="px-6 py-3">Action</th>
          </tr>
        </thead>

        <tbody>
          { dangkylophocAll !== undefined && dangkylophocAll.length > 0
            ?
            (dangkylophocAll.map((dangkylophoc: DangKyLopHoc, thutu: number) => (
              <tr
                key={dangkylophoc.dangkylophoc_id}
                className={
                  `${thutu % 2 === 0
                  ? 'even:bg-gray-50 even:dark:bg-gray-800'
                  : 'odd:bg-white odd:dark:bg-gray-900'} 
                  border-b hover:bg-gray-100 hover:cursor-pointers`}>
                <td className="px-6 py-4 font-bold">{thutu + 1}</td>
                <td className="px-6 py-4">{dangkylophoc.dangkylophoc_id}</td>
                <td className="px-6 py-4">{dangkylophoc.sinhvien_id}</td>
                <td className="px-6 py-4">{dangkylophoc.lophoc_id}</td>
                <td className="px-6 py-4">
                <button 
                onClick={openEditPopup.bind(null, dangkylophoc)}
                className="bg-gray-700 transition duration-300 ease-in-out transform 
                hover:scale-110 hover:bg-sky-400 text-white py-2 px-4 rounded-lg mr-2">
                  Sửa
                </button>
                <button 
                onClick={openDeletePopup.bind(null, dangkylophoc)}
                className="bg-gray-700 transition duration-300 ease-in-out transform 
                hover:scale-110 hover:bg-red-400 text-white py-2 px-4 rounded-lg">
                  Xóa
                </button>
                </td>
              </tr>
            )))
            :
            (
              <tr>
                <td colSpan={5}>Không có dữ liệu. Vui lòng thử lại sau</td>
              </tr>
            )
          }
        </tbody>
      </table>
    </div>

    {/* Popup thêm */}
    {showAddPopup && (
        <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
          <div className="bg-white p-8 w-96 rounded shadow-lg">
            <h2 className="text-2xl font-bold mb-4">Thêm Đăng Ký Lớp Học</h2>
            <form onSubmit={handleAdd} className="space-y-4">
              <div>
                <label htmlFor="sinhvien_id" className="block font-semibold">
                  ID Sinh Viên:
                </label>
                <select
                  value={dangKyLopHoc.sinhvien_id}
                  onChange={onChangeDangKySinhVienId}
                  className="border border-gray-300 rounded px-3 py-2 w-full "
                >
                  {sinhVienList.map((sinhvien_id) => (
                    <option key={sinhvien_id} value={sinhvien_id}>
                      {sinhvien_id}
                    </option>
                  ))}
                </select>
              </div>
              <div>
                <label htmlFor="lophoc_id" className="block font-semibold">
                  ID Lớp Học:
                </label>
                <select
                  value={dangKyLopHoc.lophoc_id}
                  onChange={onChangeLopHocId}
                  className="border border-gray-300 rounded px-3 py-2 w-full "
                >
                  {lopHocList.map((lophoc_id) => (
                    <option key={lophoc_id} value={lophoc_id}>
                      {lophoc_id}
                    </option>
                  ))}
                </select>
              </div>
              <div className="flex justify-end">
                <button
                  type="submit"
                  className="bg-sky-400 text-white px-4 py-2 rounded hover:bg-sky-500"
                >
                  Thêm Đăng Ký
                </button>
                <button
                  onClick={closeAddPopup}
                  className="bg-gray-300 text-gray-700 px-4 py-2 rounded ml-4 hover:bg-gray-400"
                >
                  Hủy
                </button>
              </div>
            </form>
          </div>
        </div>
      )}

       {/* Popup chỉnh sửa */}
       {showEditPopup && (
        <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
          <div className="bg-white p-8 w-96 rounded shadow-lg">
            <h2 className="text-2xl font-bold mb-4">Chỉnh Sửa Đăng Ký Lớp Học</h2>
            <form onSubmit={handleEdit} className="space-y-4">
            <div>
                <label htmlFor="edit_dangkylophoc_id" className="block font-semibold">
                  ID Đăng Ký Lớp Học:
                </label>
                <input
                  type="text"
                  id="edit_dangkylophoc_id"
                  value={dangKyLopHoc.dangkylophoc_id}
                  disabled
                  className="border border-gray-300 rounded px-3 py-2 w-full"
                />
              </div>
              <div>
                <label htmlFor="edit_sinhvien_id" className="block font-semibold">
                  ID Sinh Viên:
                </label>
                <select
                  value={dangKyLopHoc.sinhvien_id}
                  onChange={onChangeDangKySinhVienId}
                  className="border border-gray-300 rounded px-3 py-2 w-full "
                >
                  {sinhVienList.map((sinhvien_id) => (
                    <option key={sinhvien_id} value={sinhvien_id}>
                      {sinhvien_id}
                    </option>
                  ))}
                </select>
              </div>
              <div>
                <label htmlFor="edit_lophoc_id" className="block font-semibold">
                  ID Lớp Học:
                </label>
                <select
                  value={dangKyLopHoc.lophoc_id}
                  onChange={onChangeLopHocId}
                  className="border border-gray-300 rounded px-3 py-2 w-full "
                >
                  {lopHocList.map((lophoc_id) => (
                    <option key={lophoc_id} value={lophoc_id}>
                      {lophoc_id}
                    </option>
                  ))}
                </select>
              </div>
              <div className="flex justify-end">
                <button
                  type="submit"
                  className="bg-sky-400 text-white px-4 py-2 rounded hover:bg-sky-500"
                >
                  Lưu Chỉnh Sửa
                </button>
                <button
                  onClick={closeEditPopup}
                  className="bg-gray-300 text-gray-700 px-4 py-2 rounded ml-4 hover:bg-gray-400"
                >
                  Hủy
                </button>
              </div>
            </form>
          </div>
        </div>
      )}

       {/* Popup xóa */}
       {showDeletePopup && (
        <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
          <div className="bg-white p-8 w-1/2 rounded shadow-lg">
            <h2 className="text-2xl font-bold mb-4">Xóa Đăng Ký Lớp Học</h2>
            <form onSubmit={handleDelete}>
              <p>
                ID Đăng Ký Lớp Học: 
                <strong> {dangKyLopHoc.dangkylophoc_id}</strong>
              </p>
              <p>
                ID Sinh Viên: 
                <strong> {dangKyLopHoc.sinhvien_id}</strong> 
              </p>
              <p>
                ID Lớp Học: 
                <strong> {dangKyLopHoc.lophoc_id}</strong>
              </p>
              <div className="flex justify-end">
                <button
                  type="submit"
                  className="bg-red-400 text-white px-4 py-2 rounded hover:bg-red-500"
                >
                  Xác Nhận Xóa
                </button>
                <button
                  onClick={closeDeletePopup}
                  className="bg-gray-300 text-gray-700 px-4 py-2 rounded ml-4 hover:bg-gray-400"
                >
                  Hủy
                </button>
              </div>
            </form>
          </div>
        </div>
      )}
    </>
  )
}

export default AdminDangKyLopHoc