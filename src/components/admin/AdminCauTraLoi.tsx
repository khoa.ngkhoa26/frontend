import { useEffect, useState } from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faAngleLeft, faAngleRight, faAnglesLeft, faAnglesRight, faCirclePlus } from "@fortawesome/free-solid-svg-icons"

import { ArrayCauTraLoi, CauTraLoi, LastOrCount } from "../../utils/types"
import apiCautraloi from "../../utils/api/cautraloi"

import { ArrayCauHoi, CauHoi } from "../../utils/types"
import apiCauhoi from "../../utils/api/cauhoi"

import ApiMessageEnum from "../../utils/apimessage"

interface AdminCauTraLoiProps {
  limit: number
}

function AdminCauTraLoi(props: AdminCauTraLoiProps): JSX.Element {
  // Data chứa dữ liệu
  const [cautraloiAll, setCautraloiAll] = useState<ArrayCauTraLoi>(undefined)
  const [cautraloiCount, setCautraloiCount] = useState<number>(0)

  // Data quản lý UI/UX
  const [reloadPage, setReloadPage] = useState<boolean>(false)
  const [showInsertPopup, setShowInsertPopup] = useState<boolean>(false)
  const [showUpdatePopup, setShowUpdatePopup] = useState<boolean>(false)
  const [showRemovePopup, setShowRemovePopup] = useState<boolean>(false)

  const [cauHoiList, setCauHoiList] = useState<Array<string>>([])


  const [page, setPage] = useState<number>(1)
  const [lastPage, setLastPage] = useState<number>(1)

  // Data để chỉnh sửa dữ liệu
  const [cautraloi, setCautraloi] = useState<CauTraLoi>({
    cautraloi_id: 1,
    cauhoi_id: 1,
    cautraloi_name: ""
  })

  // Trang
  const goNextPage = (): void => {
    if (page >= lastPage) {
      setPage(lastPage)
      return
    }

    setPage(page+1)
  }
  const goPreviousPage = (): void => {
    if (page < 1) {
      setPage(1)
      return
    }

    setPage(page-1)
  }
  const gotoFirstPage = (): void => {
    setPage(1)
  }
  const gotoLastPage = (): void => {
    setPage(lastPage)
  }

  const handleInputPageChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    // Allow only numeric input
    const numericValue = event.target.value.replace(/\D/g, '')
    if (parseInt(numericValue, 10) >= lastPage) {
      setPage(lastPage)
      return
    }

    setPage(parseInt(numericValue, 10))
  }

  // Thêm
  const onClickOpenInsertPopup = (): void => {
    setShowInsertPopup(true)
  }
  const onClickCloseInsertPopup = (): void => {
    setShowInsertPopup(false)
  }
  const handleInsertSubmit = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault()

    const message: string = await apiCautraloi.insert(cautraloi.cauhoi_id, cautraloi.cautraloi_name)
      if (message === ApiMessageEnum.INSERT_CAUTRALOI_SUCCESS) {
        setReloadPage(true)
        setShowInsertPopup(false)
        alert("Thêm Thành Công")
      }
  }

  // Chỉnh sửa
  const onClickOpenUpdatePopup = (cautraloi: CauTraLoi): void => {
    setCautraloi(cautraloi)
    setShowUpdatePopup(true)
  }
  const onClickCloseUpdatePopup = () => {
    setShowUpdatePopup(false)
  }
  const handleUpdateSubmit = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault()

    const message: string = await apiCautraloi.update(cautraloi.cautraloi_id, cautraloi.cauhoi_id, cautraloi.cautraloi_name)
    if (message === ApiMessageEnum.UPDATE_CAUTRALOI_SUCCESS) {
      setCautraloi({
        cautraloi_id: 0,
        cauhoi_id: 0,
        cautraloi_name: ""
      })
      setReloadPage(true)
      setShowUpdatePopup(false)
      alert("Cập nhật thành công")
    }
  }

  // Xóa
  const onClickOpenRemovePopup = (cautraloi: CauTraLoi): void => {
    setCautraloi(cautraloi)
    setShowRemovePopup(true)
  }
  const onClickCloseRemovePopup = (): void => {
    setShowRemovePopup(false)
  }
  const handleRemoveSubmit = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault()
  
    const message: string = await apiCautraloi.remove(cautraloi.cautraloi_id)
    console.log(message)
    if (message === ApiMessageEnum.REMOVE_CAUTRALOI_SUCCESS) {
      setCautraloi({
        cautraloi_id: 1,
        cauhoi_id: 1,
        cautraloi_name: ""
      })
      setReloadPage(true)
      setShowRemovePopup(false)
      alert("Xóa thành công!")
    }
  }

  const onChangeCauhoiid = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    // Allow only numeric input
    const numericValue = event.target.value.replace(/\D/g, "")
    if (parseInt(numericValue, 10) < 1) {
      setCautraloi({...cautraloi, cauhoi_id: 1})
      return
    }

    setCautraloi({...cautraloi, cauhoi_id: parseInt(numericValue, 10)})
  }
  const onChangeCautraloiname = (event: React.ChangeEvent<HTMLTextAreaElement>): void => {
    setCautraloi({ ...cautraloi, cautraloi_name: event.target.value })
  }

  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      const data: ArrayCauTraLoi = await apiCautraloi.getAll(props.limit, page-1)
      if (data !== undefined)
        setCautraloiAll(data)

      const countdata: LastOrCount = await apiCautraloi.getCount()
      if (countdata !== undefined) {
        setLastPage(Math.ceil(countdata as number / props.limit))

        setCautraloiCount(countdata)
      }
    }

    fetchData()
    if (reloadPage === true)
      setReloadPage(false)
  }, [props.limit, page, reloadPage])

  // Lấy thông tin câu hỏi
  useEffect(() => {
    const fetchCauHoi = async (): Promise<void> => {
      try {
        const data: ArrayCauHoi = await apiCauhoi.getAll(10, 0)
  
        if (data !== undefined) {
          // Lấy danh sách tên tài khoản từ dữ liệu người dùng
          const idCauHoi = data.map((cauhoi: CauHoi) => cauhoi.cauhoi_id)
          setCauHoiList(idCauHoi)
        }
      } catch (error) {
        console.error('Error fetching account list:', error)
      }
    }
  
    fetchCauHoi()
  }, [])



  return (
    <>
      {/* Nội dung trong trang */}
      <div className="flex justify-between items-center mb-2">
        <h1 className="text-black text-2xl font-bold">Quản lý câu trả lời</h1>
        <button   
          onClick={onClickOpenInsertPopup}
          className="bg-gray-700 transition duration-300 ease-in-out transform 
          hover:scale-110 hover:bg-green-400 text-white font-bold py-2 px-4 
          rounded flex items-center gap-2 mr-4"
        >
          <FontAwesomeIcon icon={faCirclePlus} className="w-5 h-5" />
          <span>
            Thêm
          </span>
        </button>
      </div>

      <div className="table-wrapper" style={{ maxHeight: '500px', overflowY: 'auto' }}>
        <table className="w-full text-center mt-6 shadow-lg rounded-lg">
          <thead className="text-center bg-gray-700 shadow-lg rounded-lg">
            <tr className="text-white">
              <th scope="col" className="px-6 py-3">STT</th>
              <th scope="col" className="px-6 py-3">ID</th>
              <th scope="col" className="px-6 py-3">ID Câu hỏi</th>
              <th scope="col" className="px-6 py-3">Nội dung câu trả lời</th>
              <th scope="col" className="px-6 py-3">Action</th>
            </tr>
          </thead>

          <tbody>
            { cautraloiAll !== undefined && cautraloiAll.length > 0
              ?
              (cautraloiAll.map((cautraloi: CauTraLoi, thutu: number) => (
                <tr
                key={cautraloi.cautraloi_id}
                className={
                  `${thutu % 2 === 0
                    ? 'even:bg-gray-50 even:dark:bg-gray-800'
                    : 'odd:bg-white odd:dark:bg-gray-900'}
                    border-b hover:bg-gray-100 hover:cursor-pointers`
                  }
                  >
                  <td className="px-6 py-4 font-bold">{thutu + 1}</td>
                  <td className="px-6 py-4">{cautraloi.cautraloi_id}</td>
                  <td className="px-6 py-4">{cautraloi.cauhoi_id}</td>
                  <td className="px-6 py-4">{cautraloi.cautraloi_name}</td>
                  <td className="px-6 py-4">
                  <button
                    className="bg-gray-700 transition duration-300 ease-in-out transform 
                    hover:scale-110 hover:bg-sky-400 text-white py-2 px-4 rounded-lg mr-2 "
                    onClick={onClickOpenUpdatePopup.bind(null, cautraloi)}
                  >
                    Sửa
                  </button>
                  <button
                    className="bg-gray-700 transition duration-300 ease-in-out transform 
                    hover:scale-110 hover:bg-red-400 text-white py-2 px-4 rounded-lg"
                    onClick={onClickOpenRemovePopup.bind(null, cautraloi)}
                  >
                    Xóa
                  </button>
                  </td>
                </tr>
              )))
              :
              (
                <tr>
                  <td colSpan={5}>Không có dữ liệu. Vui lòng thử lại sau</td>
                </tr>
              )
            }
          </tbody>
        </table>
      </div>

      {/* Chuyển trang */}
      <div className="flex justify-center items-center space-x-2 mt-12">
        <button onClick={gotoFirstPage} className="focus:outline-none">
          <div className="inline-flex items-center justify-center transition duration-300 ease-in-out transform 
            hover:scale-110 hover:bg-black rounded-full bg-sky-400 text-white w-8 h-8">
            <FontAwesomeIcon icon={faAnglesLeft} />
          </div>
        </button>
        <button onClick={goPreviousPage} className="focus:outline-none">
          <div className="inline-flex items-center justify-center transition duration-300 ease-in-out transform 
            hover:scale-110 hover:bg-black rounded-full bg-sky-400 text-white w-8 h-8">
            <FontAwesomeIcon icon={faAngleLeft} />
          </div>
        </button>
        <p className="px-10 font-bold">
          Trang 
          <input type="text" className="w-6 ml-2" 
          value={page} onChange={handleInputPageChange} />
        </p>
        <button onClick={goNextPage} className="focus:outline-none">
          <div className="inline-flex items-center justify-center transition duration-300 ease-in-out transform 
            hover:scale-110 hover:bg-black rounded-full bg-sky-400 text-white w-8 h-8">
            <FontAwesomeIcon icon={faAngleRight} />
          </div>
        </button>
        <button onClick={gotoLastPage} className="focus:outline-none">
          <div className="inline-flex items-center justify-center transition duration-300 ease-in-out transform 
            hover:scale-110 hover:bg-black rounded-full bg-sky-400 text-white w-8 h-8">
            <FontAwesomeIcon icon={faAnglesRight} />
          </div>
        </button>
      </div>

      {/* Popup thêm */}
      {showInsertPopup && (
      <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
        <div className="bg-white p-8 w-1/2 rounded shadow-lg">
          <h2 className="text-2xl font-bold mb-4">Thêm Câu Trả Lời</h2>
          <form
            onSubmit={handleInsertSubmit}
            className="space-y-4"
          >
            <div>
              <label htmlFor="cauhoi_id" className="block font-semibold">
                ID Câu Hỏi:
              </label>
              <select
                  value={cautraloi.cauhoi_id}
                  onChange={onChangeCauhoiid}
                  className="border border-gray-300 rounded px-3 py-2 w-full "
                >
                  {cauHoiList.map((caihoi_id) => (
                    <option key={caihoi_id} value={caihoi_id}>
                      {caihoi_id}
                    </option>
                  ))}
                </select>
            </div>
            <div>
              <label htmlFor="cautraloi_name" className="block font-semibold">
                Nội Dung Câu Trả Lời:
              </label>
              <textarea
                value={cautraloi.cautraloi_name}
                onChange={onChangeCautraloiname}
                className="border border-gray-300 rounded px-3 py-2 w-full"
                rows={4}
              />
            </div>
            <div className="flex justify-end">
              <button
                type="submit"
                className="bg-sky-400 text-white px-4 py-2 rounded hover:bg-sky-500"
              >
                Thêm
              </button>
              <button
                type="button"
                onClick={onClickCloseInsertPopup}
                className="bg-gray-300 text-gray-700 px-4 py-2 rounded ml-4 hover:bg-gray-400"
              >
                Hủy
              </button>
            </div>
          </form>
        </div>
      </div>
      )}

      {/* Popup chỉnh sửa */}
      {showUpdatePopup && (
        <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
          <div className="bg-white p-8 w-1/2 rounded shadow-lg">
            <h2 className="text-2xl font-bold mb-4">Chỉnh Sửa Câu Trả Lời</h2>
            <form onSubmit={handleUpdateSubmit} className="space-y-4">
              <div>
                <label htmlFor="edit_cautraloi_id" className="block font-semibold">
                  ID:
                </label>
                <input
                  type="text"
                  value={cautraloi.cautraloi_id}
                  disabled
                  className="border border-gray-300 rounded px-3 py-2 w-full bg-gray-100"
                />
              </div>
              <div>
                <label htmlFor="edit_cauhoi_id" className="block font-semibold">
                  ID Câu Hỏi:
                </label>
                <select
                  value={cautraloi.cauhoi_id}
                  onChange={onChangeCauhoiid}
                  className="border border-gray-300 rounded px-3 py-2 w-full "
                >
                  {cauHoiList.map((caihoi_id) => (
                    <option key={caihoi_id} value={caihoi_id}>
                      {caihoi_id}
                    </option>
                  ))}
                </select>
              </div>
              <div>
                <label htmlFor="edit_cautraloi_name" className="block font-semibold">
                  Nội Dung Câu Trả Lời:
                </label>
                <textarea
                  value={cautraloi.cautraloi_name}
                  onChange={onChangeCautraloiname}
                  className="border border-gray-300 rounded px-3 py-2 w-full"
                  rows={4}
                />
              </div>
              <div className="flex justify-end">
                <button
                  type="submit"
                  className="bg-sky-400 text-white px-4 py-2 rounded hover:bg-sky-500"
                >
                  Lưu
                </button>
                <button
                  type="button"
                  onClick={onClickCloseUpdatePopup}
                  className="bg-gray-300 text-gray-700 px-4 py-2 rounded ml-4 hover:bg-gray-400"
                >
                  Hủy
                </button>
              </div>
            </form>
          </div>
        </div>
      )}

      {/* Popup xóa */}
      {showRemovePopup && (
        <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
          <div className="bg-white p-8 w-1/2 rounded shadow-lg">
          <h2 className="text-2xl font-bold mb-4">Xác nhận Xóa Câu Trả Lời</h2>
          <p className="mb-4">Bạn có chắc muốn xóa câu trả lời này?</p>
          <form onSubmit={handleRemoveSubmit}>
            <div className="space-y-4">
              <p>
                ID: <strong>{cautraloi.cautraloi_id}</strong>
              </p>
              <p>
                Nội dung câu trả lời: <strong>{cautraloi.cautraloi_name}</strong>
              </p>
              <div className="flex justify-end">
                <button
                  type="submit"
                  className="bg-red-500 text-white px-4 py-2 rounded hover:bg-red-600"
                >
                  Xóa
                </button>
                <button
                  type="button"
                  onClick={onClickCloseRemovePopup}
                  className="bg-gray-300 text-gray-700 px-4 py-2 rounded ml-4 hover:bg-gray-400"
                >
                  Hủy
                </button>
              </div>
            </div>
          </form>
          </div>
        </div>
      )}
    </>
  )
}

export default AdminCauTraLoi