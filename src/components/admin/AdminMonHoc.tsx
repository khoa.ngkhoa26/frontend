import { useEffect, useState } from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCirclePlus } from "@fortawesome/free-solid-svg-icons"

import { ArrayMonHoc, MonHoc } from "../../utils/types"
import apiMonHoc from "../../utils/api/monhoc"

import { ArrayLopHoc, LopHoc } from "../../utils/types"
import apiLophoc from "../../utils/api/lophoc"

import ApiMessageEnum from "../../utils/apimessage"

function AdminMonHoc(): JSX.Element {
  const [monhocAll, setMonhocAll] = useState<ArrayMonHoc>(undefined)
  
  const [showEditPopup, setShowEditPopup] = useState<boolean>(false)
  const [showDeletePopup, setShowDeletePopup] = useState<boolean>(false)
  const [showAddPopup, setShowAddPopup] = useState<boolean>(false)
  
  const [lopHocList, setLopHocList] = useState<Array<string>>([])
  
  // Thêm
  const [monHoc, setMonHoc] = useState<MonHoc>({
    monhoc_id: 1,
    lophoc_id: 1,
    monhoc_name: '',
  })

  // Thêm
  const openAddPopup = () => {
    setShowAddPopup(true)
  }
  const closeAddPopup = () => {
    setShowAddPopup(false)
  }
  const handleAdd  = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault()
  
    const message: string = await apiMonHoc.insert(monHoc.lophoc_id, monHoc.monhoc_name)
    if (message === ApiMessageEnum.INSERT_MONHOC_SUCCESS) {
      setShowAddPopup(false)
      alert('Thêm thành công')

      const newData: ArrayMonHoc = await apiMonHoc.getAll(10,0)
      if (newData !== undefined){
        setMonhocAll(newData)
      }
    }
  }

  // Chỉnh sửa
  const openEditPopup = (monhoc: MonHoc): void => {
    setMonHoc(monhoc)
    setShowEditPopup(true)
  }
  const closeEditPopup = () => {
    setShowEditPopup(false)
  }
  const handleEdit  = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault()

    const message: string = await apiMonHoc.update(monHoc.monhoc_id, monHoc.lophoc_id, monHoc.monhoc_name)
    if (message === ApiMessageEnum.UPDATE_MONHOC_SUCCESS) {
      setMonHoc({
        monhoc_id: 0,
        lophoc_id: 0,
        monhoc_name: ""
      })
      setShowEditPopup(false)
      alert("Cập nhật thành công")

      const newData: ArrayMonHoc = await apiMonHoc.getAll(10,0)
      if (newData !== undefined){
        setMonhocAll(newData)
      }
    }
  }

  // Xóa
  const openDeletePopup = (monhoc: MonHoc): void => {
    setMonHoc(monhoc)
    setShowDeletePopup(true)
  }
  const closeDeletePopup = () => {
    setShowDeletePopup(false)
  }
  const handleDelete = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault()
  
    const message: string = await apiMonHoc.remove(monHoc.monhoc_id)
    console.log(message)
    if (message === ApiMessageEnum.REMOVE_MONHOC_SUCCESS) {
      setMonHoc({
        monhoc_id: 1,
        lophoc_id: 1,
        monhoc_name: ""
      })
      setShowDeletePopup(false)
      alert("Xóa thành công!")

      const newData: ArrayMonHoc = await apiMonHoc.getAll(10,0)
      if (newData !== undefined){
        setMonhocAll(newData)
      }
    }
  }

  const onChangeLopHocIdSelect = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    const numericValue = event.target.value.replace(/\D/g, "")
    if (parseInt(numericValue, 10) < 1) {
      setMonHoc({...monHoc, lophoc_id: 1})
      return
    }

    setMonHoc({...monHoc, lophoc_id: parseInt(numericValue, 10)})
  }
  const onChangeMonHocName = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setMonHoc({ ...monHoc, monhoc_name: event.target.value })
  }
  

  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      const data: ArrayMonHoc = await apiMonHoc.getAll(10, 0)
      if (data !== undefined)
      setMonhocAll(data)
    }

    fetchData()
  }, [])

   // Lấy thông tin lớp học
   useEffect(() => {
    const fetchLopHoc = async (): Promise<void> => {
      try {
        const data: ArrayLopHoc = await apiLophoc.getAll(10, 0)
  
        if (data !== undefined) {
          // Lấy danh sách tên tài khoản từ dữ liệu người dùng
          const idLopHoc = data.map((lophoc: LopHoc) => lophoc.lophoc_id)
          setLopHocList(idLopHoc)
        }
      } catch (error) {
        console.error('Error fetching account list:', error)
      }
    }
  
    fetchLopHoc()
  }, [])


  return (
    <>
      {/*Nội dung trong trang */}
      <div className="flex justify-between items-center mb-4">
        <h1 className="text-black text-2xl font-bold">Quản lý môn học</h1>
        <button 
        onClick={openAddPopup}
        className="bg-gray-700 transition duration-300 ease-in-out transform 
        hover:scale-110 hover:bg-green-400 text-white font-bold py-2 px-4 rounded flex items-center gap-2 mr-4">
          <FontAwesomeIcon icon={faCirclePlus} className="w-5 h-5" />
          <span>
            Thêm
          </span>
        </button>
      </div>

      <div className="table-wrapper" style={{ maxHeight: '550px', overflowY: 'auto' }}>
        <table className="w-full text-center mt-6 shadow-lg rounded-lg">
          <thead className="text-center bg-gray-700 shadow-lg rounded-lg">
            <tr className="text-white">
              <th scope="col" className="px-6 py-3">STT</th>
              <th scope="col" className="px-6 py-3">ID</th>
              <th scope="col" className="px-6 py-3">ID Lớp học</th>
              <th scope="col" className="px-6 py-3">Tên môn học</th>
              <th scope="col" className="px-6 py-3">Action</th>
            </tr>
          </thead>

          <tbody>
            { monhocAll !== undefined
              ?
              (monhocAll.map((monhoc: MonHoc, thutu: number) => (
                <tr key={monhoc.monhoc_id} className={`${thutu % 2 === 0 ? 'even:bg-gray-50 even:dark:bg-gray-800' : 'odd:bg-white odd:dark:bg-gray-900'} 
                                              border-b hover:bg-gray-100 hover:cursor-pointers`}>
                  <td className="px-6 py-4 font-bold">{thutu + 1}</td>
                  <td className="px-6 py-4">{monhoc.monhoc_id}</td>
                  <td className="px-6 py-4">{monhoc.lophoc_id}</td>
                  <td className="px-6 py-4">{monhoc.monhoc_name}</td>
                  <td className="px-6 py-4">
                  <button 
                onClick={openEditPopup.bind(null, monhoc)}
                className="bg-gray-700 transition duration-300 ease-in-out transform 
                hover:scale-110 hover:bg-sky-400 text-white py-2 px-4 rounded-lg mr-2">
                    Sửa
                  </button>
                  <button 
                  onClick={openDeletePopup.bind(null, monhoc)}
                  className="bg-gray-700 transition duration-300 ease-in-out transform 
                  hover:scale-110 hover:bg-red-400 text-white py-2 px-4 rounded-lg">
                    Xóa
                  </button>
                  </td>
                </tr>
              )))
              :
              (
                <tr>
                  <td colSpan={5}>Không có dữ liệu. Vui lòng thử lại sau</td>
                </tr>
              )
            }
          </tbody>
        </table>
      </div>

      {/* Popup thêm */}
      {showAddPopup && (
        <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
          <div className="bg-white p-8 w-1/3 rounded shadow-lg">
            <div>
              <p className="text-2xl font-bold mb-4">Thêm môn học mới</p>
              <form onSubmit={handleAdd}>
                  <div className="mb-4">
                    <label className="block text-lg font-medium mb-2">
                      Tên môn học:
                    </label>
                    <input
                      type="text"
                      value={monHoc.monhoc_name}
                      onChange={onChangeMonHocName}
                      className="border border-gray-300 rounded px-3 py-2 w-full"
                    />
                  </div>
                  <div className="flex justify-center">
                    <button
                      type="submit"
                      className="bg-sky-400 text-white px-4 py-2 rounded hover:bg-sky-500 mr-4"
                    >
                      Thêm
                    </button>
                    <button
                      onClick={closeAddPopup}
                      className="bg-gray-300 text-gray-700 px-4 py-2 rounded hover:bg-gray-400"
                    >
                      Hủy
                    </button>
                  </div>
                </form>
            </div>
          </div>
        </div>
      )}

       {/* Popup chỉnh sửa */}
       {showEditPopup && (
        <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
          <div className="bg-white p-8 w-1/3 rounded shadow-lg">
            <form onSubmit={handleEdit}>
              <h2 className="text-2xl font-bold mb-4">Chỉnh sửa môn học</h2>
              <div className="mt-2">
                <label className="text-black">ID:</label>
                <input
                  type="text"
                  value={monHoc.monhoc_id}
                  disabled
                  className="border border-gray-300 rounded px-3 py-2 w-full bg-gray-100"
                />
              </div>
              <div className="mt-2">
                <label className="text-black">ID lớp học:</label>
                  <select
                  value={monHoc.lophoc_id}
                  onChange={onChangeLopHocIdSelect}
                  className="border border-gray-300 rounded px-3 py-2 w-full "
                >
                  {lopHocList.map((lophoc_id) => (
                    <option key={lophoc_id} value={lophoc_id}>
                      {lophoc_id}
                    </option>
                  ))}
                </select>
              </div>
              <div className="mt-2">
                <label className="text-black">Tên môn học:</label>
                <input
                  type="text"
                  value={monHoc.monhoc_name}
                  onChange={onChangeMonHocName}
                  className="border border-gray-300 rounded px-3 py-2 w-full"
                />
              </div>
              <div className="flex justify-end mt-5">
                <button
                  type="submit"
                  className="bg-sky-400 text-white px-4 py-2 rounded hover:bg-sky-500"
                >
                  Lưu
                </button>
                <button
                  onClick={closeEditPopup}
                  className="bg-gray-300 text-gray-700 px-4 py-2 rounded ml-4 hover:bg-gray-400"
                >
                  Hủy
                </button>
              </div>
            </form>
          </div>
        </div>
      )}

       {/* Popup xóa */}
       {showDeletePopup && (
        <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
          <div className="bg-white p-8 w-1/3 rounded shadow-lg">
            <div>
              <p className="text-2xl font-bold mb-4">Xóa môn học</p>
              <p className="text-lg mb-4">
                Bạn có chắc chắn muốn xóa môn học này không?
              </p>
              <form onSubmit={handleDelete}>
                <p className="text-lg font-medium mb-4">
                  ID: <strong> {monHoc.monhoc_id}</strong>
                </p>
                <p className="text-lg font-medium mb-4">
                  Tên môn học: <strong> {monHoc.monhoc_name}</strong>
                </p>
                <div className="flex justify-center">
                  <button
                    type="submit"
                    className="bg-red-500 text-white px-4 py-2 rounded hover:bg-red-600 mr-4"
                  >
                    Xóa
                  </button>
                  <button
                    onClick={closeDeletePopup}
                    className="bg-gray-300 text-gray-700 px-4 py-2 rounded hover:bg-gray-400"
                  >
                    Hủy
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      )}
    </>
  )
}

export default AdminMonHoc