import { useEffect, useState } from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCirclePlus } from "@fortawesome/free-solid-svg-icons"

import { ArrayCauHoi, CauHoi } from "../../utils/types"
import apiCauhoi from "../../utils/api/cauhoi"

interface AdminCaiHoiProps {
  limit: number
}

function AdminCauHoi(props: AdminCaiHoiProps): JSX.Element {
  const [cauhoiAll, setCauhoiAll] = useState<ArrayCauHoi>(undefined)
  const [showInsertPopup, setShowInsertPopup] = useState(false)
  const [editingQuestion, setEditingQuestion] = useState<CauHoi | null>(null) // Sửa
  const [deletingQuestion, setDeletingQuestion] = useState<CauHoi | null>(null) // Xóa
  
  const [newQuestion, setNewQuestion] = useState({
    cauhoi_name: '',
  })  

  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      const data: ArrayCauHoi = await apiCauhoi.getAll(10, 0)
      if (data !== undefined)
        setCauhoiAll(data)
    }
  
    fetchData()
  }, [])

  // Thêm
  const openInsertPopup = () => {
    setShowInsertPopup(true)
  }
  
  const closeInsertPopup = () => {
    setShowInsertPopup(false)
  }
  
  // Chỉnh sửa
  const handleEdit = (cauhoi: CauHoi) => {
    setEditingQuestion(cauhoi)
  }
  
  const closeEditPopup = () => {
    setEditingQuestion(null)
    setShowEditPopup(false)
  }
  
  // xóa
  const handleDelete = (cauhoi: CauHoi) => {
    setDeletingQuestion(cauhoi)
  }

  const cancelDelete = () => {
    setDeletingQuestion(null)
    setShowDeletePopup(false)
  }

  const handleSubmit = async () => {
    // xử lý thêm
  }
  
  const handleEditSubmit = async () => {
    // xử lý chỉnh sửa
  }
  
  const confirmDelete = async () => {
    // Xử lý xóa
  }
  
    return (
      <>
        {/* Nội dung trong trang */}
        <div className="flex justify-between items-center mb-4">
            <h1 className="text-black text-2xl font-bold">Quản lý câu hỏi</h1>
            <button
              className="bg-gray-700 transition-all ease-in-out hover:bg-green-400 text-white font-bold py-2 px-4 rounded flex items-center gap-2 mr-4"
              onClick={openInsertPopup}
            >
              <FontAwesomeIcon icon={faCirclePlus} className="w-5 h-5" />
              <span>Thêm</span>
            </button>
        </div>
        <div className="table-wrapper" style={{ maxHeight: '550px', overflowY: 'auto' }}>
            <table className="w-full text-center mt-6 shadow-lg rounded-lg">
              <thead className="text-center bg-gray-700 shadow-lg rounded-lg">
                <tr className="text-white">
                  <th scope="col" className="px-6 py-3">STT</th>
                  <th scope="col" className="px-6 py-3">ID</th>
                  <th scope="col" className="px-6 py-3">ID Câu trả lời</th>
                  <th scope="col" className="px-6 py-3">Nội dung câu hỏi</th>
                  <th scope="col" className="px-6 py-3">Action</th>
                </tr>
              </thead>

              <tbody>
                { cauhoiAll !== undefined
                  ?
                  (cauhoiAll.map((cauhoi: CauHoi, thutu: number) => (
                    <tr key={cauhoi.cauhoi_id} className={`${thutu % 2 === 0 ? 'even:bg-gray-50 even:dark:bg-gray-800' : 'odd:bg-white odd:dark:bg-gray-900'} 
                                                  border-b hover:bg-gray-100 hover:cursor-pointers`}>
                      <td className="px-6 py-4 font-bold">{thutu + 1}</td>
                      <td className="px-6 py-4">{cauhoi.cauhoi_id}</td>
                      <td className="px-6 py-4">{cauhoi.cautraloi_id}</td>
                      <td className="px-6 py-4 max-w-md truncate">{cauhoi.cauhoi_name}</td>
                      <td className="px-6 py-4">
                      <button 
                      onClick={() => handleEdit(cauhoi)}
                      className="bg-gray-700 transition-all ease-in-out hover:bg-sky-400
                                          text-white py-2 px-4 rounded-lg mr-2">
                        Edit
                      </button>
                      <button 
                      onClick={() => handleDelete(cauhoi)}
                      className="bg-gray-700 transition-all ease-in-out hover:bg-red-400
                                          text-white py-2 px-4 rounded-lg">
                        Delete
                      </button>
                      </td>
                    </tr>
                  )))
                  :
                  (
                    <tr>
                      <td colSpan={5}>Không có dữ liệu. Vui lòng thử lại sau</td>
                    </tr>
                  )
                }
              </tbody>
            </table>
          </div>

      {/* Popup thêm câu hỏi */}
      {showInsertPopup && (
        <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
          <div className="bg-white p-8 w-1/2 rounded shadow-lg">
            <h2 className="text-2xl font-bold mb-4">Thêm Câu Hỏi</h2>
            <form onSubmit={handleSubmit} className="space-y-4">
              <div>
                <label htmlFor="cauhoi_name" className="block font-semibold">
                  Nội Dung Câu Hỏi:
                </label>
                <textarea
                  id="cauhoi_name"
                  value={newQuestion.cauhoi_name}
                  onChange={(e) =>
                    setNewQuestion({ ...newQuestion, cauhoi_name: e.target.value })
                  }
                  className="border border-gray-300 rounded px-3 py-2 w-full"
                  rows={4}
                />
              </div>
              <div className="flex justify-end">
                <button
                  type="submit"
                  className="bg-sky-400 text-white px-4 py-2 rounded hover:bg-sky-500"
                >
                  Thêm Câu Hỏi
                </button>
                <button
                  type="button"
                  onClick={closeInsertPopup}
                  className="bg-gray-300 text-gray-700 px-4 py-2 rounded ml-4 hover:bg-gray-400"
                >
                  Đóng
                </button>
              </div>
            </form>
          </div>
        </div>
      )}

    {/* Popup chỉnh sửa */}
    {editingQuestion && (
      <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
        <div className="bg-white p-8 w-1/2 rounded shadow-lg">
          <h2 className="text-2xl font-bold mb-4">
            Chỉnh Sửa Câu Hỏi
          </h2>
          <form onSubmit={handleEditSubmit} className="space-y-4">
            <div>
              <label htmlFor="cauhoi_id" className="block font-semibold">
                ID:
              </label>
              <input
                type="text"
                id="cauhoi_id"
                value={editingQuestion.cauhoi_id}
                disabled
                className="border border-gray-300 rounded px-3 py-2 w-full bg-gray-100"
              />
            </div>
            <div>
              <label htmlFor="cautraloi_id" className="block font-semibold">
                ID Câu trả lời:
              </label>
              <input
                type="text"
                id="cautraloi_id"
                value={editingQuestion.cautraloi_id}
                onChange={(e) =>
                  setEditingQuestion({
                    ...editingQuestion,
                    cautraloi_id: e.target.value,
                  })
                }
                className="border border-gray-300 rounded px-3 py-2 w-full"
              />
            </div>
            <div>
              <label htmlFor="cauhoi_name" className="block font-semibold">
                Nội Dung Câu Hỏi:
              </label>
              <textarea
                id="cauhoi_name"
                value={editingQuestion.cauhoi_name}
                onChange={(e) =>
                  setEditingQuestion({
                    ...editingQuestion,
                    cauhoi_name: e.target.value,
                  })
                }
                className="border border-gray-300 rounded px-3 py-2 w-full resize-none mh-100"
                rows={4} // Điều chỉnh số hàng hiển thị
              />
            </div>
            <div className="flex justify-end">
              <button
                type="submit"
                className="bg-sky-400 text-white px-4 py-2 rounded hover:bg-sky-500"
              >
                Lưu
              </button>
              <button
                type="button"
                onClick={closeEditPopup}
                className="bg-gray-300 text-gray-700 px-4 py-2 rounded ml-4 hover:bg-gray-400"
              >
                Đóng
              </button>
            </div>
          </form>
        </div>
      </div>
    )}

    {/* Popup xóa */}
    {deletingQuestion && (
      <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
        <div className="bg-white p-8 w-1/2 rounded shadow-lg">
          <h2 className="text-2xl font-bold mb-4">Xác nhận Xóa Câu Hỏi</h2>
          <p className="mb-4">Bạn có chắc muốn xóa câu hỏi này?</p>
          <form onSubmit={confirmDelete}>
            <p className="mb-4">
              ID: 
              <strong> {deletingQuestion.cauhoi_id}</strong>
            </p>
            <p className="mb-4">
              Nội dung câu hỏi: 
              <strong> {deletingQuestion.cauhoi_name}</strong>
            </p>
            <div className="flex justify-end">
              <button
                type="submit"
                className="bg-red-600 text-white px-4 py-2 rounded hover:bg-red-700 mr-4"
              >
                Xóa
              </button>
              <button
                onClick={cancelDelete}
                className="bg-gray-300 text-gray-700 px-4 py-2 rounded hover:bg-gray-400"
              >
                Hủy
              </button>
            </div>
          </form>
        </div>
      </div>
    )}
    </>
  )
}

export default AdminCauHoi