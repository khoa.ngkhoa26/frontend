import { useEffect, useState } from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCirclePlus } from "@fortawesome/free-solid-svg-icons"

import { ArraySinhVien, SinhVien } from "../../utils/types"
import apiSinhvien from "../../utils/api/sinhvien"

import { ArrayNguoiDung, NguoiDung } from "../../utils/types"
import apiNguoidung from "../../utils/api/nguoidung"
import sinhvien from "../../utils/api/sinhvien"

import ApiMessageEnum from "../../utils/apimessage"

function AdminSinhVien(): JSX.Element {
  const [sinhvienAll, setSinhvienAll] = useState<ArraySinhVien>(undefined)
  const [showAddPopup, setShowAddPopup] = useState<boolean>(false)
  const [showEditPopup, setShowEditPopup] = useState<boolean>(false)
  const [showDeletePopup, setShowDeletePopup] = useState<boolean>(false)
  const [accountList, setAccountList] = useState<Array<string>>([])
  const [selectedDate, setSelectedDate] = useState("")

  // Thêm 
  const [sinhVien, setSinhVien] = useState<SinhVien>({
    sinhvien_id: 1,
    nguoidung_account: "",
    sinhvien_name: "",
    sinhvien_birthday: "",
    sinhvien_gender: "",
  })

  // Thêm
  const openAddPopup = () => {
    setShowAddPopup(true)
  }
  const closeAddPopup = () => {
    setShowAddPopup(false)
  }
  const handleAdd = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault()

    const message: string = await apiSinhvien.insert(sinhVien.nguoidung_account, sinhVien.sinhvien_name, sinhVien.sinhvien_birthday, sinhVien.sinhvien_gender)
    if (message === ApiMessageEnum.INSERT_SINHVIEN_SUCCESS) {
      setSinhVien({
        sinhvien_id: 1,
        nguoidung_account: "",
        sinhvien_name: "",
        sinhvien_birthday: "",
        sinhvien_gender: "",
      })
      setShowAddPopup(false)
      alert("Thêm thành công")

      const newData: ArraySinhVien = await apiSinhvien.getAll(10,0)
      if (newData !== undefined){
        setSinhvienAll(newData)
      }
    }
  }

  // chỉnh sửa
  const openEditPopup = (sinhVien: SinhVien): void => {
    setSinhVien(sinhVien)
    setShowEditPopup(true)
  }
  const closeEditPopup = () => {
    setShowEditPopup(false)
  }
  const handleEdit = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault()

    const message: string = await apiSinhvien.update(sinhVien.sinhvien_id, sinhVien.nguoidung_account, sinhVien.sinhvien_name, sinhVien.sinhvien_birthday, sinhVien.sinhvien_gender)
    if (message === ApiMessageEnum.UPDATE_SINHVIEN_SUCCESS) {
      setSinhVien({
        sinhvien_id: 1,
        nguoidung_account: "",
        sinhvien_name: "",
        sinhvien_birthday: "",
        sinhvien_gender: "",
      })
      setShowEditPopup(false)
      alert("Chỉnh sửa thành công")

      const newData: ArraySinhVien = await apiSinhvien.getAll(10,0)
      if (newData !== undefined){
        setSinhvienAll(newData)
      }
    }
  }
  
  // xóa
  const openDeletePopUp = (sinhVien: SinhVien): void => {
    setSinhVien(sinhVien)
    setShowDeletePopup(true)
  }
  const closeDeletePopUp = () => {
    setShowDeletePopup(false)
  }
  const handleDelete = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault()

    const message: string = await apiSinhvien.remove(sinhVien.sinhvien_id)
    if (message === ApiMessageEnum.REMOVE_CAUHOI_SUCCESS) {
      setSinhVien({
        sinhvien_id: 1,
        nguoidung_account: "",
        sinhvien_name: "",
        sinhvien_birthday: "",
        sinhvien_gender: "",
      })
      setShowEditPopup(false)
      alert("Chỉnh sửa thành công")

      const newData: ArraySinhVien = await apiSinhvien.getAll(10,0)
      if (newData !== undefined){
        setSinhvienAll(newData)
      }
    }
  }
  
  const onChangeAccount = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    setSinhVien({ ...sinhVien, nguoidung_account: event.target.value })
  }
  const onChangeSinhVienName = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setSinhVien({ ...sinhVien, sinhvien_name: event.target.value })
  }
  const onChangeSinhNhat = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setSinhVien({ ...sinhVien, sinhvien_birthday: event.target.value })
  }
  const onChangeGioiTinh = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    setSinhVien({ ...sinhVien, sinhvien_gender: event.target.value })
  }

  const formatDate = (isoDate) => {
    const date = new Date(isoDate) // Chuyển đổi thành đối tượng Date
    const options = { year: 'numeric', month: 'numeric', day: 'numeric' }
    return date.toLocaleDateString('vi-VN', options) 
  }
   
  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      const data: ArraySinhVien = await apiSinhvien.getAll(10, 0)
      if (data !== undefined)
      setSinhvienAll(data)
    }

    fetchData()
  }, [])

  // Lấy thông tin tài khoản người dùng có trên server
  useEffect(() => {
    const fetchAccounts = async (): Promise<void> => {
      try {
        const data: ArrayNguoiDung = await apiNguoidung.getAll(10, 0)
  
        if (data !== undefined) {
          // Lấy danh sách tên tài khoản từ dữ liệu người dùng
          const accountNames = data.map((nguoidung: NguoiDung) => nguoidung.nguoidung_account)
          setAccountList(accountNames)
        }
      } catch (error) {
        console.error('Error fetching account list:', error)
      }
    }
  
    fetchAccounts()
  }, [])
  return (
    <>
      {/* Nội dung trong trang */}
      <div className="flex justify-between items-center mb-4">
        <h1 className="text-black text-2xl font-bold">Quản lý sinh viên</h1>
        <button 
        onClick={openAddPopup}
        className="bg-gray-700 transition duration-300 ease-in-out transform 
        hover:scale-110 hover:bg-green-400 text-white font-bold py-2 px-4 rounded flex items-center gap-2 mr-4">
          <FontAwesomeIcon icon={faCirclePlus} className="w-5 h-5" />
          <span>
            Thêm
          </span>
        </button>
      </div>

      <table className="text-center w-full mt-6 mr-10 shadow-lg rounded-lg">
        <thead className="text-center bg-gray-700 shadow-lg rounded-lg">
          <tr className="text-white">
            <th scope="col" className="px-6 py-3">STT</th>
            <th scope="col" className="px-6 py-3">ID</th>
            <th scope="col" className="px-6 py-3">Tên tài khoản</th>
            <th scope="col" className="px-6 py-3">Tên sinh viên</th>
            <th scope="col" className="px-6 py-3">Ngày sinh</th>
            <th scope="col" className="px-6 py-3">Giới tính</th>
            <th scope="col" className="px-6 py-3">Action</th>
          </tr>
        </thead>

        <tbody>
          { sinhvienAll !== undefined
            ?
            (sinhvienAll.map((sinhvien: SinhVien, thutu: number) => (
              <tr
                key={sinhvien.sinhvien_id}
                className={
                  `${thutu % 2 === 0
                  ?
                  'even:bg-gray-50 even:dark:bg-gray-800'
                  :
                  'odd:bg-white odd:dark:bg-gray-900'} border-b hover:bg-gray-100 hover:cursor-pointers`}>
                <td className="px-6 py-4 font-bold">{thutu + 1}</td>
                <td className="px-6 py-4">{sinhvien.sinhvien_id}</td>
                <td className="px-6 py-4">{sinhvien.nguoidung_account}</td>
                <td className="px-6 py-4">{sinhvien.sinhvien_name}</td>
                <td className="px-6 py-4">{formatDate(sinhvien.sinhvien_birthday)}</td>
                <td className="px-6 py-4">{sinhvien.sinhvien_gender}</td>
                <td className="px-6 py-4">
                <button
                onClick={openEditPopup.bind(null, sinhvien)}
                className="bg-gray-700 transition duration-300 ease-in-out transform 
                hover:scale-110 hover:bg-sky-400 
                  text-white py-2 px-4 rounded-lg mr-2"
                >
                  Sửa
                </button>
                <button
                onClick={openDeletePopUp.bind(null, sinhvien)}
                className="bg-gray-700 transition duration-300 ease-in-out transform 
                hover:scale-110 hover:bg-red-400 
                  text-white py-2 px-4 rounded-lg"
                >
                  Xóa
                </button>
                </td>
              </tr>
            )))
            :
            (
              <tr>
                <td colSpan={6}>Không có dữ liệu. Vui lòng thử lại sau</td>
              </tr>
            )
          }
        </tbody>
      </table>

      {/* Popup thêm */}
      {showAddPopup && (
        <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
          <div className="bg-white p-8 w-1/3 rounded shadow-lg">
            <h2 className="text-2xl font-bold mb-4">Thêm Sinh Viên</h2>
            <form onSubmit={handleAdd} className="flex flex-col gap-4">
            <select
              value={sinhVien.nguoidung_account}
              onChange={onChangeAccount}
              className="border border-gray-300 rounded px-3 py-2"
            >
              <option value="">Chọn tài khoản</option>
              {/* Hiển thị danh sách tài khoản */}
              {accountList.map((account) => (
                <option key={account} value={account}>
                  {account}
                </option>
              ))}
            </select>
              <input
                type="text"
                placeholder="Tên sinh viên"
                value={sinhVien.sinhvien_name}
                onChange={onChangeSinhVienName}
                className="border border-gray-300 rounded px-3 py-2"
              />
               <input
                  type="datetime"
                  value={sinhVien.sinhvien_birthday}
                  onChange={onChangeSinhNhat}
                  className="border border-gray-300 rounded px-3 py-2"
                />
              <select
                value={sinhVien.sinhvien_gender}
                onChange={onChangeGioiTinh}
                className="border border-gray-300 rounded px-3 py-2"
              >
                <option value="Nam">Nam</option>
                <option value="Nu">Nữ</option>
              </select>
              <div className="flex justify-end">
                <button
                  type="submit"
                  className="bg-sky-400 text-white px-4 py-2 rounded hover:bg-sky-500 mr-4"
                >
                  Thêm
                </button>
                <button
                  onClick={closeAddPopup}
                  className="bg-gray-300 text-gray-700 px-4 py-2 rounded hover:bg-gray-400"
                >
                  Hủy
                </button>
              </div>
            </form>
          </div>
        </div>
      )}

       {/* Popup chỉnh sửa */}
       {showEditPopup && (
        <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
          <div className="bg-white p-8 w-1/3 rounded shadow-lg">
            <h2 className="text-2xl font-bold mb-4">Chỉnh sửa Sinh Viên</h2>
            <form onSubmit={handleEdit} className="flex flex-col gap-4">
              <input
                type="text"
                placeholder="ID"
                value={sinhVien.sinhvien_id}
                disabled
                className="border border-gray-300 rounded px-3 py-2"
              />
              <select
                value={sinhVien.nguoidung_account}
                onChange={onChangeAccount}
                className="border border-gray-300 rounded px-3 py-2"
              >
                {/* Hiển thị danh sách tài khoản */}
                {accountList.map((account) => (
                  <option key={account} value={account}>
                    {account}
                  </option>
                ))}
              </select>
              <input
                type="text"
                placeholder="Tên sinh viên"
                value={sinhVien.sinhvien_name}
                className="border border-gray-300 rounded px-3 py-2"
              />
              <input
                  type="datetime-local"
                  value={sinhVien.sinhvien_birthday}
                  onChange={onChangeSinhNhat}
                  className="border border-gray-300 rounded px-3 py-2"
                />
              <select
                value={sinhVien.sinhvien_gender}
                className="border border-gray-300 rounded px-3 py-2"
              >
                <option value="Nam">Nam</option>
                <option value="Nu">Nữ</option>
              </select>
              <div className="flex justify-end">
                <button
                  type="submit"
                  className="bg-sky-400 text-white px-4 py-2 rounded hover:bg-sky-500 mr-4"
                >
                  Lưu
                </button>
                <button
                  onClick={closeEditPopup}
                  className="bg-gray-300 text-gray-700 px-4 py-2 rounded hover:bg-gray-400"
                >
                  Hủy
                </button>
              </div>
            </form>
          </div>
        </div>
      )}

       {/* Popup xóa */}
       {showDeletePopup && (
        <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
          <div className="bg-white p-8 w-1/2 rounded shadow-lg">
            <h2 className="text-2xl font-bold mb-4">Xóa Sinh Viên</h2>
            <form onSubmit={handleDelete} className="flex flex-col gap-4">
                <label className="block mb-1">ID</label>
                <input
                  type="text"
                  value={sinhVien.sinhvien_id}
                  readOnly
                  className="border border-gray-300 rounded px-3 py-2"
                />
                <label className="block mb-1">Tên tài khoản</label>
                <input
                  type="text"
                  value={sinhVien.nguoidung_account}
                  readOnly
                  className="border border-gray-300 rounded px-3 py-2"
                />
                <label className="block mb-1">Tên sinh viên</label>
                <input
                  type="text"
                  value={sinhVien.sinhvien_name}
                  readOnly
                  className="border border-gray-300 rounded px-3 py-2"
                />
                <label className="block mb-1">Ngày sinh</label>
                <input
                  type="text"
                  value={formatDate(sinhVien.sinhvien_birthday)}
                  readOnly
                  className="border border-gray-300 rounded px-3 py-2"
                />
              <div className="flex justify-end">
                <button
                  type="submit"
                  className="bg-red-500 text-white px-4 py-2 rounded hover:bg-red-600 mr-4"
                >
                  Xóa
                </button>
                <button
                  onClick={closeDeletePopUp}
                  className="bg-gray-300 text-gray-700 px-4 py-2 rounded hover:bg-gray-400"
                >
                  Hủy
                </button>
              </div>
            </form>
          </div>
        </div>
      )}
    </>
  )
}

export default AdminSinhVien