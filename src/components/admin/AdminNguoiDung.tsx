import { useEffect, useState } from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCirclePlus } from "@fortawesome/free-solid-svg-icons"

import { ArrayNguoiDung, NguoiDung } from "../../utils/types"
import apiNguoidung from "../../utils/api/nguoidung"

import { ArrayQuyen, Quyen } from "../../utils/types"
import apiQuyen from "../../utils/api/quyen"

import ApiMessageEnum from "../../utils/apimessage"

function AdminNguoiDung(): JSX.Element {
  const [nguoidungAll, setNguoidungAll] = useState<ArrayNguoiDung>(undefined)

  const [showAddPopup, setShowAddPopup] = useState<boolean>(false)
  const [showEditPopup, setShowEditPopup] = useState<boolean>(false)
  const [showDeletePopup, setShowDeletePopup] = useState<boolean>(false)
  
  const [quyenList, setQuyenList] = useState<Array<string>>([])

  const [nguoiDung, setNguoiDung] = useState<NguoiDung>({
    nguoidung_account: "",
    quyen_id: 1,
    nguoidung_password: "",
    nguoidung_displayname: "",
  })

  // Thêm
  const openAddPopup = () => {
    setShowAddPopup(true)
  }
  const closeAddPopup = () => {
    setShowAddPopup(false)
  }
  const handleAdd = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault()

    const message: string = await apiNguoidung.insert(nguoiDung.nguoidung_account, nguoiDung.quyen_id, nguoiDung.nguoidung_password, nguoiDung.nguoidung_displayname )
    if (message === ApiMessageEnum.INSERT_NGUOIDUNG_SUCCESS) {
      setNguoiDung({
        nguoidung_account: "",
        quyen_id: 1,
        nguoidung_password: "",
        nguoidung_displayname: "",
      })
      setShowAddPopup(false)
      alert("Thêm thành công")

      const newData: ArrayNguoiDung = await apiNguoidung.getAll(10,0)
      if (newData !== undefined){
        setNguoidungAll(newData)
      }
    }
  }
  
  // Chỉnh sửa
  const openEditPopup = (user: NguoiDung): void => {
    setNguoiDung(user)
    setShowEditPopup(true)
  }
  const closeEditPopup = () => {
    setShowEditPopup(false)
  }
  const handleEdit= async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault()

    const message: string = await apiNguoidung.update(nguoiDung.nguoidung_account, nguoiDung.quyen_id, nguoiDung.nguoidung_password, nguoiDung.nguoidung_displayname )
    if (message === ApiMessageEnum.UPDATE_NGUOIDUNG_SUCCESS) {
      setNguoiDung({
        nguoidung_account: "",
        quyen_id: 0,
        nguoidung_password: "",
        nguoidung_displayname: "",
      })
      setShowEditPopup(false)
      alert("Cập nhật thành công")

      const newData: ArrayNguoiDung = await apiNguoidung.getAll(10,0)
      if (newData !== undefined){
        setNguoidungAll(newData)
      }
    }
  }
  
  // Xóa
  const openDeletePopup = (user: NguoiDung): void => {
    setNguoiDung(user)
    setShowDeletePopup(true)
  }
  const closeDeletePopup = () => {
    setShowDeletePopup(false)
  }
  const handleDelete = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault()

    const message: string = await apiNguoidung.remove(nguoiDung.nguoidung_account)
    if (message === ApiMessageEnum.REMOVE_NGUOIDUNG_SUCCESS) {
      setNguoiDung({
        nguoidung_account: "",
        nguoidung_displayname: "",
        quyen_id: 1,
        nguoidung_password: "",
      })
      setShowDeletePopup(false)
      alert("Xóa thành công")

      const newData: ArrayNguoiDung = await apiNguoidung.getAll(10,0)
      if (newData !== undefined){
        setNguoidungAll(newData)
      }
    }
  }

  const onChangeQuyen = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    const numericValue = event.target.value.replace(/\D/g, "")
    if (parseInt(numericValue, 10) < 1) {
      setNguoiDung({...nguoiDung, quyen_id: 1})
      return
    }
    setNguoiDung({...nguoiDung, quyen_id: parseInt(numericValue, 10)})
  }
  const onChangeAccount = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setNguoiDung({ ...nguoiDung, nguoidung_account: event.target.value })
  }
  const onChangePassWord = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setNguoiDung({ ...nguoiDung, nguoidung_password: event.target.value })
  }
  const onChangeName = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setNguoiDung({ ...nguoiDung, nguoidung_displayname: event.target.value })
  }

  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      const data: ArrayNguoiDung = await apiNguoidung.getAll(10, 0)
      if (data !== undefined)
      setNguoidungAll(data)
    }

    fetchData()
  }, [])

  // Lấy thông tin câu hỏi
  useEffect(() => {
    const fetchQuyen = async (): Promise<void> => {
      try {
        const data: ArrayQuyen = await apiQuyen.getAll(10, 0)
  
        if (data !== undefined) {
          // Lấy danh sách tên tài khoản từ dữ liệu người dùng
          const quyenId = data.map((quyen: Quyen) => quyen.quyen_id)
          setQuyenList(quyenId)
        }
      } catch (error) {
        console.error('Error fetching account list:', error)
      }
    }
  
    fetchQuyen()
  }, [])

  return (
    <>
      <div className="flex justify-between items-center mb-4">
        <h1 className="text-black text-2xl font-bold">Quản lý người dùng</h1>
        <button 
        onClick={openAddPopup}
        className="bg-gray-700 transition duration-300 ease-in-out transform 
        hover:scale-110 hover:bg-green-400 text-white font-bold py-2 px-4 rounded flex items-center gap-2 mr-4">
          <FontAwesomeIcon icon={faCirclePlus} className="w-5 h-5" />
          <span>
            Thêm
          </span>
        </button>
      </div>

      <div className="table-wrapper" style={{ maxHeight: '550px', overflowY: 'auto' }}>
        <table className="w-full text-center mt-6 shadow-lg rounded-lg">
          <thead className="text-center bg-gray-700 shadow-lg rounded-lg">
            <tr className="text-white">
              <th scope="col" className="px-6 py-3">STT</th>
              <th scope="col" className="px-6 py-3">Tên tài khoản</th>
              <th scope="col" className="px-6 py-3">ID quyền</th>
              <th scope="col" className="px-6 py-3">Mật khẩu</th>
              <th scope="col" className="px-6 py-3">Tên hiển thị</th>
              <th scope="col" className="px-6 py-3">Action</th>
            </tr>
          </thead>

          <tbody>
            { nguoidungAll !== undefined
              ?
              (nguoidungAll.map((nguoidung: NguoiDung, thutu: number) => (
                <tr key={nguoidung.nguoidung_account} className={`${thutu % 2 === 0 ? 'even:bg-gray-50 even:dark:bg-gray-800' : 'odd:bg-white odd:dark:bg-gray-900'} 
                                              border-b hover:bg-gray-100 hover:cursor-pointers`}>
                  <td className="px-6 py-4 font-bold">{thutu + 1}</td>
                  <td className="px-6 py-4">{nguoidung.nguoidung_account}</td>
                  <td className="px-6 py-4">{nguoidung.quyen_id}</td>
                  <td className="px-6 py-4 max-w-11 truncate">{nguoidung.nguoidung_password}</td>
                  <td className="px-6 py-4">{nguoidung.nguoidung_displayname}</td>
                  <td className="px-6 py-4">
                  <button 
                  onClick={openEditPopup.bind(null, nguoidung)}
                  className="bg-gray-700 transition duration-300 ease-in-out transform 
                  hover:scale-110 hover:bg-sky-400 text-white py-2 px-4 rounded-lg mr-2">
                    Edit
                  </button>
                  <button 
                  onClick={openDeletePopup.bind(null, nguoidung)}
                  className="bg-gray-700 transition duration-300 ease-in-out transform 
                  hover:scale-110 hover:bg-red-400 text-white py-2 px-4 rounded-lg">
                    Delete
                  </button>
                  </td>
                </tr>
              )))
              :
              (
                <tr>
                  <td colSpan={6}>Không có dữ liệu. Vui lòng thử lại sau</td>
                </tr>
              )
            }
          </tbody>
        </table>
      </div>

        {/* Popup thêm mới */}
        {showAddPopup && (
        <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
          <div className="bg-white p-8 w-1/3 rounded shadow-lg">
            <form onSubmit={handleAdd}>
              <p className="text-2xl font-bold mb-4">Thêm người dùng mới</p>
              <div className="mb-4">
                <label className="block text-lg font-medium mb-2">
                  ID Quyền:
                </label>
                <select
                  value={nguoiDung.quyen_id}
                  onChange={onChangeQuyen}
                  className="border border-gray-300 rounded px-3 py-2 w-full "
                >
                  {quyenList.map((quyen_id) => (
                    <option key={quyen_id} value={quyen_id}>
                      {quyen_id}
                    </option>
                  ))}
                </select>
              </div>
              <div className="mb-4">
                <label className="block text-lg font-medium mb-2">
                  Tên tài khoản:
                </label>
                <input
                  type="text"
                  value={nguoiDung.nguoidung_account}
                  onChange={onChangeAccount} 
                  className="border border-gray-300 rounded px-3 py-2 w-full"
                />
              </div>
              <div className="mb-4">
                <label className="block text-lg font-medium mb-2">
                  Mật khẩu:
                </label>
                <input
                  type="password"
                  value={nguoiDung.nguoidung_password}
                  onChange={onChangePassWord}
                  className="border border-gray-300 rounded px-3 py-2 w-full"
                />
              </div>
              <div className="mb-4">
                <label className="block text-lg font-medium mb-2">
                  Tên hiển thị:
                </label>
                <input
                  type="text"
                  value={nguoiDung.nguoidung_displayname}
                  onChange={onChangeName}
                  className="border border-gray-300 rounded px-3 py-2 w-full"
                />
              </div>
              <div className="flex justify-end">
                <button
                  type="submit"
                  className="bg-blue-500 text-white px-4 py-2 rounded hover:bg-blue-600 mr-4"
                >
                  Thêm
                </button>
                <button
                  onClick={closeAddPopup}
                  className="bg-gray-300 text-gray-700 px-4 py-2 rounded hover:bg-gray-400"
                >
                  Hủy
                </button>
              </div>
            </form>
          </div>
        </div>
      )}

       {/* Popup chỉnh sửa */}
       {showEditPopup && (
        <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
          <div className="bg-white p-8 w-1/3 rounded shadow-lg">
            <p className="text-2xl font-bold mb-4">Chỉnh sửa người dùng</p>
            <form onSubmit={handleEdit}>
              <div className="mb-4">
                <label className="block text-lg font-medium mb-2">
                  ID quyền:
                </label>
                <select
                  value={nguoiDung.quyen_id}
                  onChange={onChangeQuyen}
                  className="border border-gray-300 rounded px-3 py-2 w-full "
                >
                  {quyenList.map((quyen_id) => (
                    <option key={quyen_id} value={quyen_id}>
                      {quyen_id}
                    </option>
                  ))}
                </select>
              </div>
              <div className="mb-4">
                <label className="block text-lg font-medium mb-2">
                  Tên tài khoản:
                </label>
                <input
                  type="text"
                  value={nguoiDung.nguoidung_account}
                  disabled
                  className="border border-gray-300 rounded px-3 py-2 w-full"
                />
              </div>
              <div className="mb-4">
                <label className="block text-lg font-medium mb-2">
                  Tên hiển thị:
                </label>
                <input
                  type="text"
                  value={nguoiDung.nguoidung_displayname}
                  disabled
                  className="border border-gray-300 rounded px-3 py-2 w-full"
                />
              </div>
              <div className="flex justify-end">
                <button
                  type="submit"
                  className="bg-sky-400 text-white px-4 py-2 rounded hover:bg-sy-500 mr-4"
                >
                  Lưu
                </button>
                <button
                  onClick={closeEditPopup}
                  className="bg-gray-300 text-gray-700 px-4 py-2 rounded hover:bg-gray-400"
                >
                  Hủy
                </button>
              </div>
            </form>
            </div>
          </div>
      )}

      {/* Popup xóa */}
      {showDeletePopup && (
        <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
          <div className="bg-white p-8 w-1/3 rounded shadow-lg">
            <p className="text-2xl font-bold mb-4">Xác nhận xóa người dùng</p>
            <form onSubmit={handleDelete}>
              <p className="text-lg mb-4">
                Bạn có chắc chắn muốn xóa người dùng này không?
              </p>
              <p className="text-lg font-medium mb-4">
                ID quyền: {nguoiDung.quyen_id}
              </p>
              <p className="text-lg font-medium mb-4">
                Tên tài khoản: {nguoiDung.nguoidung_account}
              </p>
              <div className="flex justify-end">
                <button
                  type="submit"
                  className="bg-red-500 text-white px-4 py-2 rounded hover:bg-red-600 mr-4"
                >
                  Xác nhận xóa
                </button>
                <button
                  onClick={closeDeletePopup}
                  className="bg-gray-300 text-gray-700 px-4 py-2 rounded hover:bg-gray-400"
                >
                  Hủy
                </button>
              </div>
            </form>
          </div>
        </div>
      )}
    </>
  )
}

export default AdminNguoiDung