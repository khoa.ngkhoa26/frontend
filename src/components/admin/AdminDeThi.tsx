import { useEffect, useState } from "react"

import { ArrayDeThi, DeThi } from "../../utils/types"
import apiDethi from "../../utils/api/dethi"
import util from "../../utils/util"

function AdminDeThi(): JSX.Element {
  const [dethiAll, setDethiAll] = useState<ArrayDeThi>(undefined)

  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      const data: ArrayDeThi = await apiDethi.getAll(100, 0)
      if (data !== undefined)
      setDethiAll(data)
    }

    fetchData()
  }, [])

  return (
    <>
      <div className="flex justify-between items-center mb-4">
        <h1 className="text-black text-2xl font-bold">Quản lý Đề thi</h1>
      </div>

      <div className="table-wrapper" style={{ maxHeight: '550px', overflowY: 'auto' }}>
        <table className="w-full text-center mt-6 shadow-lg rounded-lg">
          <thead className="text-center bg-gray-700 shadow-lg rounded-lg">
            <tr className="text-white">
              <th scope="col" className="px-6 py-3">STT</th>
              <th scope="col" className="px-6 py-3">ID</th>
              <th scope="col" className="px-6 py-3">ID Đề thi môn học</th>
              <th scope="col" className="px-6 py-3">ID Sinh viên</th>
              <th scope="col" className="px-6 py-3">Ngày bắt đầu</th>
              <th scope="col" className="px-6 py-3">Ngày kết thúc</th>
            </tr>
          </thead>

          <tbody>
            { dethiAll !== undefined && dethiAll.length > 0
              ?
              (dethiAll.map((dethi: DeThi, thutu: number) => (
                <tr
                  key={dethi.dethi_id}
                  className={
                    `${thutu % 2 === 0
                    ? 'even:bg-gray-50 even:dark:bg-gray-800'
                    : 'odd:bg-white odd:dark:bg-gray-900'}
                    border-b hover:bg-gray-100 hover:cursor-pointers`
                  }
                >
                  <td className="px-6 py-4 font-bold">{thutu + 1}</td>
                  <td className="px-6 py-4">{dethi.dethi_id}</td>
                  <td className="px-6 py-4">{dethi.dethimonhoc_id}</td>
                  <td className="px-6 py-4">{dethi.sinhvien_id}</td>
                  <td className="px-6 py-4">{util.formatDateTime(dethi.dethi_startdate)}</td>
                  <td className="px-6 py-4">{util.formatDateTime(dethi.dethi_enddate)}</td>
                </tr>
              )))
              :
              (
                <tr>
                  <td colSpan={6}>Không có dữ liệu. Vui lòng thử lại sau</td>
                </tr>
              )
            }
          </tbody>
        </table>
      </div>
    </>
  )
}

export default AdminDeThi