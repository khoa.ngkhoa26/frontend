import { useEffect, useState } from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCirclePlus } from "@fortawesome/free-solid-svg-icons"

import { ArrayLopHoc, LopHoc } from "../../utils/types"
import apiLophoc from "../../utils/api/lophoc"

import ApiMessageEnum from "../../utils/apimessage"

function AdminLopHoc(): JSX.Element {
  const [lophocAll, setLophocAll] = useState<ArrayLopHoc>(undefined)
  
  const [showAddPopup, setShowAddPopup] = useState<boolean>(false)
  const [showEditPopup, setShowEditPopup] = useState<boolean>(false)
  const [showDeletePopup, setShowDeletePopup] = useState<boolean>(false)

  const [lopHoc, setLopHoc] = useState<LopHoc>({
    lophoc_id: 1,
    lophoc_name: ""
  })

  // Thêm
  const openAddPopup = () => {
    setShowAddPopup(true)
  }
  const closeAddPopup = () => {
    setShowAddPopup(false)
  }
  const handleAddClass = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault()
  
    const message: string = await apiLophoc.insert(lopHoc.lophoc_name)
    if (message === ApiMessageEnum.INSERT_LOPHOC_SUCCESS) {
      setShowAddPopup(false)
      alert('Thêm thành công')

      const newData: ArrayLopHoc = await apiLophoc.getAll(10,0)
      if (newData !== undefined){
        setLophocAll(newData)
      }
    }
  }
  
  // Chỉnh sửa
  const openEditPopup = (lopHoc: LopHoc): void => {
    setLopHoc(lopHoc)
    setShowEditPopup(true)
  }
  const closeEditPopup = () => {
    setShowEditPopup(false)
  }
  const handleEdit  = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault()

    const message: string = await apiLophoc.update(lopHoc.lophoc_id, lopHoc.lophoc_name)
    if (message === ApiMessageEnum.UPDATE_LOPHOC_SUCCESS) {
      setLopHoc({
        lophoc_id: 0,
        lophoc_name: ""
      })
      setShowEditPopup(false)
      alert("Cập nhật thành công")

      const newData: ArrayLopHoc = await apiLophoc.getAll(10,0)
      if (newData !== undefined){
        setLophocAll(newData)
      }
    }
  }
  
  // Xóa
  const openDeletePopup = (lopHoc: LopHoc): void => {
    setLopHoc(lopHoc)
    setShowDeletePopup(true)
  }
  const closeDeletePopup = () => {
    setShowDeletePopup(false)
  }
  const handleDelete = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault()
  
    const message: string = await apiLophoc.remove(lopHoc.lophoc_id)
    console.log(message)
    if (message === ApiMessageEnum.REMOVE_LOPHOC_SUCCESS) {
      setLopHoc({
        lophoc_id: 1,
        lophoc_name: ""
      })
      setShowDeletePopup(false)
      alert("Xóa thành công!")

      const newData: ArrayLopHoc = await apiLophoc.getAll(10,0)
      if (newData !== undefined){
        setLophocAll(newData)
      }
    }
  }

  const onChangeLopHocName = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setLopHoc({ ...lopHoc, lophoc_name: event.target.value })
  }

  useEffect(() => {
    const fetchData = async (): Promise<void> => {
      const data: ArrayLopHoc = await apiLophoc.getAll(10, 0)
      if (data !== undefined)
      setLophocAll(data)
    }

    fetchData()
  }, [])

  return (
    <>
      <div className="flex justify-between items-center mb-4">
        <h1 className="text-black text-2xl font-bold">Quản lý lớp học</h1>
        <button 
        onClick={openAddPopup}
        className="bg-gray-700 transition duration-300 ease-in-out transform 
        hover:scale-110 hover:bg-green-400 text-white font-bold py-2 px-4 rounded flex items-center gap-2 mr-4">
          <FontAwesomeIcon icon={faCirclePlus} className="w-5 h-5" />
          <span>
            Thêm
          </span>
        </button>
      </div>

      <div className="table-wrapper" style={{ maxHeight: '550px', overflowY: 'auto' }}>
        <table className="w-full text-center mt-6 shadow-lg rounded-lg">
          <thead className="text-center bg-gray-700 shadow-lg rounded-lg">
            <tr className="text-white">
              <th scope="col" className="px-6 py-3">STT</th>
              <th scope="col" className="px-6 py-3">ID</th>
              <th scope="col" className="px-6 py-3">Tên lớp học</th>
              <th scope="col" className="px-6 py-3">Action</th>
            </tr>
          </thead>

          <tbody>
            { lophocAll !== undefined && lophocAll.length > 0
              ?
              (lophocAll.map((lophoc: LopHoc, thutu: number) => (
                <tr
                  key={lophoc.lophoc_id}
                  className={
                    `${thutu % 2 === 0
                    ? 'even:bg-gray-50 even:dark:bg-gray-800'
                    : 'odd:bg-white odd:dark:bg-gray-900'} 
                    border-b hover:bg-gray-100 hover:cursor-pointers`}>
                  <td className="px-6 py-4 font-bold">{thutu + 1}</td>
                  <td className="px-6 py-4">{lophoc.lophoc_id}</td>
                  <td className="px-6 py-4">{lophoc.lophoc_name}</td>
                  <td className="px-6 py-4">
                  <button 
                  onClick={openEditPopup.bind(null, lophoc)}
                  className="bg-gray-700 transition duration-300 ease-in-out transform 
                  hover:scale-110 hover:bg-sky-400 text-white py-2 px-4 rounded-lg mr-2">
                    Sửa
                  </button>
                  <button
                  onClick={openDeletePopup.bind(null, lophoc)}
                   className="bg-gray-700 transition duration-300 ease-in-out transform 
                   hover:scale-110 hover:bg-red-400 text-white py-2 px-4 rounded-lg">
                    Xóa
                  </button>
                  </td>
                </tr>
              )))
              :
              (
                <tr>
                  <td colSpan={4}>Không có dữ liệu. Vui lòng thử lại sau</td>
                </tr>
              )
            }
          </tbody>
        </table>
      </div>

      {/* Popup Thêm */}
      {showAddPopup && (
          <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
            <div className="bg-white p-8 w-1/3 rounded shadow-lg">
              <h2 className="text-2xl font-bold mb-4">Thêm Lớp Học</h2>
              <form onSubmit={handleAddClass} className="mt-2">
                <label className="text-black">Tên lớp học:</label>
                <input
                  type="text"
                  value={lopHoc.lophoc_name}
                  onChange={onChangeLopHocName}
                  className="border border-gray-300 rounded px-3 py-2 w-full"
                />
                <div className="flex justify-end mt-5">
                  <button
                    type="submit"
                    className="bg-sky-400 text-white px-4 py-2 rounded hover:bg-sky-500"
                  >
                    Lưu
                  </button>
                  <button
                    onClick={closeAddPopup}
                    className="bg-gray-300 text-gray-700 px-4 py-2 rounded ml-4 hover:bg-gray-400"
                  >
                    Hủy
                  </button>
                </div>
              </form>
            </div>
          </div>
        )}

      {/* Popup Chỉnh sửa */}
      {showEditPopup && (
        <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
          <div className="bg-white p-8 w-1/3 rounded shadow-lg">
            <h2 className="text-2xl font-bold mb-4">Chỉnh Sửa Lớp Học</h2>
            <form onSubmit={handleEdit} className="mt-2">
              <label className="text-black">ID:</label>
              <input
                type="text"
                value={lopHoc.lophoc_id}
                disabled
                className="border border-gray-300 rounded px-3 py-2 w-full bg-gray-100"
              />
              <div className="mt-2">
                <label className="text-black">Tên lớp học:</label>
                <input
                  type="text"
                  value={lopHoc.lophoc_name}
                  onChange={onChangeLopHocName}
                  className="border border-gray-300 rounded px-3 py-2 w-full"
                />
              </div>
              <div className="flex justify-end mt-5">
                <button
                  type="submit"
                  className="bg-sky-400 text-white px-4 py-2 rounded hover:bg-sky-500"
                >
                  Lưu
                </button>
                <button
                  onClick={closeEditPopup}
                  className="bg-gray-300 text-gray-700 px-4 py-2 rounded ml-4 hover:bg-gray-400"
                >
                  Hủy
                </button>
              </div>
            </form>
          </div>
        </div>
      )}

    {/* Popup Xóa */}
    {showDeletePopup && (
          <div className="fixed inset-0 z-50 overflow-auto bg-gray-800 bg-opacity-75 flex items-center justify-center">
            <div className="bg-white p-8 w-1/3 rounded shadow-lg">
              <h2 className="text-2xl font-bold mb-4">Xóa Lớp Học</h2>
              <form onSubmit={handleDelete}>
                <p>
                  ID:
                  <strong> {lopHoc.lophoc_id}</strong>
                </p>
                <p>
                  Tên lớp học:
                  <strong> {lopHoc.lophoc_name}</strong>
                </p>
                <div className="flex justify-end mt-5">
                  <button
                    type="submit"
                    className="bg-red-500 text-white px-4 py-2 rounded hover:bg-red-600"
                  >
                    Xóa
                  </button>
                  <button
                    onClick={closeDeletePopup}
                    className="bg-gray-300 text-gray-700 px-4 py-2 rounded ml-4 hover:bg-gray-400"
                  >
                    Hủy
                  </button>
                </div>
              </form>
            </div>
          </div>
        )}
    </>
  )
}

export default AdminLopHoc