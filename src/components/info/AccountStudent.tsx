import { useEffect, useState } from "react"
import { OnlyNguoiDung, OnlySinhVien } from "../../utils/types"
import apiNguoidung from "../../utils/api/nguoidung"
import util from "../../utils/util"
import ApiMessageEnum from "../../utils/apimessage"

interface AccountStudentProps {
  changeReloadPage: (newReloadPage: boolean) => void
  nguoidunginfo: OnlyNguoiDung
  sinhvieninfo: OnlySinhVien
}

function AccountStudent(props: AccountStudentProps): JSX.Element {
  const [showEditAccountNguoiDung, setShowEditAccountNguoiDung] = useState<boolean>(false)
  const [newPasswordNguoiDung, setNewPasswordNguoiDung] = useState<string>(props.nguoidunginfo?.nguoidung_password || "")
  const [newDisplayNameNguoiDung, setNewDisplayNameNguoiDung] = useState<string>(props.nguoidunginfo?.nguoidung_displayname || "")

  const openEditAccountNguoiDungPopup = () => {
    setShowEditAccountNguoiDung(true)
    setNewPasswordNguoiDung(props.nguoidunginfo?.nguoidung_password || "")
    setNewDisplayNameNguoiDung(props.nguoidunginfo?.nguoidung_displayname || "")
  }
  
  const closeEditAccountNguoiDungPopup = () => {
    setShowEditAccountNguoiDung(false)
  }
  
  const onClickSaveChangeNguoidung = async () => {
    const message: string = await apiNguoidung.updateWithoutQuyen(props.nguoidunginfo?.nguoidung_account as string, newPasswordNguoiDung, newDisplayNameNguoiDung)
    if (message === ApiMessageEnum.UPDATE_NGUOIDUNG_WITHOUT_QUYEN) {
      props.changeReloadPage(true)
      setShowEditAccountNguoiDung(false)
      alert("cập nhập thành công")
    }
  }

  useEffect(() => {

  }, [props.nguoidunginfo, props.sinhvieninfo])

  return (
    <>
      { props.nguoidunginfo === undefined || props.sinhvieninfo === undefined
        ?
        <h1 className="text-center">Vui lòng thử lại</h1>
        :
        <div className="w-1/2 bg-white overflow-hidden shadow rounded-lg border">
          <div className="px-4 py-5 sm:px-6 flex justify-between items-end">
            <div>
              <h3 className="text-lg leading-6 font-bold text-gray-900">
                Thông Tin Tài Khoản
              </h3>
              <p className="mt-1 max-w-2xl text-sky-400">
                Đây là một số thông tin tài khoản của bạn.
              </p>
            </div>
            <button
            onClick={openEditAccountNguoiDungPopup}
            className="bg-gray-800 text-white py-2 px-4 rounded-md 
            hover:bg-sky-400 transition duration-300 ease-in-out 
            transform hover:scale-105 hover:font-bold">
                Chỉnh sửa
            </button>
          </div>
          <div className="border-t border-gray-200 px-4 py-5 sm:p-0">
            <dl className="sm:divide-y sm:divide-gray-200">
              <div className="py-3 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 hover:bg-gray-300 hover:cursor-help">
                <dt className="text-sm font-medium text-gray-500">
                  Tên tài khoản
                </dt>
                <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                  {props.nguoidunginfo?.nguoidung_account}
                </dd>
              </div>
              <div className="py-3 sm:py-5 sm:grid sm:grid-cols-3 
              sm:gap-4 sm:px-6 hover:bg-gray-300 hover:cursor-help">
                  <dt className="text-sm font-medium text-gray-500">
                      Tên hiển thị:
                  </dt>
                  <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                      {props.nguoidunginfo?.nguoidung_displayname}
                  </dd>
              </div>
              <div className="py-3 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 hover:bg-gray-300 hover:cursor-help">
                <dt className="text-sm font-medium text-gray-500">
                  Mật khẩu:
                </dt>
                <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2 max-w-96 truncate">
                  {props.nguoidunginfo?.nguoidung_password}
                </dd>
              </div>
            </dl>
          </div>
        <div className="border-t border-gray-200 px-4 py-5 sm:p-0"/>
          <div className="px-4 py-5 sm:px-6 flex justify-between items-end">
            <div>
              <h3 className="text-lg leading-6 font-bold text-gray-900">
                Thông Tin Cá Nhân
              </h3>
              <p className="mt-1 max-w-2xl text-sky-400">
                Đây là một số thông tin cá nhân của bạn.
              </p>
            </div>
          </div>
          <div className="border-t border-gray-200 px-4 py-5 sm:p-0">
            <dl className="sm:divide-y sm:divide-gray-200">
              <div className="py-3 sm:py-5 sm:grid sm:grid-cols-3 
              sm:gap-4 sm:px-6 hover:bg-gray-300 hover:cursor-help">
                <dt className="text-sm font-medium text-gray-500">
                  Mã sinh viên:
                </dt>
                <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                  {props.sinhvieninfo?.sinhvien_id}
                </dd>
              </div>
              <div className="py-3 sm:py-5 sm:grid sm:grid-cols-3 
              sm:gap-4 sm:px-6 hover:bg-gray-300 hover:cursor-help">
                <dt className="text-sm font-medium text-gray-500">
                  Tên sinh viên:
                </dt>
                <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                  {props.sinhvieninfo?.sinhvien_name}
                </dd>
              </div>
              <div className="py-3 sm:py-5 sm:grid sm:grid-cols-3 
                sm:gap-4 sm:px-6 hover:bg-gray-300 hover:cursor-help">
                <dt className="text-sm font-medium text-gray-500">
                  Giới tính:
                </dt>
                <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2 max-w-96 truncate">
                  {props.sinhvieninfo?.sinhvien_gender}
                </dd>
              </div>
              <div className="py-3 sm:py-5 sm:grid sm:grid-cols-3 
                  sm:gap-4 sm:px-6 hover:bg-gray-300 hover:cursor-help">
                    <dt className="text-sm font-medium text-gray-500">
                      Ngày sinh:
                    </dt>
                    <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2 max-w-96 truncate">
                      {util.formatDate(props.sinhvieninfo?.sinhvien_birthday)}
                    </dd>
              </div>
            </dl>
          </div>
        </div>
      }

      {showEditAccountNguoiDung && (
        <div className="fixed inset-0 z-10 overflow-y-auto">
          <div className="flex items-center justify-center min-h-screen pt-4 px-4 pb-20 text-center">
            <div className="fixed inset-0 transition-opacity" aria-hidden="true">
              <div className="absolute inset-0 bg-black opacity-75"></div>
            </div>
            <span
              className="hidden sm:inline-block sm:align-middle sm:h-screen"
              aria-hidden="true"
            >
              &#8203
            </span>
    
            <div className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden 
              shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
              <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                <div className="sm:flex sm:items-start">
                  <div className="mt-3 text-center sm:mt-0 sm:text-left w-full">
                    <h3 className="text-lg leading-6 font-medium text-gray-900 mb-4">
                      Chỉnh sửa thông tin tài khoản
                    </h3>
                    {/* Form chỉnh sửa thông tin */}
                    <div className="mt-2">
                      <input
                        type="text"
                        className="block w-full p-2 border-gray-300 rounded-md shadow-sm sm:text-sm mb-2"
                        placeholder="Mật khẩu mới"
                        value={newPasswordNguoiDung}
                        onChange={(e) => setNewPasswordNguoiDung(e.target.value)}
                      />
                      <input
                        type="text"
                        className="block w-full p-2 border-gray-300 rounded-md shadow-sm sm:text-sm mb-4"
                        placeholder="Tên hiển thị mới"
                        value={newDisplayNameNguoiDung}
                        onChange={(e) => setNewDisplayNameNguoiDung(e.target.value)}
                      />
                      <button
                        type="button"
                        className="inline-flex justify-center w-full rounded-md border border-transparent shadow-sm px-4 py-2 bg-sky-400 text-base font-medium text-white 
                        hover:bg-sky-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:text-sm"
                        onClick={onClickSaveChangeNguoidung}
                      >
                        Lưu thay đổi
                      </button>
                      <button
                        type="button"
                        className="inline-flex justify-center w-full rounded-md border border-red-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 
                        focus:outline-none focus:ring-2 focus:ring-sky-400 sm:text-sm mr-2 mt-2"
                        onClick={closeEditAccountNguoiDungPopup}
                      >
                        Hủy
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  )
}

export default AccountStudent