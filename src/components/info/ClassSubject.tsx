import { ArrayMonHoc, MonHoc, OnlyLopHoc } from "../../utils/types"

interface ClassSubjectProps {
  changeReloadPage: (newReloadPage: boolean) => void
  lophocinfo: OnlyLopHoc
  monhocinfo: ArrayMonHoc
}

function ClassSubject(props: ClassSubjectProps): JSX.Element {
  return (
    <>
      { props.lophocinfo === undefined || props.monhocinfo === undefined
        ?
        <h1 className="text-center">Vui lòng thử lại</h1>
        :
        <div className="w-1/2 bg-white overflow-hidden shadow rounded-lg border">
          <div className="px-4 py-5 sm:px-6 flex justify-between items-end">
            <div>
              <h3 className="text-lg leading-6 font-bold text-gray-900">
                Thông Tin Lớp Học
              </h3>
              <p className="mt-1 max-w-2xl text-sky-400">
                Đây là một số thông tin lớp học của bạn
              </p>
            </div>
          </div>
          <div className="border-t border-gray-200 px-4 py-5 sm:p-0">
            <dl className="sm:divide-y sm:divide-gray-200">
              <div className="py-3 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 hover:bg-gray-300 hover:cursor-help">
                <dt className="text-sm font-medium text-gray-500">
                  Tên lớp học
                </dt>
                <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                  {props.lophocinfo?.lophoc_name}
                </dd>
              </div>
            </dl>
          </div>
        <div className="border-t border-gray-200 px-4 py-5 sm:p-0"/>
          <div className="px-4 py-5 sm:px-6 flex justify-between items-end">
            <div>
              <h3 className="text-lg leading-6 font-bold text-gray-900">
                Thông Tin Môn học
              </h3>
              <p className="mt-1 max-w-2xl text-sky-400">
                Đây là một số thông tin môn học
              </p>
            </div>
          </div>
          <div className="border-t border-gray-200 px-4 py-5 sm:p-0">
            <dl className="sm:divide-y sm:divide-gray-200">
              { props.monhocinfo !== undefined &&
                props.monhocinfo.map((monhoc: MonHoc) => {
                  if (monhoc.lophoc_id === props.lophocinfo?.lophoc_id)
                    return (
                      <div className="py-3 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 hover:bg-gray-300 hover:cursor-help">
                        <dt className="text-sm font-medium text-gray-500">
                          Tên môn học:
                        </dt>
                        <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                          {monhoc.monhoc_name}
                        </dd>
                      </div>
                    )
                  
                  return <></>
                })
              }
            </dl>
          </div>
        </div>
      }
    </>
  )
}

export default ClassSubject