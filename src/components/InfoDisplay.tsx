import { useEffect, useState } from "react"
import { InfoEnum } from "../utils/enum"
import AccountStudent from "./info/AccountStudent"
import { ArrayDeThiMonHoc, ArrayMonHoc, ArrayThoiGianDeThiChinh, OnlyLopHoc, OnlyNguoiDung, OnlySinhVien } from "../utils/types"
import ClassSubject from "./info/ClassSubject"

interface InfoDisplayProps {
  changeReloadPage: (newReloadPage: boolean) => void
  infomenuChoose: string
  nguoidunginfo: OnlyNguoiDung
  sinhvieninfo: OnlySinhVien
  lophocinfo: OnlyLopHoc
  monhocinfo: ArrayMonHoc
  dethimonhocinfo: ArrayDeThiMonHoc
  thoigiandethiinfo: ArrayThoiGianDeThiChinh
}

function InfoDisplay(props: InfoDisplayProps): JSX.Element {
  const [isLoading, setIsLoading] = useState<boolean>(true)
  const [component, setComponent] = useState<JSX.Element | null>(null)

  useEffect(() => {
    setIsLoading(true)

    const loadComponent = async () => {
      if (props.infomenuChoose === InfoEnum.ACCOUNT_STUDENT) {
        // Simulate loading delay
        await new Promise((resolve) => setTimeout(resolve, 1000))
        setComponent(
          <AccountStudent
            changeReloadPage={props.changeReloadPage}
            nguoidunginfo={props.nguoidunginfo} sinhvieninfo={props.sinhvieninfo}
          />
        )
      } else if (props.infomenuChoose === InfoEnum.CLASS_SUBJECT) {
        // Simulate loading delay
        await new Promise((resolve) => setTimeout(resolve, 1000))
        setComponent(
          <ClassSubject
            changeReloadPage={props.changeReloadPage}
            lophocinfo={props.lophocinfo} monhocinfo={props.monhocinfo}
          />
        )
      } else {
        setComponent(null) // Reset to null if not matching any condition
      }
      // } else if (props.menuChoose === AdminEnum.CAUHOI) {
      //   // Simulate loading delay
      //   await new Promise((resolve) => setTimeout(resolve, 1000))
      //   setComponent(<AdminCauHoi limit={props.limit} />)
      // } else if (props.menuChoose === AdminEnum.CAUHOIDETHI) {
      //   // Simulate loading delay
      //   await new Promise((resolve) => setTimeout(resolve, 1000))
      //   setComponent(<AdminCauHoiDeThi limit={props.limit} />)
      // } else if (props.menuChoose === AdminEnum.CAUTRALOI) {
      //   // Simulate loading delay
      //   await new Promise((resolve) => setTimeout(resolve, 1000))
      //   setComponent(<AdminCauTraLoi limit={props.limit} />)
      // } else if (props.menuChoose === AdminEnum.CHUONG) {
      //   // Simulate loading delay
      //   await new Promise((resolve) => setTimeout(resolve, 1000))
      //   setComponent(<AdminChuong />)
      // } else {
      //   setComponent(null) // Reset to null if not matching any condition
      // }

      setIsLoading(false)
    }

    loadComponent()
  }, [props.infomenuChoose, props.nguoidunginfo, props.sinhvieninfo])

  return (
    <>{isLoading ? <p>Đang tải...</p> : component}</>
  )
}

export default InfoDisplay