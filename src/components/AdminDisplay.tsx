import { useEffect, useState } from "react"

import { AdminEnum } from "../utils/enum"
import AdminCaiDat from "./admin/AdminCaiDat"
import AdminCauHoi from "./admin/AdminCauHoi"
import AdminCauHoiDeThi from "./admin/AdminCauHoiDeThi"
import AdminCauTraLoi from "./admin/AdminCauTraLoi"
import AdminChuong from "./admin/AdminChuong"
import AdminDeThi from "./admin/AdminDeThi"
import AdminDeThiMonHoc from "./admin/AdminDeThiMonHoc"
import AdminLopHoc from "./admin/AdminLopHoc"
import AdminMonHoc from "./admin/AdminMonHoc"
import AdminQuyen from "./admin/AdminQuyen"
import AdminNguoiDung from "./admin/AdminNguoiDung"
import AdminSinhVien from "./admin/AdminSinhVien"
import AdminThoiGianDeThiChinh from "./admin/AdminThoiGianDeThiChinh"
import AdminDangKyLopHoc from "./admin/AdminDangKyLopHoc"

interface AdminDisplayProps {
  menuChoose: string
  limit: number
  changeLimit: (newLimit: number) => void
}

function AdminDisplay(props: AdminDisplayProps): JSX.Element {
  const [isLoading, setIsLoading] = useState(true)
  const [component, setComponent] = useState<JSX.Element | null>(null)

  useEffect(() => {
    const loadComponent = async () => {
      setIsLoading(true)

      if (props.menuChoose === AdminEnum.CAUHOI) {
        // Simulate loading delay
        await new Promise((resolve) => setTimeout(resolve, 1000))
        setComponent(
          <AdminCaiDat
            limit={props.limit} changeLimit={props.changeLimit}
          />
        )
      } else if (props.menuChoose === AdminEnum.CAUHOI) {
        // Simulate loading delay
        await new Promise((resolve) => setTimeout(resolve, 1000))
        setComponent(<AdminCauHoi limit={props.limit} />)
      } else if (props.menuChoose === AdminEnum.CAUHOIDETHI) {
        // Simulate loading delay
        await new Promise((resolve) => setTimeout(resolve, 1000))
        setComponent(<AdminCauHoiDeThi limit={props.limit} />)
      } else if (props.menuChoose === AdminEnum.CAUTRALOI) {
        // Simulate loading delay
        await new Promise((resolve) => setTimeout(resolve, 1000))
        setComponent(<AdminCauTraLoi limit={props.limit} />)
      } else if (props.menuChoose === AdminEnum.CHUONG) {
        // Simulate loading delay
        await new Promise((resolve) => setTimeout(resolve, 1000))
        setComponent(<AdminChuong />)
      } else if (props.menuChoose === AdminEnum.DANGKYLOPHOC) {
        // Simulate loading delay
        await new Promise((resolve) => setTimeout(resolve, 1000))
        setComponent(<AdminDangKyLopHoc />)
      } else if (props.menuChoose === AdminEnum.DETHI) {
        // Simulate loading delay
        await new Promise((resolve) => setTimeout(resolve, 1000))
        setComponent(<AdminDeThi />)
      } else if (props.menuChoose === AdminEnum.DETHIMONHOC) {
        // Simulate loading delay
        await new Promise((resolve) => setTimeout(resolve, 1000))
        setComponent(<AdminDeThiMonHoc />)
      } else if (props.menuChoose === AdminEnum.LOPHOC) {
        // Simulate loading delay
        await new Promise((resolve) => setTimeout(resolve, 1000))
        setComponent(<AdminLopHoc />)
      } else if (props.menuChoose === AdminEnum.MONHOC) {
        // Simulate loading delay
        await new Promise((resolve) => setTimeout(resolve, 1000))
        setComponent(<AdminMonHoc />)
      } else if (props.menuChoose === AdminEnum.NGUOIDUNG) {
        // Simulate loading delay
        await new Promise((resolve) => setTimeout(resolve, 1000))
        setComponent(<AdminNguoiDung />)
      } else if (props.menuChoose === AdminEnum.QUYEN) {
        // Simulate loading delay
        await new Promise((resolve) => setTimeout(resolve, 1000))
        setComponent(<AdminQuyen />)
      } else if (props.menuChoose === AdminEnum.SINHVIEN) {
        // Simulate loading delay
        await new Promise((resolve) => setTimeout(resolve, 1000))
        setComponent(<AdminSinhVien />)
      } else if (props.menuChoose === AdminEnum.THOIGIANDETHICHINH) {
        // Simulate loading delay
        await new Promise((resolve) => setTimeout(resolve, 1000))
        setComponent(<AdminThoiGianDeThiChinh />)
      } else {
        setComponent(null) // Reset to null if not matching any condition
      }

      setIsLoading(false)
    }

    loadComponent()
  }, [props.menuChoose])

  return (
    <div className="w-full max-w-6xl">
      {isLoading ? <p>Đang tải...</p> : component}
    </div>
  )
}

export default AdminDisplay