const ExamHeroBanner = () => {
  return (
    <div className="flex items-center justify-center">
      <article
        className="relative isolate flex flex-col justify-end overflow-hidden 
          rounded-2xl px-32 pb-12 pt-28 max-w-sm mx-auto mt-10 mb-10 shadow-lg transition duration-300 ease-in-out transform hover:scale-105"
      >
        <img
          src="https://img.freepik.com/free-vector/flat-background-back-school-event_23-2149531983.jpg?size=626&ext=jpg&ga=GA1.1.5091195.1700123451&semt=sph"
          className="absolute inset-0 h-full w-full object-cover"
        />
        <div className="absolute inset-0 bg-gradient-to-t from-gray-600 via-gray-900/40"></div>
        <h3 className="z-10 mt-3 text-3xl font-bold text-white">T E S T</h3>
      </article>
      <article
        className="relative isolate flex flex-col justify-end overflow-hidden 
          rounded-2xl px-32 pb-12 pt-28 max-w-sm mx-auto mt-10 mb-10 shadow-lg transition duration-300 ease-in-out transform hover:scale-105"
      >
        <img
          src="https://img.freepik.com/free-psd/left-handers-day-banner-template-concept_23-2148603484.jpg?size=626&ext=jpg"
          className="absolute inset-0 h-full w-full object-cover"
        />
        <div className="absolute inset-0 bg-gradient-to-t from-gray-600 via-gray-900/40"></div>
        <h3 className="z-10 mt-3 text-3xl font-bold text-white">X I N </h3>
      </article>
      <article
        className="relative isolate flex flex-col justify-end overflow-hidden 
          rounded-2xl px-32 pb-12 pt-28 max-w-sm mx-auto mt-10 mb-10 shadow-lg transition duration-300 ease-in-out transform hover:scale-105"
      >
        <img
          src="https://img.freepik.com/free-vector/hand-drawn-flat-new-year-s-resolutions-illustration_23-2149194131.jpg?w=1060&t=st=1703436442~exp=1703437042~hmac=3defc3d1177cd7acd589764cd3ac3bf74ef7eb2e73cf1ae6b6b0c85134696213"
          className="absolute inset-0 h-full w-full object-cover"
        />
        <div className="absolute inset-0 bg-gradient-to-t from-gray-600 via-gray-900/40"></div>
        <h3 className="z-10 mt-3 text-3xl font-bold text-white">C H A O</h3>
      </article>
    </div>
  );
};

export default ExamHeroBanner;
