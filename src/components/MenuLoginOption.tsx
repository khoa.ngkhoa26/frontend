import { useState } from "react"
import { Link } from "react-router-dom"
import Cookies from "js-cookie"

import apiAuth from "../utils/api/auth" 

interface MenuLoginOptionProps {
  isAdmin: boolean
  changeIsAdmin: (newIsAdmin: boolean) => void
  rerender: boolean
  changeRerender: (newRerender: boolean) => void
}

function MenuLoginOption(props: MenuLoginOptionProps): JSX.Element {
  const [showOptions, setShowOptions] = useState(false)

  const onClickLogout = async (): Promise<void> => {
    try {
      // Gọi API logout trước
      await apiAuth.logout()

      // Sau đó, xóa cookie trên phía client
      Cookies.remove("login-token", { path: "/"})

      localStorage.clear()

      // Thực hiện các thao tác khác (nếu cần)
      if (props.isAdmin) {
        props.changeIsAdmin(false)
      }

      if (props.rerender === false) {
        props.changeRerender(true)
      }

      alert("Đăng xuất thành công")
    } catch (error) {
      console.error("Error during logout:", error)
      // Xử lý lỗi nếu cần
      alert("Đã xảy ra lỗi khi đăng xuất")
    }
  }

  const onClickAdmin = (): void => {
    if (props.rerender === false) {
      props.changeRerender(true)
    }
  }

  const toggleOptions = () => {
    setShowOptions(!showOptions)
  }

  return (
    <div className="flex items-center">
      <div className="relative inline-block text-left">
        <div>
          <button
            type="button"
            className="flex text-white focus:outline-none"
            onClick={toggleOptions}
          >
            <img
              className="h-8 w-8 rounded-full object-cover cursor-pointer"
              src="https://cdn-icons-png.flaticon.com/512/3177/3177440.png"
              alt="Avatar"
            />
          </button>
        </div>
        {/* Dropdown content */}
        {showOptions && (
          <div className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg
                         bg-white ring-1 ring-black ring-opacity-5">
            <div className="py-1" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
              <Link to="/info" className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100" role="menuitem">
                Thông tin cá nhân
              </Link>
              {props.isAdmin && (
                <Link to="/admin"
                  className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                  role="menuitem"
                  onClick={onClickAdmin}>
                  Quản trị viên
                </Link>
              )}
              <Link to="/" className="block px-4 py-2 text-sm text-red-600
                                 hover:bg-gray-100" role="menuitem" onClick={onClickLogout}>
                Đăng xuất
              </Link>
            </div>
          </div>
        )}
      </div>
    </div>
  )
}

export default MenuLoginOption
