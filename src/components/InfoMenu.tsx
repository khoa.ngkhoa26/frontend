import { InfoEnum } from "../utils/enum"

interface InfoMenuProps {
  infomenuChoose: string
  changeInfomenuChoose: (newInfomenuChoose: string) => void
}

function InfoMenu(props: InfoMenuProps): JSX.Element {
  const onClickAccountStudent = () => {
    props.changeInfomenuChoose(InfoEnum.ACCOUNT_STUDENT)
  }
  const onClickClassSubject = () => {
    props.changeInfomenuChoose(InfoEnum.CLASS_SUBJECT)
  }
  const onClickExamSchedule = () => {
    props.changeInfomenuChoose(InfoEnum.EXAM_SCHEDULE)
  }
  const onClickResultRealExam = () => {
    props.changeInfomenuChoose(InfoEnum.RESULT_REAL_EXAM)
  }
  const onClickResultTestExam = () => {
    props.changeInfomenuChoose(InfoEnum.RESULT_TEST_EXAM)
  }

  return (
    <div className="w-64 mr-12 ">
      <div className="sticky top-12 text-center flex flex-col gap-2 p-2 border rounded-xl bg-sky-400 text-white">
        <button
          className={
            props.infomenuChoose === InfoEnum.ACCOUNT_STUDENT
            ?
            "px-3 mt-4 py-1 font-semibold transition duration-300 ease-in-out transform hover:scale-105 bg-white text-black hover:bg-black hover:text-white rounded-md"
            :
            "px-3 mt-10 py-1 font-semibold opacity-90 transition duration-300 ease-in-out transform hover:scale-105 hover:bg-white hover:text-black rounded-md"
          }
          onClick={onClickAccountStudent}
        >
          Quản Lý Thông Tin
        </button>

        <button
          className={
            props.infomenuChoose === InfoEnum.CLASS_SUBJECT
            ?
            "px-3 mt-4 py-1 font-semibold transition duration-300 ease-in-out transform hover:scale-105 bg-white text-black hover:bg-black hover:text-white rounded-md"
            :
            "px-3 mt-10 py-1 font-semibold opacity-90 transition duration-300 ease-in-out transform hover:scale-105 hover:bg-white hover:text-black rounded-md"
          }
          onClick={onClickClassSubject}
        >
          Thông tin lớp học và môn học
        </button>

        <button
          className={
            props.infomenuChoose === InfoEnum.EXAM_SCHEDULE
            ?
            "px-3 mt-4 py-1 font-semibold transition duration-300 ease-in-out transform hover:scale-105 bg-white text-black hover:bg-black hover:text-white rounded-md"
            :
            "px-3 mt-10 py-1 font-semibold opacity-90 transition duration-300 ease-in-out transform hover:scale-105 hover:bg-white hover:text-black rounded-md"
          }
          onClick={onClickExamSchedule}
        >
          Lịch Thi
        </button>

        <button 
          className={
            props.infomenuChoose === InfoEnum.RESULT_REAL_EXAM
            ?
            "px-3 mt-4 py-1 font-semibold transition duration-300 ease-in-out transform hover:scale-105 bg-white text-black hover:bg-black hover:text-white rounded-md"
            :
            "px-3 mt-10 py-1 font-semibold opacity-90 transition duration-300 ease-in-out transform hover:scale-105 hover:bg-white hover:text-black rounded-md"
          }
          onClick={onClickResultRealExam}
        >
          Kết Quả Bài Thi
        </button>

        <button
          className={
            props.infomenuChoose === InfoEnum.RESULT_TEST_EXAM
            ?
            "px-3 mt-4 py-1 font-semibold transition duration-300 ease-in-out transform hover:scale-105 bg-white text-black hover:bg-black hover:text-white rounded-md"
            :
            "px-3 mt-10 py-1 font-semibold opacity-90 transition duration-300 ease-in-out transform hover:scale-105 hover:bg-white hover:text-black rounded-md"
          }
          onClick={onClickResultTestExam}
        >
          Lịch Sử Luyện Thi
        </button>
      </div>
    </div>
  )
}

export default InfoMenu