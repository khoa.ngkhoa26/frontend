import { Link } from "react-router-dom"

import  { MenuEnum }  from "../utils/enum"
import MenuLoginOption from "./MenuLoginOption"
import { useEffect, useState } from "react"

interface MenuProps {
  isLogin: boolean
  isAdmin: boolean
  changeIsAdmin: (newIsAdmin: boolean) => void
  rerender: boolean
  changeRerender: (newRerender: boolean) => void
  pageChoose: string
}

function Menu(props: MenuProps): JSX.Element {
  const [hideMenu, setHideMenu] = useState<boolean>(false)

  useEffect(() => {
    // Giấu menu khi ở trang admin
    if (props.pageChoose === MenuEnum.ADMIN)
      setHideMenu(true)
    else
      setHideMenu(false)

    if (props.rerender === true)
      props.changeRerender(false)
  }, [props.pageChoose, props.rerender])

  return (
    <nav className={
      hideMenu
      ?
      "hidden"
      :
      "flex items-center justify-around h-16 bg-white shadow-xl fixed top-0 left-0 right-0 z-50"
      }>
      <Link
        to="/">
        <img src="./test-logo.png" width={100} height={100} className="ml-4" />
      </Link>

      <Link
        to="/"
        className={
          props.pageChoose === MenuEnum.HOME
          ?
          "text-white font-bold bg-sky-400 rounded py-2 px-4 transition duration-300 ease-in-out  hover:bg-black  hover:text-white"
          :
          "text-black transition duration-300 ease-in-out hover:bg-sky-400 hover:text-white py-2 px-4 rounded font-bold"
        }>
        Trang chủ
      </Link>

      <Link to="/exam"
        className={
          props.pageChoose === MenuEnum.EXAM
          ?
          "text-white font-bold bg-sky-400 rounded py-2 px-4 transition duration-300 ease-in-out  hover:bg-black  hover:text-white"
          :
          "text-black transition duration-300 ease-in-out hover:bg-sky-400 hover:text-white py-2 px-4 rounded font-bold"
        }>
        Luyện thi
      </Link>

      {props.isLogin
      ?
        <MenuLoginOption
          isAdmin={props.isAdmin} changeIsAdmin={props.changeIsAdmin}
          rerender={props.rerender} changeRerender={props.changeRerender}
          />
      :
      (
        <Link
          to="/login"
          className={
            props.pageChoose === MenuEnum.LOGIN
            ?
            "text-white font-bold bg-sky-400 rounded py-2 px-4 transition duration-300 ease-in-out  hover:bg-black  hover:text-white"
            :
            "text-black transition duration-300 ease-in-out hover:bg-sky-400 hover:text-white py-2 px-4 rounded font-bold"
          }>
          Đăng nhập
        </Link>
      )}
    </nav>
  )
}

export default Menu