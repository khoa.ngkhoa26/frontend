import { faBook, faMagnifyingGlass, faQuestion } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

import { AdminEnum } from "../utils/enum"

interface AdminMenuProps {
  menuChoose: string
  changeMenuChoose: (newMenuChoose: string) => void
}

function AdminMenu(props: AdminMenuProps): JSX.Element {
  const onClickCauHoi = (): void => {
    props.changeMenuChoose(AdminEnum.CAUHOI)
  }
  const onClickCauHoiDeThi = (): void => {
    props.changeMenuChoose(AdminEnum.CAUHOIDETHI)
  }
  const onClickCauTraLoi = (): void => {
    props.changeMenuChoose(AdminEnum.CAUTRALOI)
  }
  const onClickChuong = (): void => {
    props.changeMenuChoose(AdminEnum.CHUONG)
  }
  const onClickDeThi = (): void => {
    props.changeMenuChoose(AdminEnum.DETHI)
  }
  const onClickDeThiMonHoc = (): void => {
    props.changeMenuChoose(AdminEnum.DETHIMONHOC)
  }
  const onClickDangKyLopHoc = (): void => {
    props.changeMenuChoose(AdminEnum.DANGKYLOPHOC)
  }
  const onClickLopHoc = (): void => {
    props.changeMenuChoose(AdminEnum.LOPHOC)
  }
  const onClickMonHoc = (): void => {
    props.changeMenuChoose(AdminEnum.MONHOC)
  }
  const onClickNguoiDung = (): void => {
    props.changeMenuChoose(AdminEnum.NGUOIDUNG)
  }
  const onClickNguoiQuyen = (): void => {
    props.changeMenuChoose(AdminEnum.QUYEN)
  }
  const onClickSinhVien = (): void => {
    props.changeMenuChoose(AdminEnum.SINHVIEN)
  }
  const onClickThoiGianDeThiChinh = (): void => {
    props.changeMenuChoose(AdminEnum.THOIGIANDETHICHINH)
  }

  return (
    <div className="flex shadow-lg h-screen">
      <div className="hidden md:flex md:w-64 md:flex-col h-full">
        <div className="flex flex-col flex-grow pt-5 bg-white shadow-xl">
          <div className="flex items-center flex-shrink-0 px-4">
            <img className="w-450 h-450" src="./logo_menu_admin.png" alt="" />
          </div>

          <div className="px-4 mt-8">
            <label  className="sr-only"> Search </label>
            <div className="relative">
              <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                <FontAwesomeIcon icon={faMagnifyingGlass} className="w-5 h-5 text-gray-400" />
              </div>

              <input type="search" name="" id=""
                className="block w-full py-2 pl-10 border border-gray-300 rounded-lg focus:ring-sky-400 focus:border-sky-400 sm:text-sm"
                placeholder="Tìm kiếm" />
            </div>
          </div>

          <div className="px-4 mt-6">
            <hr className="border-gray-200" />
          </div>

          <div className="flex flex-col flex-1 px-3 mt-6">
            <div className="space-y-4">
              <div className="flex-1 space-y-2">
                <h2 className="w-32 font-bold border-b-2 border-sky-400">
                  Quản Lý Câu Hỏi
                </h2>
                <button
                  className={
                    props.menuChoose === AdminEnum.CAUHOI
                    ?
                    "flex items-center px-4 py-2.5 text-sm font-medium text-white transition-all duration-200 bg-sky-400 rounded-lg group shadow-lg max-w-96 truncate"
                    :
                    "flex items-center px-4 py-2.5 text-sm font-medium transition-all duration-200 text-gray-900 hover:text-white rounded-lg hover:bg-sky-400 hover:shadow-lg group max-w-96 truncate"
                  }
                  onClick={onClickCauHoi}
                >
                  <FontAwesomeIcon icon={faQuestion} className="flex-shrink-0 w-5 h-5 mr-4" /> 
                  QL Bảng Câu Hỏi
                </button>

                <button
                  className={
                    props.menuChoose === AdminEnum.CAUTRALOI
                    ?
                    "flex items-center px-4 py-2.5 text-sm font-medium text-white transition-all duration-200 bg-sky-400 rounded-lg group shadow-lg max-w-96 truncate"
                    :
                    "flex items-center px-4 py-2.5 text-sm font-medium transition-all duration-200 text-gray-900 hover:text-white rounded-lg hover:bg-sky-400 hover:shadow-lg group max-w-96 truncate"
                  }
                  onClick={onClickCauTraLoi}
                >
                  <FontAwesomeIcon icon={faQuestion} className="flex-shrink-0 w-5 h-5 mr-4" />
                  QL Bảng Câu Trả Lời
                </button>

                <div className="mt-6">
                  <hr className="border-gray-200" />
                </div>

                <h2 className="w-32 font-bold border-b-2 border-sky-400">
                  Quản lý lớp học
                </h2>
                <button
                  className={
                    props.menuChoose === AdminEnum.LOPHOC
                    ?
                    "flex items-center px-4 py-2.5 text-sm font-medium text-white transition-all duration-200 bg-sky-400 rounded-lg group shadow-lg"
                    :
                    "flex items-center px-4 py-2.5 text-sm font-medium transition-all duration-200 text-gray-900 hover:text-white rounded-lg hover:bg-sky-400 hover:shadow-lg group"
                  }
                  onClick={onClickLopHoc}
                >
                  <FontAwesomeIcon icon={faQuestion} className="flex-shrink-0 w-5 h-5 mr-4" />
                  QL Bảng Lớp Học
                </button>
                <button
                  className={
                    props.menuChoose === AdminEnum.DANGKYLOPHOC
                    ?
                    "flex items-center px-4 py-2.5 text-sm font-medium text-white transition-all duration-200 bg-sky-400 rounded-lg group shadow-lg max-w-96 truncate"
                    :
                    "flex items-center px-4 py-2.5 text-sm font-medium transition-all duration-200 text-gray-900 hover:text-white rounded-lg hover:bg-sky-400 hover:shadow-lg group max-w-96 truncate"
                  }
                  onClick={onClickDangKyLopHoc}
                >
                  <FontAwesomeIcon icon={faQuestion} className="flex-shrink-0 w-5 h-5 mr-4" />
                  QL Đăng Ký Lớp Học
                </button>

                <div className="mt-6">
                  <hr className="border-gray-200" />
                </div>

                <h2 className="w-36 font-bold border-b-2 border-sky-400 capitalize">
                  Quản lý môn học
                </h2>
                <button
                  className={
                    props.menuChoose === AdminEnum.MONHOC
                    ?
                    "flex items-center px-4 py-2.5 text-sm font-medium text-white transition-all duration-200 bg-sky-400 rounded-lg group shadow-lg"
                    :
                    "flex items-center px-4 py-2.5 text-sm font-medium transition-all duration-200 text-gray-900 hover:text-white rounded-lg hover:bg-sky-400 hover:shadow-lg group"
                  }
                  onClick={onClickMonHoc}
                >
                  <FontAwesomeIcon icon={faBook} className="flex-shrink-0 w-5 h-5 mr-4" />
                  QL Bảng Môn Học
                </button>

                <button
                  className={
                    props.menuChoose === AdminEnum.CHUONG
                    ?
                    "flex items-center px-4 py-2.5 text-sm font-medium text-white transition-all duration-200 bg-sky-400 rounded-lg group shadow-lg"
                    :
                    "flex items-center px-4 py-2.5 text-sm font-medium transition-all duration-200 text-gray-900 hover:text-white rounded-lg hover:bg-sky-400 hover:shadow-lg group"
                  }
                  onClick={onClickChuong}
                >
                  <FontAwesomeIcon icon={faQuestion} className="flex-shrink-0 w-5 h-5 mr-4" />
                  QL Bảng Chương
                </button>

                <div className="mt-6">
                  <hr className="border-gray-200" />
                </div>

                <h2 className="w-32 font-bold border-b-2 border-sky-400 capitalize">
                  Quản lý đề thi
                </h2>
                <button
                  className={
                    props.menuChoose === AdminEnum.DETHI
                    ?
                    "flex items-center px-4 py-2.5 text-sm font-medium text-white transition-all duration-200 bg-sky-400 rounded-lg group shadow-lg"
                    :
                    "flex items-center px-4 py-2.5 text-sm font-medium transition-all duration-200 text-gray-900 hover:text-white rounded-lg hover:bg-sky-400 hover:shadow-lg group"
                  }
                  onClick={onClickDeThi}
                >
                  <FontAwesomeIcon icon={faQuestion} className="flex-shrink-0 w-5 h-5 mr-4" />
                  QL Bảng Đề Thi
                </button>

                <button
                  className={
                    props.menuChoose === AdminEnum.DETHIMONHOC
                    ?
                    "flex items-center px-4 py-2.5 text-sm font-medium text-white transition-all duration-200 bg-sky-400 rounded-lg group shadow-lg max-w-96 truncate"
                    :
                    "flex items-center px-4 py-2.5 text-sm font-medium transition-all duration-200 text-gray-900 hover:text-white rounded-lg hover:bg-sky-400 hover:shadow-lg group max-w-96 truncate"
                  }
                  onClick={onClickDeThiMonHoc}
                >
                  <FontAwesomeIcon icon={faQuestion} className="flex-shrink-0 w-5 h-5 mr-4" />
                  QL Bảng Đề Thi Môn Học
                </button>

                <button
                  className={
                    props.menuChoose === AdminEnum.CAUHOIDETHI
                    ?
                    "flex items-center px-4 py-2.5 text-sm font-medium text-white transition-all duration-200 bg-sky-400 rounded-lg group shadow-lg max-w-96 truncate"
                    :
                    "flex items-center px-4 py-2.5 text-sm font-medium transition-all duration-200 text-gray-900 hover:text-white rounded-lg hover:bg-sky-400 hover:shadow-lg group max-w-96 truncate"
                  }
                  onClick={onClickCauHoiDeThi}
                >
                  <FontAwesomeIcon icon={faQuestion} className="flex-shrink-0 w-5 h-5 mr-4" />
                  QL Bảng Câu Hỏi Đề Thi
                </button>

                <button
                  className={
                    props.menuChoose === AdminEnum.THOIGIANDETHICHINH
                    ?
                    "flex items-center px-4 py-2.5 text-sm font-medium text-white transition-all duration-200 bg-sky-400 rounded-lg group shadow-lg max-w-96 truncate"
                    :
                    "flex items-center px-4 py-2.5 text-sm font-medium transition-all duration-200 text-gray-900 hover:text-white rounded-lg hover:bg-sky-400 hover:shadow-lg group max-w-96 truncate"
                  }
                  onClick={onClickThoiGianDeThiChinh}
                >
                  <FontAwesomeIcon icon={faBook} className="flex-shrink-0 w-5 h-5 mr-4" />
                  QL Thời Gian Đề Thi Chính
                </button>

                <div className="mt-6">
                  <hr className="border-gray-200" />
                </div>

                <h2 className="w-44 font-bold border-b-2 border-sky-400">
                  Quản Lý Người Dùng
                  </h2>
                <button
                  className={
                    props.menuChoose === AdminEnum.NGUOIDUNG
                    ?
                    "flex items-center px-4 py-2.5 text-sm font-medium text-white transition-all duration-200 bg-sky-400 rounded-lg group shadow-lg"
                    :
                    "flex items-center px-4 py-2.5 text-sm font-medium transition-all duration-200 text-gray-900 hover:text-white rounded-lg hover:bg-sky-400 hover:shadow-lg group"
                  }
                  onClick={onClickNguoiDung}
                >
                  <FontAwesomeIcon icon={faBook} className="flex-shrink-0 w-5 h-5 mr-4" />
                  QL bảng Người dùng
                </button>

                <button
                  className={
                    props.menuChoose === AdminEnum.QUYEN
                    ?
                    "flex items-center px-4 py-2.5 text-sm font-medium text-white transition-all duration-200 bg-sky-400 rounded-lg group shadow-lg"
                    :
                    "flex items-center px-4 py-2.5 text-sm font-medium transition-all duration-200 text-gray-900 hover:text-white rounded-lg hover:bg-sky-400 hover:shadow-lg group"
                  }
                  onClick={onClickNguoiQuyen}
                >
                  <FontAwesomeIcon icon={faBook} className="flex-shrink-0 w-5 h-5 mr-4" />
                  QL bảng Quyền
                </button>

                <button
                  className={
                    props.menuChoose === AdminEnum.SINHVIEN
                    ?
                    "flex items-center px-4 py-2.5 text-sm font-medium text-white transition-all duration-200 bg-sky-400 rounded-lg group shadow-lg"
                    :
                    "flex items-center px-4 py-2.5 text-sm font-medium transition-all duration-200 text-gray-900 hover:text-white rounded-lg hover:bg-sky-400 hover:shadow-lg group"
                  }
                  onClick={onClickSinhVien}
                >
                  <FontAwesomeIcon icon={faBook} className="flex-shrink-0 w-5 h-5 mr-4" />
                  QL bảng Sinh Viên
                </button>
                
                <div className="mt-6">
                  <hr className="border-gray-200" />
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default AdminMenu