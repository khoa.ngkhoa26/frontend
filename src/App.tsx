import { useState } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";

import Menu from "./components/Menu";
import useCheckIfLogin from "./hooks/useCheckIfLogin";
import useCheckIfAdmin from "./hooks/useCheckIfAdmin";
import Admin from "./pages/Admin";
import Exam from "./pages/Exam";
import Home from "./pages/Home";
import Info from "./pages/Info";
import Login from "./pages/Login";
import NotFound from "./pages/NotFound";
import Quiz from "./pages/Quiz";

function App(): JSX.Element {
  // Data dùng cho UI/UX (reload component hay reload css)
  const [rerender, setRerender] = useState<boolean>(false);
  const [isLogin, setIsLogin] = useState<boolean>(false);
  const [isAdmin, setIsAdmin] = useState<boolean>(false);
  const [pageChoose, setPageChoose] = useState<string>("");

  useCheckIfLogin({
    rerender,
    changeRerender: setRerender,
    isLogin,
    changeIsLogin: setIsLogin,
  });
  useCheckIfAdmin({ isLogin, isAdmin, changeIsAdmin: setIsAdmin });

  return (
    <BrowserRouter>
      <Menu
        isLogin={isLogin}
        isAdmin={isAdmin}
        changeIsAdmin={setIsAdmin}
        rerender={rerender}
        changeRerender={setRerender}
        pageChoose={pageChoose}
      />

      <Routes>
        <Route
          path="/"
          element={
            <Home
              rerender={rerender}
              changeRerender={setRerender}
              pageChoose={pageChoose}
              changePageChoose={setPageChoose}
            />
          }
        />

        <Route
          path="/login"
          element={
            <Login
              rerender={rerender}
              changeRerender={setRerender}
              pageChoose={pageChoose}
              changePageChoose={setPageChoose}
            />
          }
        />

        <Route
          path="/admin"
          element={
            <Admin
              rerender={rerender}
              changeRerender={setRerender}
              pageChoose={pageChoose}
              changePageChoose={setPageChoose}
            />
          }
        />

        <Route
          path="/exam"
          element={
            <Exam
              rerender={rerender}
              changeRerender={setRerender}
              pageChoose={pageChoose}
              changePageChoose={setPageChoose}
            />
          }
        />

        <Route
          path="/info"
          element={
            <Info
              rerender={rerender}
              changeRerender={setRerender}
              pageChoose={pageChoose}
              changePageChoose={setPageChoose}
            />
          }
        />

        <Route path="/quiz/:subjectId" element={<Quiz />} />

        <Route path="/*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
